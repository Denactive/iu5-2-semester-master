import React, { useCallback, useEffect, useMemo } from "react";
import { useState } from "react";
import { Input, Button, CircularProgress, Container, ButtonGroup, Grid, Box } from "@mui/material";
import cv from "@techstark/opencv-js";
import { Tensor, InferenceSession } from "onnxruntime-web";
import * as ort from "onnxruntime-web";

import "./index.css";
import Header from "@/Header";

// //////////////////////////////////////////////////////////////////

/*

Оригинал: https://github.com/DonVadimon/RNS_DZ2

Модель должна лежать в public/models/*.onnx

Запуск:
```bash
nvm use
npm ci
npm run dev
npm run build # Сборка бандла для развертывания веб-приложения на сервере
```
*/

// //////////////////////////////////////////////////////////////////


console.log("Попытка 4. Версия модели tiny");

/**
 * Поменяйте тут путь к вашей модели, подписи к классам и цвета.
 * Индекс класса соответствует порядку их записи в файле obj.names в датасете
 */
const MODEL_NAME = "./27.05-15_00_56-tiny.onnx";
const CLASSES_ATTRS: Record<
    number,
    {
        name: string;
        color: string;
    }
> = {
    0: {
        name: "Крендель",
        color: "#ff355e",
    },
    1: {
        name: "Рогалик",
        color: "#66ff66",
    },
    2: {
        name: "Пончик",
        color: "#33ddff",
    },
};

const MODE_INPUT_SHAPE = [1, 3, 640, 640];

const preprocessing = async (source: HTMLImageElement, modelWidth: number, modelHeight: number) => {
    const mat = cv.imread(source); // read from img tag
    const matC3 = new cv.Mat(mat.rows, mat.cols, cv.CV_8UC3); // new image matrix
    cv.cvtColor(mat, matC3, cv.COLOR_RGBA2BGR); // RGBA to BGR

    // padding image to [n x n] dim
    const maxSize = Math.max(matC3.rows, matC3.cols); // get max size from width and height
    const xPad = maxSize - matC3.cols, // set xPadding
        xRatio = maxSize / matC3.cols; // set xRatio
    const yPad = maxSize - matC3.rows, // set yPadding
        yRatio = maxSize / matC3.rows; // set yRatio
    const matPad = new cv.Mat(); // new mat for padded image
    cv.copyMakeBorder(matC3, matPad, 0, yPad, 0, xPad, cv.BORDER_CONSTANT); // padding black

    const input = cv.blobFromImage(
        matPad,
        1 / 255.0, // normalize
        new cv.Size(modelWidth, modelHeight), // resize to model input size
        new cv.Scalar(0, 0, 0),
        true, // swapRB
        false // crop
    ); // preprocessing image matrix

    // release mat opencv
    mat.delete();
    matC3.delete();
    matPad.delete();

    return [input, xRatio, yRatio];
};

export const App = () => {
    const [file, setFile] = useState<File>();
    const [modelObjectUrl, setModelObjectUrl] = useState<string>();
    const [session, setSession] = useState<InferenceSession>();
    const [viewState, setViewState] = useState<
        "initial" | "preview" | "processing" | "result" | "error"
    >("initial");

    useEffect(() => {
        // fetch("https://drive" + MODEL_NAME, {mode: "no-cors"}) // Скачать из облака
        fetch("./" + MODEL_NAME)
            .then((r) => {
                return r.blob();
            })
            .then((b) => {
                console.log(b.size);
                const objectUrl = URL.createObjectURL(b);
                setModelObjectUrl(objectUrl);
            });
    }, []);

    useEffect(() => {
        console.log("modelObjectUrl", modelObjectUrl);

        if (!modelObjectUrl) {
            console.log("=( modelObjectUrl undefined");
            return;
        }

        // По хорошему, тут надо проверить какой-то магический флаг со статусом из библиотеки opencv-js.
        // Но из-за того, что модель весит много и загружается долго, это прокатывает.

        // cv["onRuntimeInitialized"] = async () => {
        // create session
        console.log("Loading YOLOv7 model...");
        const yolov7 = InferenceSession.create(modelObjectUrl).then((yolov7) => {
            // warmup main model
            console.log("Warming up model...");
            const tensor = new Tensor(
                "float32",
                new Float32Array(MODE_INPUT_SHAPE.reduce((a, b) => a * b)),
                MODE_INPUT_SHAPE
            );
            return yolov7.run({ images: tensor }).then(() => {
                setSession(yolov7);
                console.log("Сессия создана и подготовлена");
            });
        });
        // };
    }, [modelObjectUrl]);

    const onFileChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];
        if (file && file.type === "image/jpeg") {
            setViewState("preview");
            setFile(file);
        } else {
            console.warn("Не jpg !");
        }
    }, []);

    const submitAnalize = async () => {
        const canvas = document.getElementById("__CANVA__") as HTMLCanvasElement;
        const ctx = canvas?.getContext("2d", { willReadFrequently: true });

        if (!file || !session || !canvas || !ctx) {
            return;
        }

        setViewState("processing");

        // !!! STEP 2
        const image = new Image();

        image.src = URL.createObjectURL(file);
        image.onload = async () => {
            canvas.width = image.width;
            canvas.height = image.height;

            const xRatio1 = canvas.width / 640; // set xRatio1
            const yRatio1 = canvas.height / 640; // set yRatio1

            ctx.drawImage(image, 0, 0, canvas.width, canvas.height);

            const [modelWidth, modelHeight] = MODE_INPUT_SHAPE.slice(2);
            const [input, xRatio, yRatio] = await preprocessing(image, modelWidth, modelHeight);
            const tensor = new Tensor("float32", new cv.Mat(input).data32F, MODE_INPUT_SHAPE); // to ort.Tensor

            const { output } = await session.run({ images: tensor }); // run session and get output layer

            console.log("output: ", output);

            // !!! STEP 3
            const boxes = [];

            // looping through output
            for (let r = 0; r < output.size; r += output.dims[1]) {
                const data = output.data.slice(r, r + output.dims[1]); // get rows
                const x0 = data.slice(1)[0];
                const y0 = data.slice(1)[1];
                const x1 = data.slice(1)[2];
                const y1 = data.slice(1)[3];
                const classId = data.slice(1)[4];
                const score = data.slice(1)[5];

                const w = Number(x1) - Number(x0);
                const h = Number(y1) - Number(y0);

                boxes.push({
                    classId,
                    probability: score,
                    bounding: [
                        Number(x0) * Number(xRatio) * Number(xRatio1),
                        Number(y0) * Number(yRatio) * Number(yRatio1),
                        w * Number(xRatio) * Number(xRatio1),
                        h * Number(yRatio) * Number(yRatio1),
                    ],
                });
            }

            boxes.forEach((box) => {
                const [x1, y1, width, height] = box.bounding;
                const { classId, probability } = box;
                const attrs = CLASSES_ATTRS[Number(classId)];

                ctx.strokeStyle = attrs.color;
                ctx.lineWidth = 5; // толщина линии в 5px
                ctx.strokeRect(x1, y1, width, height);
                const msg = `${attrs.name} - ${(Number(probability) * 100).toFixed(0)}%`;
                ctx.font = "24px serif";
                ctx.fillText(msg, x1, y1);
            });

            setViewState("result");
        };
    };

    const previewSrc = useMemo(() => (file ? URL.createObjectURL(file) : ""), [file]);
    const showPreview = viewState === "preview" || (viewState === "processing" && previewSrc);

    const allowUpload = !!session;
    const allowSubmit = allowUpload && !!file && viewState !== "processing";
    const submitLoading = !session;

    return (
        <Container>
            <Header title="ДЗ-2 Кириллов Д.С." sections={[{ title: "", url: "#" }]} />
            <Container className="form">
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <ButtonGroup className="controls" variant="contained">
                            <Button disabled={!allowUpload} component="label" variant="contained">
                                {file ? file.name.slice(0, 20) : "Загрузить"}
                                <Input className="file-input" type="file" onChange={onFileChange} />
                            </Button>
                            <p> </p>
                            <Button
                                disabled={!allowSubmit}
                                variant={"contained"}
                                onClick={submitAnalize}
                            >
                                {submitLoading ? (
                                    <CircularProgress size={32} color="inherit" />
                                ) : (
                                    "Анализ"
                                )}
                            </Button>
                        </ButtonGroup>
                    </Grid>
                    <Grid item xs={6}>
                        <Box display="flex" justifyContent="center" flexDirection="row">
                            <span>
                                {Object.entries(CLASSES_ATTRS).map(([, { name, color }]) => (
                                    <span style={{ marginRight: "0.5rem" }} key={name}>
                                        <span
                                            style={{
                                                display: "inline-block",
                                                backgroundColor: color,
                                                width: "30px",
                                                height: "1rem",
                                            }}
                                        ></span>
                                        &nbsp;
                                        {name}
                                    </span>
                                ))}
                            </span>
                        </Box>
                    </Grid>
                </Grid>
            </Container>

            <div className={`image-view ${viewState}`}>
                {showPreview && (
                    <img src={previewSrc} className="preview" crossOrigin={"anonymous"} />
                )}
                <canvas id="__CANVA__"></canvas>
                {viewState === "processing" && (
                    <div className="processing-blur">
                        <CircularProgress className="preview-progress" />
                    </div>
                )}
            </div>
        </Container>
    );
};
