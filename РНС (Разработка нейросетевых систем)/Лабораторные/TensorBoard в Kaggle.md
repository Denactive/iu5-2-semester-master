#РНС 

В Kaggle не работает TensorBoard extension.

Решение 1 - задеплоить TensorBoard сервер в бесплатном хостинге типа nrok и отправлять логи туда через curl.

Решение 2 - скачать логи, запустить TensorBoard локально.

```python
# Общее имя для модели, логов в TensorBoard, архива с логами
MODEL_NAME = datetime.now().strftime("%d.%m-%H:%M:%S") # даты будут -3 часа, т.к. часовой пояс на сервере Kaggle - UTC +0
os.environ["MODEL_NAME"] = MODEL_NAME # буду использовать для названия zip-архива в unix-команде

# ...

# Инициализация TensorBoard
train_log_dir = 'logs/tensorboard-lab3/train/' + MODEL_NAME
train_summary_writer = tfsummary.create_file_writer(train_log_dir)

# ...

# Запись в TensorBoard во время обучения
with train_summary_writer.as_default():
	tfsummary.scalar('loss', tmp[-1][0], step=pbar.n)
```

Скачиваю архив с логами, чтобы посмотреть локально:
```bash
zip -r "logs/tensorboard-logs-$MODEL_NAME.zip" logs/tensorboard-lab3 > /dev/null 2>&1
```

На панели справа ЛКМ по "обновить папку kaggle/working/" > листаем до файла .zip > три точки > Download.
