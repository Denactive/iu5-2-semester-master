#РНС

Источники:
- [Гениальный колаб](https://colab.research.google.com/github/kmkarakaya/ML_tutorials/blob/master/LSTM_Understanding_the_Number_of_Parameters.ipynb#scrollTo=eZUy9dtVjOC5)

## KERAS LSTM CELL STRUCTURE

![[Pasted image 20240520155525.png]]

Рисунок похож на тот с зеленными прямоугольниками из [[РНС (Разработка нейросетевых систем)/Лекция 7#Долгая краткосрочная память (LSTM)]]

Обзор рисунка с внутренней структурой ячейки LSTM:
- У ячейки LSTM есть 3 входа:
    - $h_{t−1}$ значение скрытого состояния предыдущего временного шага (t-1)
    - $c_{t−1}$ предыдущий временной шаг (t-1) Значения состояния ячейки
    - $x_{t}$ текущее входное значение временного интервала (t)
- Существует 4 полносвязных слоя:
    - Forget Gate
    - 2x Input Gates = Input + Candidate
    - Output Gate
- Размеры тензоров ввода и вывода обозначены кружками.
    - Ячейка и скрытые состояния представляют собой векторы, размерность которых равна 2. Это число определяется программистом путем установки параметра LSTM (LSTMoutputDimension) = 2
    - Вход - это вектор с размерностью = 3. Это число также определяется программистом путем принятия решения о том, сколько измерений будет представлять входные данные (например, размерность one-hot кодирования, word embedding и т.д.)
- Обратите внимание, что по определению:
    - Размерности скрытого слоя и состояния ячейки должны совпадать
    - Размерности скрытого слоя и состояния ячейки в моменты времени **t-1** и **t** должны совпадать
    - Каждый вектор входа в последовательности должен иметь один и тот же размер

## Простое объяснение
### Параметры вентиля на примере Forget Gate

![[Pasted image 20240520161652.png]]
- На рисунке
	- Красные кружки - Hidden state values ($2 h_{t−1}$) и
	- Зеленые кружки - Input values $3x_t$
- Всего 5 значений ( $2 h_{t−1} + 3 x_t$ ) являются входом для **полносвязного слоя**
- В выходном слое есть **2 значения** (количество значений равно размерности $h_{t−1}$ Hidden state vector)
- Посчитаем количество параметров по формуле для полносвязного слоя [[Количество параметров в полносвязной НС]]:
	$= (h_{t−1} + x_t) × h_{t−1} + h_{t−1}$
    $= (2 + 3) × 2 + 2$
    $= 12$
- **Итого, Forget Gate имеет 12 параметров (weights + biases)**


### Формула числа всех параметров

В LSTM-ячейке Pytorch есть 4 вентиля. Каждый имеет полносвязную архитектуру внутри.

Формула числа всех параметров LSTM-ячейки PyTorch:
$$4 × (( x  +  h ) ×  h  + h )$$
- x - размерность входного вектора
- h - размерность LSTM units / cells / latent space / output

> В лекциях Канева [[РНС (Разработка нейросетевых систем)/Лекция 7|Лекция 7]] в LSTM 3 вентиля ! Важно для РК
> В реализации PyTorch - 4 вентиля - input gate дублируется


## Матан

**Обозначения**:
Размерности:
- $d$ - размерность входной последовательности
- $h$ - размерность скрытого слоя
Векторы:
- $x_t \in \mathbb{R}^{d}$ - input vector to the LSTM unit
- $f_t \in \mathbb{R}^{h}$ - forget gate's activation vector
- $i_t \in \mathbb{R}^{h}$ - input/update gate's activation vector
- $o_t \in \mathbb{R}^{h}$ - output gate's activation vector
- $h_t \in \mathbb{R}^{h}$ - hidden state vector also known as output vector of the LSTM unit
- $\tilde{c}_ {t} \in \mathbb{R}^{h}$ - cell input activation vector
- $c_t \in \mathbb{R}^{h}$ - cell state vector
- $W \in \mathbb{R}^{h \times d}, U \in \mathbb{R}^{h \times h}, b \in \mathbb{R}^{h}$ - обучаемые матрицы весов и вектор смещений (bias)
Функции активации:
- $\sigma_g$ - sigmoid function.
- $\sigma_c$ - hyperbolic tangent function.
- $\sigma_h$ - hyperbolic tangent function или, как предлагается в некоторых научных работах, $\sigma_h(x) = x$

> Отличие от формул Канева из [[РНС (Разработка нейросетевых систем)/Лекция 7#Долгая краткосрочная память (LSTM)|лекции 7 РНС]]:
- $h_{t-1}$ у Канева обозначено как $c_{t-1}$
- $\tilde{c}_ {t}$ у Канева нет. Значение подставлено сразу в $c_t$. Матрица $U_c$ у него 0, т.е. слагаемое $U_c h_ {t-1}$ у него отсутствует  

Вектор вентиля забывания:
$$ f_ {t} = \sigma _ {g} ( W_ {f} x_ {t} + U_ {f} h_ {t-1} + b_ {f} )$$

Вектор входного вентиля:
 $$i_ {t} = \sigma _ {g} ( W_ {i} x_ {t} + U_ {i} h_{t-1} + b_ {i} )$$
Вектор выходного вентиля:
 $$o_ {t} = \sigma _ {g} ( W_ {o} x_ {t} + U_ {o} h_ {t-1} + b_ {0} )$$
Вектор состояний:
 $$\tilde{c}_ {t} = \sigma _ {c} ( W_ {c} x_ {t} + U_c h_ {t-1} + b_ {c} )$$
 $$c_ {t} = f_ {t} \circ c_ {t-1} + i_ {t} \circ \tilde{c}_t$$
Выходной вектор:
 $$h_ {t} = o_ {t} \circ \sigma_h (c_t)$$



TODO: допиши из колаба