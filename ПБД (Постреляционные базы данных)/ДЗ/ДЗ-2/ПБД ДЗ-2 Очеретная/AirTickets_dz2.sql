-- таблицы дз2
DROP TABLE Flights;
DROP TABLE Airplanes;

CREATE TABLE Airplanes
(
	airplaneID serial PRIMARY KEY NOT NULL,
	planeModel varchar(255) NOT NULL,
	speed point NOT NULL,
	liftingCapacity integer NOT NULL
);

CREATE TABLE Flights
(
	flightID serial PRIMARY KEY NOT NULL,
	departurePoint varchar(255) NOT NULL,
	destinationPoint varchar(255) NOT NULL,
	totalTime integer NOT NULL,
	
	airplane_ID integer,
	CONSTRAINT FK_Airplanes_Flights FOREIGN KEY(airplane_ID) REFERENCES Airplanes(airplaneID)
);

-- Создание индексов
CREATE INDEX ON Airplanes USING hash(planeModel);
CREATE INDEX ON Flights USING btree(totalTime);

-- Заполнение таблиц
-- вспомогательные функции
CREATE OR REPLACE FUNCTION random_between(low INT, high INT) 
   RETURNS INT AS
$$
BEGIN
   RETURN floor(random()* (high - low + 1) + low);
END;
$$ language 'plpgsql' STRICT;

DROP TABLE "Flights1";
DROP TABLE "Airplanes1";

CREATE OR REPLACE PROCEDURE drop_all_tables()
AS $$
BEGIN
	FOR i in 1..50 LOOP
		EXECUTE format('DROP TABLE "Flights%s"', i);
		EXECUTE format('DROP TABLE "Airplanes%s"', i);
	END LOOP;
END
$$ LANGUAGE 'plpgsql';

CALL drop_all_tables();

-- основной цикл создания и заполнения таблиц
CALL create_and_fill_tables();

-- CALL create_tables(3, 3, true, false);
-- CALL fill_tables(3, 3, 0.1, 0.1, 1000, 2000);

EXPLAIN (FORMAT JSON)
-- select * from "Airplanes33" where planeModel = 'Победа' 
-- EXPLAIN ANALYZE 
SELECT * FROM "Airplanes51" as aps JOIN "Flights51" as fls on aps.airplaneID=fls.airplane_ID 
-- 	WHERE aps.planeModel = 'Победа' AND fls.totalTime = 365;
	WHERE fls.airplane_ID=1 AND aps.airplaneID=1

select * from "Airplanes52" where planeModel = 'Победа';
select * from "Flights52" where airplane_ID=1 where totalTime = 365;


CREATE OR REPLACE PROCEDURE create_and_fill_tables()
AS $$
DECLARE
	sel numeric;
	size_ integer;
	k_ integer;
	table_number integer;
BEGIN
	table_number := 1;
	-- selectivity variation
	FOREACH sel IN ARRAY ARRAY[0.01, 0.02, 0.03, 0.05, 0.1, 0.15, 0.2, 0.3] LOOP
		CALL create_tables(table_number, table_number, true, false);
		CALL fill_tables(table_number, table_number, sel, 0.1, 1000, 1000);
		CALL create_tables(table_number + 8, table_number + 8, true, false);
		CALL fill_tables(table_number + 8, table_number + 8, 0.1, sel, 1000, 1000);
		
		-- no indexes
		CALL create_tables(table_number + (8*2), table_number + (8*2), false, false);
		CALL fill_tables(table_number + (8*2), table_number + (8*2), sel, 0.1, 1000, 1000);
		CALL create_tables(table_number + (8*3), table_number + (8*3), false, false);
		CALL fill_tables(table_number + (8*3), table_number + (8*3), 0.1, sel, 1000, 1000);
		
		table_number := table_number + 1;
	END LOOP;
	table_number := table_number + 8*3;
	
	-- table_size variation
	FOREACH size_ IN ARRAY ARRAY[10^2, 10^3, 10^4, 10^5, 10^6] LOOP
		CALL create_tables(table_number, table_number, true, false);
		CALL fill_tables(table_number, table_number, 0.1, 0.1, size_, size_);
		
		-- no indexes
		CALL create_tables(table_number + 5, table_number + 5, false, false);
		CALL fill_tables(table_number + 5, table_number + 5, 0.1, 0.1, size_, size_);
		
		table_number := table_number + 1;
	END LOOP;
	table_number := table_number + 5;
	
	-- relations variation
	FOREACH k_ IN ARRAY ARRAY[1, 2, 10, 100] LOOP
		CALL create_tables(table_number, table_number, false, true);
		CALL fill_tables(table_number, table_number, 0.1, 0.1, 1000, 1000 * k_);
		
		-- no indexes
		CALL create_tables(table_number + 4, table_number + 4, false, false);
		CALL fill_tables(table_number + 4, table_number + 4, 0.1, 0.1, 1000, 1000 * k_);
		
		table_number := table_number + 1;
	END LOOP;
END
$$ LANGUAGE 'plpgsql';

-- 	RAISE EXCEPTION USING errcode='E0001', message='Ошибка, sel: ' || sel;
select gen_fk_indexes(10, 1);

CREATE OR REPLACE FUNCTION gen_fk_indexes(size_ integer, k_ integer) 
   RETURNS integer[] AS
$$
DECLARE
	fk_indexes integer[];
	idx integer;
	new_idx integer;
	t_ integer;
BEGIN
	FOR i in 1..(size_ / k_) LOOP
		FOR j in 1..k_ LOOP
			fk_indexes := array_append(fk_indexes, i);
		END LOOP;
	END LOOP;
	
	-- Тасование Фишера — Йетса
	FOR i in 1..(size_ - 1) LOOP
		idx := size_ - i + 1;
		new_idx := random_between(1, idx - 1);
		
		t_ := fk_indexes[idx];
		fk_indexes[idx] := fk_indexes[new_idx];
		fk_indexes[new_idx] := t_;
	END LOOP;
		 
	RETURN fk_indexes;
END;
$$ language 'plpgsql' STRICT;

CREATE OR REPLACE PROCEDURE fill_tables(table1_number integer, table2_number integer, sel1 numeric, sel2 numeric, rows_count1 integer, rows_count2 integer) 
AS $$
DECLARE
	sel1_counter integer;
	sel2_counter integer;
	planeModelVal varchar(255);
	totalTimeVal integer;
	fk_indexes integer[];
BEGIN 
	sel1_counter := 0;
	sel2_counter := 0;
	
	FOR i IN 1..rows_count1 LOOP
		IF ((sel1_counter < sel1 * rows_count1) AND (random() < sel1)) 
		 OR (rows_count1 - i < sel1 * rows_count1 - sel1_counter) THEN
			planeModelVal = 'Победа';
			sel1_counter = sel1_counter + 1;
		ELSE
			planeModelVal = 'Boeing-' || i;
		END IF;
		
		EXECUTE format('
		 INSERT INTO %I(airplaneID, planeModel, speed, liftingCapacity)
			VALUES (%s, %L, %L, %L)
		 ', concat('Airplanes', table1_number), i, planeModelVal, POINT(random_between(12, 24)*10, random_between(24, 36)*10), random_between(8, 20)*10
		);
	END LOOP;

	fk_indexes := gen_fk_indexes(rows_count2, rows_count2 / rows_count1);
		
	FOR i IN 1..rows_count2 LOOP
		IF ((sel2_counter < sel2 * rows_count2) AND (random() < sel2)) 
		 OR (rows_count2 - i < sel2 * rows_count2 - sel2_counter) THEN
			totalTimeVal = 365; -- minutes
			sel2_counter = sel2_counter + 1;
		ELSE
			totalTimeVal = random_between(12, 36) * 10; -- [2, 6] hours
		END IF;

		EXECUTE format('
		 INSERT INTO %I(flightID, departurePoint, destinationPoint, totalTime, airplane_ID)
			VALUES (%s, %L, %L, %L, %s)
		 ', concat('Flights', table2_number), i, 'DepartureAirport' || i, 'DestinationAirport' || i, totalTimeVal, fk_indexes[i]
		);
	
	END LOOP;
END 
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE PROCEDURE create_tables(table1_number integer, table2_number integer, with_indexes boolean = false, fk_pk_indexes boolean = false) 
AS $$
BEGIN 
	EXECUTE format('
	 CREATE TABLE IF NOT EXISTS %I (
		airplaneID serial PRIMARY KEY NOT NULL,
		planeModel varchar(255) NOT NULL,
		speed point NOT NULL,
		liftingCapacity integer NOT NULL
	)', concat('Airplanes', table1_number));
	
	EXECUTE format('
	 CREATE TABLE IF NOT EXISTS %I (
		flightID serial PRIMARY KEY NOT NULL,
		departurePoint varchar(255) NOT NULL,
		destinationPoint varchar(255) NOT NULL,
		totalTime integer NOT NULL,
		airplane_ID integer,
		CONSTRAINT FK_%s_%s FOREIGN KEY(airplane_ID) REFERENCES %I(airplaneID)
	 )', concat('Flights', table2_number), concat('Airplanes', table1_number), concat('Flights', table2_number), concat('Airplanes', table1_number));
	
	IF (with_indexes = true) THEN
		EXECUTE format('CREATE INDEX ON %I USING hash(planeModel)', concat('Airplanes', table1_number));
		EXECUTE format('CREATE INDEX ON %I USING btree(totalTime)', concat('Flights', table2_number));
	END IF;
	
	IF (fk_pk_indexes = true) THEN
		EXECUTE format('CREATE INDEX ON %I USING hash(airplaneID)', concat('Airplanes', table1_number));
		EXECUTE format('CREATE INDEX ON %I USING btree(airplane_ID)', concat('Flights', table2_number));
	END IF;
END
$$ LANGUAGE 'plpgsql';




-- пункт 3 с одинаковыми размерами таблиц
CALL create_and_fill_tables_punkt3();

CREATE OR REPLACE PROCEDURE create_and_fill_tables_punkt3()
AS $$
DECLARE
	sel numeric;
	size_ integer;
	k_ integer;
	table_number integer;
BEGIN
	table_number := 51;
	
	FOREACH k_ IN ARRAY ARRAY[1, 2, 10, 100] LOOP
		CALL create_tables(table_number, table_number, false, true);
		CALL fill_tables_punkt3(table_number, table_number, 0.1, 0.1, 1000, 1000, k_);
		
		-- no indexes
		CALL create_tables(table_number + 4, table_number + 4, false, false);
		CALL fill_tables_punkt3(table_number + 4, table_number + 4, 0.1, 0.1, 1000, 1000, k_);
		
		table_number := table_number + 1;
	END LOOP;
END
$$ LANGUAGE 'plpgsql';


CREATE OR REPLACE PROCEDURE fill_tables_punkt3(table1_number integer, table2_number integer, sel1 numeric, sel2 numeric, rows_count1 integer, rows_count2 integer, k_ integer) 
AS $$
DECLARE
	sel1_counter integer;
	sel2_counter integer;
	planeModelVal varchar(255);
	totalTimeVal integer;
	fk_indexes integer[];
BEGIN 
	sel1_counter := 0;
	sel2_counter := 0;
	
	FOR i IN 1..rows_count1 LOOP
		IF ((sel1_counter < sel1 * rows_count1) AND (random() < sel1)) 
		 OR (rows_count1 - i < sel1 * rows_count1 - sel1_counter) THEN
			planeModelVal = 'Победа';
			sel1_counter = sel1_counter + 1;
		ELSE
			planeModelVal = 'Boeing-' || i;
		END IF;
		
		EXECUTE format('
		 INSERT INTO %I(airplaneID, planeModel, speed, liftingCapacity)
			VALUES (%s, %L, %L, %L)
		 ', concat('Airplanes', table1_number), i, planeModelVal, POINT(random_between(12, 24)*10, random_between(24, 36)*10), random_between(8, 20)*10
		);
	END LOOP;

	fk_indexes := gen_fk_indexes(rows_count2, k_);
		
	FOR i IN 1..rows_count2 LOOP
		IF ((sel2_counter < sel2 * rows_count2) AND (random() < sel2)) 
		 OR (rows_count2 - i < sel2 * rows_count2 - sel2_counter) THEN
			totalTimeVal = 365; -- minutes
			sel2_counter = sel2_counter + 1;
		ELSE
			totalTimeVal = random_between(12, 36) * 10; -- [2, 6] hours
		END IF;

		EXECUTE format('
		 INSERT INTO %I(flightID, departurePoint, destinationPoint, totalTime, airplane_ID)
			VALUES (%s, %L, %L, %L, %s)
		 ', concat('Flights', table2_number), i, 'DepartureAirport' || i, 'DestinationAirport' || i, totalTimeVal, fk_indexes[i]
		);
	
	END LOOP;
END 
$$ LANGUAGE 'plpgsql';