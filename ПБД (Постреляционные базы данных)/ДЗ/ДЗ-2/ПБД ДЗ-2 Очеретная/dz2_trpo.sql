grant all privileges on database "AirTickets" to postgres;

/* Drop Tables */

DROP TABLE IF EXISTS "Users" CASCADE
;
DROP TABLE IF EXISTS "LuggageVariants" CASCADE
;
DROP TABLE IF EXISTS "AirplanePlaces" CASCADE
;
DROP TABLE IF EXISTS "Flights" CASCADE
;
DROP TABLE IF EXISTS "Tariffs" CASCADE
;
DROP TABLE IF EXISTS "FlightLuggageVariants" CASCADE
;
DROP TABLE IF EXISTS "Tickets" CASCADE
;

/* Create Tables */

CREATE TABLE "Users"
(
	"userID" serial PRIMARY KEY NOT NULL,
	"login" varchar(255) NOT NULL UNIQUE,
	"password" varchar(255) NOT NULL
)
;

CREATE TABLE "LuggageVariants"
(
	"luggageVariantID" serial PRIMARY KEY NOT NULL,
	"cost" real NOT NULL,
	"maxWeight" integer NOT NULL DEFAULT 0
)
;

CREATE TABLE "AirplanePlaces"
(
	"airplanePlacesID" serial PRIMARY KEY NOT NULL,
	"businessMiddleRowsCount" integer NOT NULL DEFAULT 0,
	"businessPlacesCount" integer NOT NULL DEFAULT 0,
	"businessSideRowsCount" integer NOT NULL DEFAULT 0,
	"economMiddleRowsCount" integer NOT NULL DEFAULT 0,
	"economPlacesCount" integer NOT NULL DEFAULT 0,
	"economSideRowsCount" integer NOT NULL DEFAULT 0,
	"firstClassMiddleRowsCount" integer NOT NULL DEFAULT 0,
	"firstClassPlacesCount" integer NOT NULL DEFAULT 0,
	"firstClassSideRowsCount" integer NOT NULL DEFAULT 0
)
;

CREATE TABLE "Flights"
(
	"flightID" serial PRIMARY KEY NOT NULL,
	"departurePoint" varchar(255) NOT NULL,
	"departureTimeAndDate" timestamp NOT NULL,
	"destinationPoint" varchar(255) NOT NULL,
	"destinationTimeAndDate" timestamp NOT NULL,
	"company" varchar(255) NOT NULL,
	
	"airplanePlacesID" integer NOT NULL,
	CONSTRAINT "FK_Flights_AirplanePlaces" FOREIGN KEY("airplanePlacesID") REFERENCES "AirplanePlaces"("airplanePlacesID")
)
;

CREATE TABLE "Tariffs"
(
	"tariffID" serial PRIMARY KEY NOT NULL,
	"class" varchar(100) NOT NULL,
	"cost" real NOT NULL,
	"eat" boolean NOT NULL DEFAULT false,
	"hasChoiceOfPlace" boolean NOT NULL DEFAULT false,
	"luggageMaxWeight" integer NOT NULL DEFAULT 0,
	"name" varchar(100) NOT NULL,
	"returnable" boolean NOT NULL DEFAULT false,
	
	"flightID" integer NOT NULL,
	CONSTRAINT "FK_Tariffs_Flights" FOREIGN KEY("flightID") REFERENCES "Flights"("flightID")
)
;

CREATE TABLE "FlightLuggageVariants"
(
	"flightLuggageVariantsID" serial PRIMARY KEY NOT NULL,
	
	"flightID" integer NOT NULL,
	"luggageVariantID" integer NOT NULL,
	
	CONSTRAINT "FK_FlightLuggageVariants_Flights" FOREIGN KEY("flightID") REFERENCES "Flights"("flightID"),
	CONSTRAINT "FK_FlightLuggageVariants_LuggageVariants" FOREIGN KEY("luggageVariantID") REFERENCES "LuggageVariants"("luggageVariantID")
)
;

CREATE TABLE "Tickets"
(
	"ticketID" serial PRIMARY KEY NOT NULL,
	"purchased" boolean NOT NULL DEFAULT false,
	"booked" boolean NOT NULL DEFAULT false,
	
	"flightID" integer NOT NULL,
	"luggageVariantID" integer,
	"tariffID" integer,
	"userID" integer,
	
	CONSTRAINT "FK_Tickets_Flights" FOREIGN KEY("flightID") REFERENCES "Flights"("flightID"),
	CONSTRAINT "FK_Tickets_LuggageVariants" FOREIGN KEY("luggageVariantID") REFERENCES "LuggageVariants"("luggageVariantID"),
	CONSTRAINT "FK_Tickets_Tariffs" FOREIGN KEY("tariffID") REFERENCES "Tariffs"("tariffID"),
	CONSTRAINT "FK_Tickets_Users" FOREIGN KEY("userID") REFERENCES "Users"("userID")
)
;


INSERT INTO "Users"("login", "password") VALUES ('svetlanlka', 'sss555');

INSERT INTO "LuggageVariants"("cost", "maxWeight") VALUES (5000, 40);
INSERT INTO "LuggageVariants"("cost", "maxWeight") VALUES (8000, 80);
INSERT INTO "LuggageVariants"("cost", "maxWeight") VALUES (2800, 20);
INSERT INTO "LuggageVariants"("cost", "maxWeight") VALUES (1600, 10);

INSERT INTO "AirplanePlaces"("businessMiddleRowsCount", "businessPlacesCount", "businessSideRowsCount", "economMiddleRowsCount", "economPlacesCount", "economSideRowsCount", "firstClassMiddleRowsCount", "firstClassPlacesCount", "firstClassSideRowsCount") VALUES (0, 60, 3, 0, 90, 3, 0, 16, 2);
INSERT INTO "AirplanePlaces"("businessMiddleRowsCount", "businessPlacesCount", "businessSideRowsCount", "economMiddleRowsCount", "economPlacesCount", "economSideRowsCount", "firstClassMiddleRowsCount", "firstClassPlacesCount", "firstClassSideRowsCount") VALUES (3, 45, 2, 4, 120, 2, 2, 12, 2);

INSERT INTO "Flights"("departurePoint", "departureTimeAndDate", "destinationPoint", "destinationTimeAndDate", "company", "airplanePlacesID") VALUES ('Москва', '2023-12-10 11:15', 'Санкт-Петербург', '2023-12-10 13:00', 'Аэрофлот', 1);
INSERT INTO "Flights"("departurePoint", "departureTimeAndDate", "destinationPoint", "destinationTimeAndDate", "company", "airplanePlacesID") VALUES ('Москва', '2023-12-10 18:00', 'Санкт-Петербург', '2023-12-10 19:40', 'S7 Airlines', 2);
INSERT INTO "Flights"("departurePoint", "departureTimeAndDate", "destinationPoint", "destinationTimeAndDate", "company", "airplanePlacesID") VALUES ('Москва', '2023-12-10 15:30', 'Санкт-Петербург', '2023-12-10 17:00', 'Аэрофлот', 1);

INSERT INTO "Tariffs"("class", "cost", "eat", "hasChoiceOfPlace", "luggageMaxWeight", "name", "returnable", "flightID") VALUES ('Бизнес', 10500, true, false, 20, 'Комфорт Лайт', false, 1);
INSERT INTO "Tariffs"("class", "cost", "eat", "hasChoiceOfPlace", "luggageMaxWeight", "name", "returnable", "flightID") VALUES ('Бизнес', 15500, true, true, 30, 'Комфорт Оптимум', true, 1);
INSERT INTO "Tariffs"("class", "cost", "eat", "hasChoiceOfPlace", "luggageMaxWeight", "name", "returnable", "flightID") VALUES ('Эконом', 3500, false, false, 0, 'Эконом Лайт', false, 1);
INSERT INTO "Tariffs"("class", "cost", "eat", "hasChoiceOfPlace", "luggageMaxWeight", "name", "returnable", "flightID") VALUES ('Эконом', 5500, false, false, 10, 'Эконом Оптимум', false, 1);
INSERT INTO "Tariffs"("class", "cost", "eat", "hasChoiceOfPlace", "luggageMaxWeight", "name", "returnable", "flightID") VALUES ('Первый класс', 20500, true, true, 100, 'Комфорт Максимум', true, 1);

INSERT INTO "Tariffs"("class", "cost", "eat", "hasChoiceOfPlace", "luggageMaxWeight", "name", "returnable", "flightID") VALUES ('Бизнес', 8500, true, false, 20, 'Комфорт Базовый', false, 2);
INSERT INTO "Tariffs"("class", "cost", "eat", "hasChoiceOfPlace", "luggageMaxWeight", "name", "returnable", "flightID") VALUES ('Бизнес', 13500, true, true, 30, 'Комфорт Стандарт', true, 2);
INSERT INTO "Tariffs"("class", "cost", "eat", "hasChoiceOfPlace", "luggageMaxWeight", "name", "returnable", "flightID") VALUES ('Эконом', 2500, false, false, 0, 'Эконом Базовый', false, 2);
INSERT INTO "Tariffs"("class", "cost", "eat", "hasChoiceOfPlace", "luggageMaxWeight", "name", "returnable", "flightID") VALUES ('Эконом', 4500, false, false, 10, 'Эконом Стандарт', false, 2);
INSERT INTO "Tariffs"("class", "cost", "eat", "hasChoiceOfPlace", "luggageMaxWeight", "name", "returnable", "flightID") VALUES ('Эконом', 6500, false, false, 20, 'Эконом Плюс', false, 2);

INSERT INTO "Tariffs"("class", "cost", "eat", "hasChoiceOfPlace", "luggageMaxWeight", "name", "returnable", "flightID") VALUES ('Бизнес', 9000, true, false, 20, 'Комфорт', false, 3);
INSERT INTO "Tariffs"("class", "cost", "eat", "hasChoiceOfPlace", "luggageMaxWeight", "name", "returnable", "flightID") VALUES ('Эконом', 4500, false, false, 0, 'Эконом Льготный', false, 3);
INSERT INTO "Tariffs"("class", "cost", "eat", "hasChoiceOfPlace", "luggageMaxWeight", "name", "returnable", "flightID") VALUES ('Эконом', 5200, false, false, 15, 'Эконом Стандарт', false, 3);

INSERT INTO "FlightLuggageVariants"("flightID", "luggageVariantID") VALUES (1, 1);
INSERT INTO "FlightLuggageVariants"("flightID", "luggageVariantID") VALUES (1, 2);
INSERT INTO "FlightLuggageVariants"("flightID", "luggageVariantID") VALUES (1, 3);
INSERT INTO "FlightLuggageVariants"("flightID", "luggageVariantID") VALUES (1, 4);
INSERT INTO "FlightLuggageVariants"("flightID", "luggageVariantID") VALUES (2, 1);
INSERT INTO "FlightLuggageVariants"("flightID", "luggageVariantID") VALUES (2, 3);
INSERT INTO "FlightLuggageVariants"("flightID", "luggageVariantID") VALUES (2, 4);
INSERT INTO "FlightLuggageVariants"("flightID", "luggageVariantID") VALUES (3, 1);
INSERT INTO "FlightLuggageVariants"("flightID", "luggageVariantID") VALUES (3, 3);
INSERT INTO "FlightLuggageVariants"("flightID", "luggageVariantID") VALUES (3, 4);


DO
$$
BEGIN
  FOR i IN 1 .. 166 LOOP
    EXECUTE 'INSERT INTO "Tickets"("purchased", "booked", "flightID", "luggageVariantID", "tariffID", "userID") VALUES (false, false, 1, null, null, null);';
  END LOOP;
END;
$$
;
DO
$$
BEGIN
  FOR i IN 1 .. 177 LOOP
    EXECUTE 'INSERT INTO "Tickets"("purchased", "booked", "flightID", "luggageVariantID", "tariffID", "userID") VALUES (false, false, 2, null, null, null);';
  END LOOP;
END;
$$
;
DO
$$
BEGIN
  FOR i IN 1 .. 166 LOOP
    EXECUTE 'INSERT INTO "Tickets"("purchased", "booked", "flightID", "luggageVariantID", "tariffID", "userID") VALUES (false, false, 3, null, null, null);';
  END LOOP;
END;
$$
;

--book
UPDATE "Tickets" SET "booked"=true, "luggageVariantID"=1, "tariffID"=1, "userID"=1 WHERE "ticketID"=3;
UPDATE "Tickets" SET "booked"=true, "luggageVariantID"=1, "tariffID"=1, "userID"=1 WHERE "ticketID"=5;

--purchase
UPDATE "Tickets" SET "purchased"=true WHERE "ticketID"=4;
UPDATE "Tickets" SET "purchased"=true WHERE "ticketID"=6;

--cancel book
UPDATE "Tickets" SET "booked"=false, "luggageVariantID"=NULL, "tariffID"=NULL, "userID"=NULL WHERE "ticketID"=3;

SELECT * FROM "Users";
SELECT * FROM "LuggageVariants";
SELECT * FROM "AirplanePlaces";
SELECT * FROM "Flights";
SELECT * FROM "Tariffs";
SELECT * FROM "FlightLuggageVariants";
SELECT * FROM "Tickets";

SELECT * FROM "Tickets"
WHERE "ticketID"=1;

select "password" from "Users" where "login"='svetlanlka';

--flights
SELECT "flightID", "departurePoint", "departureTimeAndDate", "destinationPoint", "destinationTimeAndDate", "company"
FROM "Flights" 
WHERE "departurePoint" LIKE '%Москва%' AND "destinationPoint" LIKE '%Санкт-Петер%' AND DATE("departureTimeAndDate")='2023-12-10 00:00:00.000';

--flights tariffs
SELECT f."flightID", t."tariffID", t."class", t."cost", t."eat", t."hasChoiceOfPlace", t."luggageMaxWeight", t."name", t."returnable" 
FROM "Flights" as f
JOIN "Tariffs" as t
ON t."flightID"=f."flightID"


--SELECT t."tariffID", t."class", t."cost", t."eat", t."hasChoiceOfPlace", t."luggageMaxWeight", t."name", t."returnable" 
--FROM "Tariffs" as t 
--WHERE t."flightID"=1;

--flights luggage variants
SELECT f."flightID", lv."luggageVariantID", lv."cost", lv."maxWeight"
FROM "Flights" as f
JOIN "FlightLuggageVariants" as flv
ON flv."flightID"=f."flightID"
JOIN "LuggageVariants" as lv
ON lv."luggageVariantID"=flv."luggageVariantID";

SELECT lv."luggageVariantID", lv."cost", lv."maxWeight"
FROM "LuggageVariants" as lv
JOIN "FlightLuggageVariants" as flv
ON lv."luggageVariantID"=flv."luggageVariantID"
JOIN "Flights" as f
ON flv."flightID"=f."flightID"
GROUP BY flv."flightID", lv."luggageVariantID"
HAVING flv."flightID"=2;

--flights airplane places
SELECT ap."airplanePlacesID", ap."businessMiddleRowsCount", ap."businessPlacesCount", ap."businessSideRowsCount", 
	ap."economMiddleRowsCount", ap."economPlacesCount", ap."economSideRowsCount", 
	ap."firstClassMiddleRowsCount", ap."firstClassPlacesCount", ap."firstClassSideRowsCount" 
FROM "Flights" as f
JOIN "AirplanePlaces" as ap 
ON ap."airplanePlacesID"=f."airplanePlacesID"
GROUP BY f."flightID", ap."airplanePlacesID" 
HAVING f."flightID"=2;

--flights busy places from tickets
SELECT f."flightID", t."ticketID" 
FROM "Flights" as f
JOIN "Tickets" as t 
ON t."flightID"=f."flightID"
WHERE t."booked"=true OR t."purchased"=true;

SELECT * FROM "Tickets"
WHERE "flightID"=1 AND ("booked"=true OR "purchased"=true);


--CREATE TABLE "FlightTariffs"
--(
--	"flightTariffsID" serial PRIMARY KEY NOT NULL,
--	
--	"flightID" integer NOT NULL,
--	"tariffID" integer NOT NULL,
--	
--	CONSTRAINT "FK_FlightTariffs_Flights" FOREIGN KEY("flightID") REFERENCES "Flights"("flightID"),
--	CONSTRAINT "FK_FlightTariffs_Tariffs" FOREIGN KEY("tariffID") REFERENCES "Tariffs"("tariffID")
--)
--;
--
--DROP TABLE IF EXISTS "FlightTariffs" CASCADE;
--SELECT * FROM "FlightTariffs";

