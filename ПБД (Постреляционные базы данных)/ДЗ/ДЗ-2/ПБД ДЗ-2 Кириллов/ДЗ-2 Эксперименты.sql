-- // //////////////////////////////////////////////////////////////////
-- //
-- // Таблица, индексы - база
-- //
-- // //////////////////////////////////////////////////////////////////

CREATE TABLE repositories
(
	id serial PRIMARY KEY NOT NULL,
	name varchar(255) NOT NULL,
	contributors text,
	contributors_tsv tsvector,
	files_cnt integer DEFAULT 0
);

SELECT * FROM repositories;

-- B-tree
CREATE INDEX btree_files_cnt ON repositories (files_cnt);
-- GIN
CREATE INDEX gin_contributors_tsv ON repositories USING GIN (contributors_tsv);




-- // //////////////////////////////////////////////////////////////////
-- //
-- // Функции заполнения таблиц - в генерилка.sql
-- //
-- // //////////////////////////////////////////////////////////////////


-- Пример применения функции генерации generate_table_with_fixed_selectivities с передачей списка параметров через рекурсивный запрос.
-- 
-- Демонстрируется хитрейший способ соединения колонок. Желательно одинаковой длины, иначе будут повторы.
-- Добавляем ROW_NUMBER без сортировки `ORDER BY (SELECT NULL)`
WITH selectivity as (
	SELECT *, to_tsvector(contributors) as doc_tsv, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) as rn1 FROM generate_table_with_fixed_selectivities(0.2, 0.1, 20)
),
twenty_nums as (
	SELECT *, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) as rn2 FROM unnest('{10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200}'::integer[])
)
SELECT * FROM selectivity JOIN twenty_nums on selectivity.rn1 = twenty_nums.rn2;





-- // //////////////////////////////////////////////////////////////////
-- //
-- // Заполнение таблиц для экспериментов векторами параметров
-- //
-- // //////////////////////////////////////////////////////////////////

-- Очередная утилка. Чтобы удобно оформить `experiment_params` далее в виде массива массивов параметров.
-- Хотя можно было бы обойтись рекурсией.
-- Просто `unnest` выпрямляет двумерный массив.
-- Украдено https://stackoverflow.com/questions/8137112/unnest-array-by-one-level
CREATE OR REPLACE FUNCTION unnest_nd_1d(a ANYARRAY, OUT a_1d ANYARRAY)
RETURNS SETOF ANYARRAY
LANGUAGE plpgsql IMMUTABLE PARALLEL SAFE STRICT AS
$$
BEGIN
   FOREACH a_1d SLICE 1 IN ARRAY a LOOP
      RETURN NEXT;
   END LOOP;
END
$$;



WITH sizes as (
  -- Сначала отладить для 100, потом добавлять остальные элементы массива: 1000, 10к и т.д.
  -- Лучше выполнять порционно. Сначала 100-10к, потом отдельно 100к, 1М
  -- На быстром SSD запрос со всеми 5-ю вариантами занимает больше 48 минут. Это примерно 3000 строк в секунду.
	SELECT unnest('{
		100, 10e2, 10e3, 10e4, 10e5
	}'::decimal[]::integer[]) as size_
),
experiment_params as (
	SELECT (unnested_1d[1]::real / 100::real) as num_sel, (unnested_1d[2]::real / 100::real) as text_sel, unnested_1d[3]::boolean as indexes FROM unnest_nd_1d('{
		{1, 10, true},
		{1, 10, false},
		{3, 20, true},
		{3, 20, false},
		{10, 1, true},
		{10, 1, false},
		{20, 3, true},
		{20, 3, false}
	}'::text[][]) as unnested_1d
)
-- Вот так просто выведет параметры экспериментов
-- SELECT * FROM experiment_params as ep CROSS JOIN sizes as s ORDER BY (size_, num_sel, text_sel, indexes);
-- А вот так создаст и заполнит таблицы
SELECT
	size_::text || '_' || num_sel::text || '_' || text_sel::text || '_' || indexes::text as res_for,
	create_and_fill_table(num_sel, text_sel, size_, indexes) as cnt
	FROM experiment_params CROSS JOIN sizes ORDER BY (size_, num_sel, text_sel, indexes);







-- // //////////////////////////////////////////////////////////////////
-- //
-- // Функции для проведения экспериментов
-- //
-- // //////////////////////////////////////////////////////////////////

-- Приводит вывод `EXPLAIN ANALYZE` в виду таблицы с текстовыми строками
-- Хак заключается в приведении консольного вывода `EXPLAIN ANALYZE` к строкам через `RAISE info`
-- Украдено https://stackoverflow.com/questions/36558455/postgresql-output-explain-analyze-to-file
CREATE OR REPLACE FUNCTION get_explain(in qry text, out r text)
RETURNS SETOF text as $$
BEGIN
  FOR r in EXECUTE qry LOOP
    RAISE info '%', r;
    RETURN next;
  END LOOP;
  RETURN;
END;
$$ LANGUAGE plpgsql;



DROP IF EXISTS FUNCTION parse_explain;
-- Парсит результат EXPLAIN ANALYZE
CREATE OR REPLACE FUNCTION parse_explain(qry text)
RETURNS TABLE(nodes_cnt integer, strategies text, costs_sum real, pl_time real, ex_time real) AS
$$
DECLARE
BEGIN
	RETURN QUERY
		WITH stats as (
			SELECT get_explain('EXPLAIN ANALYZE ' || qry) as r
		),
		costs_strs as (
			SELECT
				r,
				substring(substring(r from '^(.*) \(.*\).*\(') from '([a-zA-Z][a-zA-Z ]+[a-zA-Z])( on.*)?') as strategy, -- способ_перебора,
				substring(r from 'cost=\d+\.\d+\.\.(\d+\.\d+)\s')::real as costs
			FROM stats WHERE r LIKE ('%cost%')
		),
		-- Просмотреть, какие значения стратегий и стоимостей были извлечены
		-- SELECT * FROM costs_strs;
		costs_sums as (
			SELECT
				string_agg(strategy, ', ') as strategies,
				sum(costs) as costs_sum,
				count(costs)::integer as nodes_cnt
			FROM costs_strs
		),
		-- Просмотреть, как сагрегировались значения стратегий и просуммировались стоимости
		-- SELECT * FROM costs_sums;
		pl_time_strs as (
			SELECT substring(r from 'Planning Time: (\d+\.\d+)\s')::real as pl_time FROM stats WHERE r LIKE ('%Planning Time%')
		),
		ex_time_strs as (
			SELECT substring(r from 'Execution Time: (\d+\.\d+)\s')::real as ex_time FROM stats WHERE r LIKE ('%Execution Time%')
		)
		SELECT c.nodes_cnt, c.strategies, c.costs_sum, p.pl_time, e.ex_time FROM costs_sums as c CROSS JOIN pl_time_strs as p CROSS JOIN ex_time_strs as e;
END;
$$ LANGUAGE plpgsql;


-- Заданное количество раз выполняет parse_explain(qry)
-- Приводит смешанные числа к тексту для вставки в отчет 
DROP FUNCTION run_exp_n_times;
CREATE OR REPLACE FUNCTION run_exp_n_times(in qry text)
RETURNS TABLE(nodes_cnt integer, strategies text, costs_sum text, pl_time text, ex_time text) AS
$$
DECLARE
	pl_time_r real;
	ex_time_r real;
	costs_sum_r real;
BEGIN
  FOR i in 1..5 LOOP
  	SELECT * FROM parse_explain(qry) INTO nodes_cnt, strategies, costs_sum_r, pl_time_r, ex_time_r;
    -- Чтобы было проще вставить в Excel. Спойлер: не проще. Используй "мастер импорта теста", разделитель - знак табуляции
    ex_time := replace(ex_time_r::text, '.', ',');
    pl_time := replace(pl_time_r::text, '.', ',');
    costs_sum := replace(costs_sum_r::text, '.', ',');
    RETURN next;
  END LOOP;
  RETURN;
END;
$$ LANGUAGE plpgsql;


-- // //////////////////////////////////////////////////////////////////
-- //
-- // Эксперименты
-- //
-- // //////////////////////////////////////////////////////////////////

-- Делать по одному - полная хрень
SELECT * FROM run_exp_n_times(
-- 	'SELECT * FROM repositories_100_1_10_true WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'
-- 	'SELECT * FROM repositories_100_1_10_true WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'
-- 	'SELECT * FROM repositories_100_3_20_true WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'
-- 	'SELECT * FROM repositories_100_3_20_true WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'
-- 	'SELECT * FROM repositories_100_10_1_true WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'
-- 	'SELECT * FROM repositories_100_10_1_true WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'
-- 	'SELECT * FROM repositories_100_20_3_true WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'
	'SELECT * FROM repositories_100_20_3_true WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'
);

-- Трюк. Табличная функция `run_exp_n_times` возвращает тип RECORD внутри рекурсивного запроса
-- Украдено https://stackoverflow.com/questions/4700661/how-can-i-extract-the-values-from-a-record-as-individual-columns-in-postgresql
CREATE TYPE EXP_RES_T as (nodes_cnt integer, strategies text, costs_sum text, pl_time text, ex_time text);

-- 1k indeces
WITH qryies_quoted as (
	SELECT * FROM unnest('{
		''SELECT * FROM repositories_1000_1_10_true WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_1_10_true WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_3_20_true WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_3_20_true WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_10_1_true WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_10_1_true WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_20_3_true WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_20_3_true WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		}'::text[]) as qry_txt_quoted
	),
	qryies as (
		SELECT replace(replace(qry_txt_quoted, '''SELECT', 'SELECT'), ')''', ')') as qry_txt FROM qryies_quoted
	),
	experiments_series as (
		SELECT run_exp_n_times(qry_txt) as exp_result FROM qryies
	)
	SELECT
		(exp_result::text::EXP_RES_T).nodes_cnt,
		(exp_result::text::EXP_RES_T).strategies,
		(exp_result::text::EXP_RES_T).costs_sum,
		(exp_result::text::EXP_RES_T).pl_time,
		(exp_result::text::EXP_RES_T).ex_time
		FROM experiments_series;


-- 10k indeces
WITH qryies_quoted as (
	SELECT * FROM unnest('{
		''SELECT * FROM repositories_10000_1_10_true WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_1_10_true WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_3_20_true WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_3_20_true WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_10_1_true WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_10_1_true WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_20_3_true WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_20_3_true WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		}'::text[]) as qry_txt_quoted
	),
	qryies as (
		SELECT replace(replace(qry_txt_quoted, '''SELECT', 'SELECT'), ')''', ')') as qry_txt FROM qryies_quoted
	),
	experiments_series as (
		SELECT run_exp_n_times(qry_txt) as exp_result FROM qryies
	)
	SELECT
		(exp_result::text::EXP_RES_T).nodes_cnt,
		(exp_result::text::EXP_RES_T).strategies,
		(exp_result::text::EXP_RES_T).costs_sum,
		(exp_result::text::EXP_RES_T).pl_time,
		(exp_result::text::EXP_RES_T).ex_time
		FROM experiments_series;


-- 100k indeces
WITH qryies_quoted as (
	SELECT * FROM unnest('{
		''SELECT * FROM repositories_10000_1_10_true WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_1_10_true WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_3_20_true WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_3_20_true WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_10_1_true WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_10_1_true WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_20_3_true WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_20_3_true WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		}'::text[]) as qry_txt_quoted
	),
	qryies as (
		SELECT replace(replace(qry_txt_quoted, '''SELECT', 'SELECT'), ')''', ')') as qry_txt FROM qryies_quoted
	),
	experiments_series as (
		SELECT run_exp_n_times(qry_txt) as exp_result FROM qryies
	)
	SELECT
		(exp_result::text::EXP_RES_T).nodes_cnt,
		(exp_result::text::EXP_RES_T).strategies,
		(exp_result::text::EXP_RES_T).costs_sum,
		(exp_result::text::EXP_RES_T).pl_time,
		(exp_result::text::EXP_RES_T).ex_time
		FROM experiments_series;

-- 1M indeces
WITH qryies_quoted as (
	SELECT * FROM unnest('{
	''SELECT * FROM repositories_1000000_1_10_true WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_1_10_true WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_3_20_true WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_3_20_true WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_10_1_true WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_10_1_true WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_20_3_true WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_20_3_true WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		}'::text[]) as qry_txt_quoted
	),
	qryies as (
		SELECT replace(replace(qry_txt_quoted, '''SELECT', 'SELECT'), ')''', ')') as qry_txt FROM qryies_quoted
	),
	experiments_series as (
		SELECT run_exp_n_times(qry_txt) as exp_result FROM qryies
	)
	SELECT
		(exp_result::text::EXP_RES_T).nodes_cnt,
		(exp_result::text::EXP_RES_T).strategies,
		(exp_result::text::EXP_RES_T).costs_sum,
		(exp_result::text::EXP_RES_T).pl_time,
		(exp_result::text::EXP_RES_T).ex_time
		FROM experiments_series;
		

-- 100 no indeces
WITH qryies_quoted as (
	SELECT * FROM unnest('{
		''SELECT * FROM repositories_100_1_10_false WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_1_10_false WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_3_20_false WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_3_20_false WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_10_1_false WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_10_1_false WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_20_3_false WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_20_3_false WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		}'::text[]) as qry_txt_quoted
	),
	qryies as (
		SELECT replace(replace(qry_txt_quoted, '''SELECT', 'SELECT'), ')''', ')') as qry_txt FROM qryies_quoted
	),
	experiments_series as (
		SELECT run_exp_n_times(qry_txt) as exp_result FROM qryies
	)
	SELECT
		(exp_result::text::EXP_RES_T).nodes_cnt,
		(exp_result::text::EXP_RES_T).strategies,
		(exp_result::text::EXP_RES_T).costs_sum,
		(exp_result::text::EXP_RES_T).pl_time,
		(exp_result::text::EXP_RES_T).ex_time
		FROM experiments_series;

-- 1k no indeces
WITH qryies_quoted as (
	SELECT * FROM unnest('{
		''SELECT * FROM repositories_1000_1_10_false WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_1_10_false WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_3_20_false WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_3_20_false WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_10_1_false WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_10_1_false WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_20_3_false WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_20_3_false WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		}'::text[]) as qry_txt_quoted
	),
	qryies as (
		SELECT replace(replace(qry_txt_quoted, '''SELECT', 'SELECT'), ')''', ')') as qry_txt FROM qryies_quoted
	),
	experiments_series as (
		SELECT run_exp_n_times(qry_txt) as exp_result FROM qryies
	)
	SELECT
		(exp_result::text::EXP_RES_T).nodes_cnt,
		(exp_result::text::EXP_RES_T).strategies,
		(exp_result::text::EXP_RES_T).costs_sum,
		(exp_result::text::EXP_RES_T).pl_time,
		(exp_result::text::EXP_RES_T).ex_time
		FROM experiments_series;

-- 10k no indeces
WITH qryies_quoted as (
	SELECT * FROM unnest('{
		''SELECT * FROM repositories_10000_1_10_false WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_1_10_false WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_3_20_false WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_3_20_false WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_10_1_false WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_10_1_false WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_20_3_false WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_20_3_false WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		}'::text[]) as qry_txt_quoted
	),
	qryies as (
		SELECT replace(replace(qry_txt_quoted, '''SELECT', 'SELECT'), ')''', ')') as qry_txt FROM qryies_quoted
	),
	experiments_series as (
		SELECT run_exp_n_times(qry_txt) as exp_result FROM qryies
	)
	SELECT
		(exp_result::text::EXP_RES_T).nodes_cnt,
		(exp_result::text::EXP_RES_T).strategies,
		(exp_result::text::EXP_RES_T).costs_sum,
		(exp_result::text::EXP_RES_T).pl_time,
		(exp_result::text::EXP_RES_T).ex_time
		FROM experiments_series;


-- 100k no indeces
WITH qryies_quoted as (
	SELECT * FROM unnest('{
		''SELECT * FROM repositories_100000_1_10_false WHERE files_cnt = 1000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_1_10_false WHERE files_cnt = 1000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_3_20_false WHERE files_cnt = 1000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_3_20_false WHERE files_cnt = 1000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_10_1_false WHERE files_cnt = 1000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_10_1_false WHERE files_cnt = 1000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_20_3_false WHERE files_cnt = 1000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_20_3_false WHERE files_cnt = 1000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		}'::text[]) as qry_txt_quoted
	),
	qryies as (
		SELECT replace(replace(qry_txt_quoted, '''SELECT', 'SELECT'), ')''', ')') as qry_txt FROM qryies_quoted
	),
	experiments_series as (
		SELECT run_exp_n_times(qry_txt) as exp_result FROM qryies
	)
	SELECT
		(exp_result::text::EXP_RES_T).nodes_cnt,
		(exp_result::text::EXP_RES_T).strategies,
		(exp_result::text::EXP_RES_T).costs_sum,
		(exp_result::text::EXP_RES_T).pl_time,
		(exp_result::text::EXP_RES_T).ex_time
		FROM experiments_series;

-- 1M no indeces
WITH qryies_quoted as (
	SELECT * FROM unnest('{
		''SELECT * FROM repositories_1000000_1_10_false WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_1_10_false WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_3_20_false WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_3_20_false WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_10_1_false WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_10_1_false WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_20_3_false WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_20_3_false WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		}'::text[]) as qry_txt_quoted
	),
	qryies as (
		SELECT replace(replace(qry_txt_quoted, '''SELECT', 'SELECT'), ')''', ')') as qry_txt FROM qryies_quoted
	),
	experiments_series as (
		SELECT run_exp_n_times(qry_txt) as exp_result FROM qryies
	)
	SELECT
		(exp_result::text::EXP_RES_T).nodes_cnt,
		(exp_result::text::EXP_RES_T).strategies,
		(exp_result::text::EXP_RES_T).costs_sum,
		(exp_result::text::EXP_RES_T).pl_time,
		(exp_result::text::EXP_RES_T).ex_time
		FROM experiments_series;
