#ПБД

Перевод из строк с параметрами делается нахождением такой регулярки:
```regexp
((10+)\t(\d+)\t(\d+)\t([t|f]\w+)\t(\w+))
```
и заменой ее на строку вида
```regexp
'SELECT * FROM repositories_$2_$3_$4_$5 WHERE files_cnt = $20 $6 contributors_tsv @@ to_tsquery(''Кириллами'')'
```

		''SELECT * FROM repositories_100_1_10_true WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_1_10_true WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_3_20_true WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_3_20_true WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_10_1_true WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_10_1_true WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_20_3_true WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_20_3_true WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		
		''SELECT * FROM repositories_1000_1_10_true WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_1_10_true WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_3_20_true WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_3_20_true WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_10_1_true WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_10_1_true WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_20_3_true WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_20_3_true WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		
		''SELECT * FROM repositories_10000_1_10_true WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_1_10_true WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_3_20_true WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_3_20_true WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_10_1_true WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_10_1_true WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_20_3_true WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_20_3_true WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		
		''SELECT * FROM repositories_100000_1_10_true WHERE files_cnt = 1000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_1_10_true WHERE files_cnt = 1000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_3_20_true WHERE files_cnt = 1000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_3_20_true WHERE files_cnt = 1000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_10_1_true WHERE files_cnt = 1000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_10_1_true WHERE files_cnt = 1000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_20_3_true WHERE files_cnt = 1000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_20_3_true WHERE files_cnt = 1000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		
		''SELECT * FROM repositories_1000000_1_10_true WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_1_10_true WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_3_20_true WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_3_20_true WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_10_1_true WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_10_1_true WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_20_3_true WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_20_3_true WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		
		''SELECT * FROM repositories_100_1_10_false WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_1_10_false WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_3_20_false WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_3_20_false WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_10_1_false WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_10_1_false WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_20_3_false WHERE files_cnt = 1000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100_20_3_false WHERE files_cnt = 1000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		
		''SELECT * FROM repositories_1000_1_10_false WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_1_10_false WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_3_20_false WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_3_20_false WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_10_1_false WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_10_1_false WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_20_3_false WHERE files_cnt = 10000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000_20_3_false WHERE files_cnt = 10000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		
		''SELECT * FROM repositories_10000_1_10_false WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_1_10_false WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_3_20_false WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_3_20_false WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_10_1_false WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_10_1_false WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_20_3_false WHERE files_cnt = 100000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_10000_20_3_false WHERE files_cnt = 100000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		
		''SELECT * FROM repositories_100000_1_10_false WHERE files_cnt = 1000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_1_10_false WHERE files_cnt = 1000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_3_20_false WHERE files_cnt = 1000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_3_20_false WHERE files_cnt = 1000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_10_1_false WHERE files_cnt = 1000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_10_1_false WHERE files_cnt = 1000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_20_3_false WHERE files_cnt = 1000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_100000_20_3_false WHERE files_cnt = 1000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		
		''SELECT * FROM repositories_1000000_1_10_false WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_1_10_false WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_3_20_false WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_3_20_false WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_10_1_false WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_10_1_false WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_20_3_false WHERE files_cnt = 10000000 AND contributors_tsv @@ to_tsquery(''Кириллами'')'',
		''SELECT * FROM repositories_1000000_20_3_false WHERE files_cnt = 10000000 OR contributors_tsv @@ to_tsquery(''Кириллами'')''
		