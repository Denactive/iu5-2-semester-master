-- Возвращает целое число в заданном диапазоне значений [low, high] (включая обе границы)
CREATE OR REPLACE FUNCTION random_between(low INT , high INT) 
   RETURNS INT AS
$$
BEGIN
   RETURN floor(random()* (high-low + 1) + low);
END;
$$ language 'plpgsql' STRICT;




-- Генерирует "имена", состоящие из перемешанных случайным образом составных частей имен персонажей из мультфильмов.
-- Сгенерирует столько значений, сколько надо. Количество регулируется условием `WHERE rn <= 1000`
-- Т.к. функция - рекурсивная, если числовое значение в условии `WHERE rn <= 1000` превысит число <число строк surnames> x <число строк men_names>,
-- то появятся повторы
-- 
-- Спер отсюда https://ru.stackoverflow.com/questions/1415673/Как-реализовать-заполнение-таблицы-тестовыми-данными
-- Имена всяких персонажей https://ru.wikipedia.org/wiki/Категория:Персонажи_мультфильмов
WITH surnames as (
    SELECT * FROM
    unnest('{
		"Аврора",
		"Адриан",
		"Алая",
		"Ра’с",
		"Альтрон",
		"Анти-Веном",
		"Арним",
		"Арсенал",
		"Аттума",
		"Базз",
		"Базз",
		"Баки",
		"Багз",
		"Барбара",
		"Барон",
		"Баттхед",
		"Бета",
		"Бетти",
		"Бетти",
		"Братья",
		"Буревестник",
		"Бэтгёрл",
		"Бэтмен",
		"Вайб",
		"Вайл",
		"Доктор",
		"Веном",
		"Вервольф",
		"Верлиока",
		"Вижн",
		"Вики",
		"Вольт",
		"Вуди",
		"Вуди",
		"Галактус",
		"Гамби",
		"Гамбит",
		"Гамора",
		"Гаргамель",
		"Гарокк",
		"Геракл",
		"Говорун",
		"Горгон",
		"Джеймс",
		"Громовержец",
		"Громозека",
		"Грут",
		"Груффало",
		"Грю",
		"Губка Боб",
		"Гуфи",
		"Даффи",
		"Двуликий",
		"Девушка-белка",
		"Дед",
		"Дейзи",
		"Детлок",
		"Джей",
		"Джейсон",
		"Джек",
		"Джерри",
		"Джесси",
		"Джессика",
		"Джимми",
		"Джокер",
		"Джулия",
		"Дик",
		"Динь-Динь",
		"Док",
		"Доктор",
		"Доктор",
		"Домино",
		"Домо-кун",
		"Дормамму",
		"Дракс",
		"Дракула",
		"Друпи",
		"Дюймовочка",
		"Дядя",
		"Дядя",
		"Ежи",
		"Енот",
		"Жан",
		"Жёлтый",
		"Жук",
		"Загадочник",
		"Звёздный",
		"Зелёный",
		"Зелёный",
		"Принцесса",
		"Злая",
		"Зловещая шестёрка",
		"Калипсо",
		"Гарри",
		"Капитан",
		"Капитан",
		"Капитошка",
		"Карнак",
		"Харли",
		"Киборг",
		"Кидагакаш",
		"Кинг-Конг",
		"Клёпа",
		"Колосс",
		"Корсар",
		"Кот",
		"Красный",
		"Красный",
		"Кристалл",
		"Кролик",
		"Кэрол",
		"Легион",
		"Леший",
		"Лидер",
		"Лоис",
		"Лола",
		"Люди",
		"Лекс",
		"Магнето",
		"Майкрофт",
		"Макс",
		"Маленький Лисёнок",
		"Маринетт",
		"Мария",
		"Марсупилами",
		"Кот",
		"Медуза",
		"Металло",
		"Микки",
		"Миньоны",
		"Мисс",
		"Мистер",
		"Мишка",
		"Моана",
		"Мого",
		"Рене",
		"Майлз",
		"Морбиус",
		"Мэджик",
		"Найтвинг",
		"Ехидна",
		"Нова",
		"Нова",
		"Нова",
		"Носорог",
		"Нэмор",
		"Огненная",
		"Ослепительная",
		"Патрик",
		"Пепе",
		"Пеппи",
		"Пересмешница",
		"Перл",
		"Пингвин",
		"Пинки",
		"Планктон",
		"Плащ",
		"Плуто",
		"Покатигорошек",
		"Полярис",
		"Моряк",
		"Почтальон",
		"Призрачный",
		"Призрачный",
		"Пугало",
		"Пузырь",
		"Питер",
		"Радуга",
		"Рапунцель",
		"Рарити",
		"Рейстлин",
		"Робин",
		"Робин",
		"Розовая",
		"Ронан",
		"Ртуть",
		"Рудольф",
		"Рэйман",
		"Саблезубый",
		"Асами",
		"Алиса",
		"Серебряный",
		"Скрат",
		"Смурфетта",
		"Снегурочка",
		"Соколиный",
		"Ёж",
		"Спиди",
		"Стервятник",
		"Барсучиха"
	}'::text[]) surname
),
men_names as (
    SELECT * FROM
    unnest('{
		"Агрест",
		"Ведьма",
		"аль Гул",
		"Зола",
		"Баззард",
		"Лайтер",
		"Барнс",
		"Банни",
		"Гордон",
		"Мордо",
		"Бета Билл",
		"Буп",
		"Росс",
		"Колобки",
		"Вайл",
		"Ватсон",
		"Вэйл",
		"Вудпекер",
		"Гордон",
		"Росс",
		"Квадратные Штаны",
		"Дак",
		"Мороз",
		"Джонсон",
		"Боб",
		"Тодд",
		"Скеллингтон",
		"Дрю",
		"Нейтрон",
		"Карпентер",
		"Грейсон",
		"Динь-",
		"Самсон",
		"Ливси",
		"Эггман",
		"Домо-",
		"Разрушитель",
		"Бен",
		"Фёдор",
		"Петруччо",
		"Ракета",
		"Паспарту",
		"шершень",
		"Лорд",
		"гоблин",
		"Фонарь",
		"Зельда",
		"королева",
		"шестёрка",
		"Каллахан",
		"Марвел",
		"Пронин",
		"Квинн",
		"Фриц",
		"колпак",
		"Череп",
		"Освальд",
		"Дэнверс",
		"Лейн",
		"Банни",
		"Икс",
		"Лютор",
		"Холмс",
		"Гуф",
		"Лисёнок",
		"Год",
		"Хилл",
		"Матроскин",
		"Маус",
		"Марвел",
		"Магу",
		"Йоги",
		"Монтойя",
		"Моралес",
		"Наклз",
		"звезда",
		"Стар",
		"Длинныйчулок",
		"Крабс",
		"Пай",
		"Кинжал",
		"Попай",
		"Печкин",
		"гонщик",
		"Пэн",
		"Дэш",
		"Маджере",
		"Гуд",
		"пантера",
		"Обвинитель",
		"Сато",
		"Селезнёва",
		"Сёрфер",
		"Соболь",
		"глаз",
		"Соник",
		"Гонзалес",
		"Стикс"
	}' ::text[]) forename
),
men as (
  SELECT
    men_names.forename as firstName,
    surname,
    row_number() OVER (ORDER BY md5(concat(men_names.forename, surname)::bytea)) rn
  FROM men_names
  CROSS JOIN surnames
),
repos as (
  SELECT rn, concat(firstName, concat(' ', surname)) as name_val, random_between(10, 100) as files_cnt_val, random_between(10, 10000) as total_size_kb_val FROM men WHERE rn <= 1000
)
-- INSERT INTO "REPOSITORIES" ("NAME", files_cnt, total_size_kb)
SELECT name_val, files_cnt_val, total_size_kb_val FROM repos;




-- Функция удаления дубликатов
-- Было проще удалить строки-дубликаты руками, чем написать это.
-- К ловеркейсу не приводит
CREATE OR REPLACE FUNCTION remove_duplicates(arr text[])
RETURNS text AS
	$$
	DECLARE
		res text[];
	BEGIN
		SELECT ARRAY(SELECT DISTINCT * FROM unnest(arr)) INTO res;
		RETURN res;
	END;
	$$ LANGUAGE plpgsql;

SELECT remove_duplicates(ARRAY['foo', 'bar', 'foo', 'baz']);



-- Возвращает случайное значение из списка. Исключает дубликаты без учета начертания
-- Работает со строками, хотя можно переписать на любой тип данных.
CREATE OR REPLACE FUNCTION get_random_word()
RETURNS text AS
	$$
	DECLARE
		idx integer;
		len integer;
		res text;
		dict text[] := remove_duplicates('{
			"я",
			"волком",
			"бы",
			"выгрыз",
			"бюрократизм",
			"к",
			"мандатам",
			"почтения",
			"нету",
			"к",
			"любым",
			"чертям",
			"с",
			"матерями",
			"катись",
			"любая",
			"бумажка",
			"но",
			"эту",
			"по",
			"длинному",
			"фронту",
			"купе",
			"и",
			"кают",
			"чиновник",
			"учтивый",
			"движется",
			"сдают",
			"паспорта",
			"и",
			"я",
			"сдаю",
			"мою",
			"пурпурную",
			"книжицу",
			"к",
			"одним",
			"паспортам",
			"улыбка",
			"у",
			"рта",
			"к",
			"другим",
			"отношение",
			"плевое",
			"с",
			"почтеньем",
			"берут",
			"например",
			"паспорта",
			"с",
			"двухспальным",
			"английским",
			"левою",
			"глазами",
			"доброго",
			"дядю",
			"выев",
			"не",
			"переставая",
			"кланяться",
			"берут",
			"как",
			"будто",
			"берут",
			"чаевые",
			"паспорт",
			"американца",
			"на",
			"польский",
			"глядят",
			"как",
			"в",
			"афишу",
			"коза",
			"на",
			"польский",
			"выпяливают",
			"глаза",
			"в",
			"тугой",
			"полицейской",
			"слоновости",
			"откуда",
			"мол",
			"и",
			"что",
			"это",
			"за",
			"географические",
			"новости",
			"и",
			"не",
			"повернув",
			"головы",
			"кочан",
			"и",
			"чувств",
			"никаких",
			"не",
			"изведав",
			"берут",
			"не",
			"моргнув",
			"паспорта",
			"датчан",
			"и",
			"разных",
			"прочих",
			"шведов",
			"и",
			"вдруг",
			"как",
			"будто",
			"ожогом",
			"рот",
			"скривило",
			"господину",
			"это",
			"господин",
			"чиновник",
			"берет",
			"мою",
			"краснокожую",
			"паспортину",
			"берет",
			"как",
			"бомбу",
			"берет",
			"как",
			"ежа",
			"как",
			"бритву",
			"обоюдоострую",
			"берет",
			"как",
			"гремучую",
			"в",
			"двадцать",
			"жал",
			"змею",
			"двухметроворостую",
			"моргнул",
			"многозначаще",
			"глаз",
			"носильщика",
			"хоть",
			"вещи",
			"снесет",
			"задаром",
			"вам",
			"жандарм",
			"вопросительно",
			"смотрит",
			"на",
			"сыщика",
			"сыщик",
			"на",
			"жандарма",
			"с",
			"каким",
			"наслажденьем",
			"жандармской",
			"кастой",
			"я",
			"был",
			"бы",
			"исхлестан",
			"и",
			"распят",
			"за",
			"то",
			"что",
			"в",
			"руках",
			"у",
			"меня",
			"молоткастый",
			"серпастый",
			"советский",
			"паспорт",
			"я",
			"волком",
			"бы",
			"выгрыз",
			"бюрократизм",
			"к",
			"мандатам",
			"почтения",
			"нету",
			"к",
			"любым",
			"чертям",
			"с",
			"матерями",
			"катись",
			"любая",
			"бумажка",
			"но",
			"эту",
			"я",
			"достаю",
			"из",
			"широких",
			"штанин",
			"дубликатом",
			"бесценного",
			"груза",
			"читайте",
			"завидуйте",
			"я",
			"гражданин",
			"советского",
			"союза"
		}'::text[]);
	BEGIN
		SELECT array_length(dict, 1) INTO len;
		idx := random_between(1, len);
		SELECT (dict)[idx] INTO res; -- Запись SELECT INTO аналогична оператору присвоения `:=`
		RETURN res;
	END;
	$$ LANGUAGE plpgsql;

SELECT get_random_word();



-- Генерирует таблицу с двумя полями с заданной селективностью в запросах с условием:
-- для числа:  `WHERE fieldname = 10 * size_`
-- для текста: `WHERE fieldname LIKE '%Кириллов`
DROP FUNCTION generate_table_with_fixed_selectivities;
CREATE OR REPLACE FUNCTION generate_table_with_fixed_selectivities(num_selectivity real, text_selectivity real, size_ integer)
RETURNS TABLE(files_cnt integer, contributors text)
	LANGUAGE plpgsql AS
	$$
 	DECLARE
		-- Константы вместо аргументов, если нужно
		-- num_selectivity real := 0.2;
		-- text_selectivity real := 0.1;
		-- size_ integer := 20;
		--
		num_cnt integer := 0;
		text_cnt integer := 0;
	BEGIN
		-- Задание seed для ГСЧ гарантирует одинаковый результат от прогона к прогону
 		EXECUTE setseed(0.005);
		
		FOR i IN 1..size_ LOOP
			-- Генерируем число
			
			-- random() < num_selectivity с вероятностью num_selectivity
			-- И
			-- num_cnt < num_selectivity * size_ - если счетчик уже добавленных специфичных значений еще не превышен
			-- ИЛИ
			-- сложилась ситуация, что из-за случайности в конце может не хватить пары значений, чтобы набрать нужное количество
			IF (random() < num_selectivity AND num_cnt < num_selectivity * size_) OR (size_ - i + 1 < num_selectivity * size_ - num_cnt) THEN
				files_cnt := 10 * size_; -- Заданное специфичное значение (вне диапазона 1, size_)
				num_cnt = num_cnt + 1;
			ELSE
				files_cnt := random_between(1, size_);
			END IF;
			
			-- Генерируем строку
			-- В целом, аналогично, но тексты отличаются наличием особой лексемы
			contributors := concat(get_random_word(), ' ', get_random_word(), ' ', get_random_word());
			IF (random() < text_selectivity AND text_cnt < text_selectivity * size_) OR (size_ - i + 1 < text_selectivity * size_ - text_cnt) THEN
				contributors := concat(contributors, ' Кириллов'); -- Специфичная лексема
				text_cnt := text_cnt + 1;
			END IF;
			RETURN NEXT;
		END LOOP;
	END;
	$$;

-- Проверка селективности в запросах на равенство специфичному значению
SELECT * FROM generate_table_with_fixed_selectivities(0.2, 0.1, 20);
SELECT count(*) FROM generate_table_with_fixed_selectivities(0.2, 0.1, 20) WHERE files_cnt = 200;
SELECT count(*) FROM generate_table_with_fixed_selectivities(0.2, 0.1, 20) WHERE contributors LIKE '%Кириллов';

-- Проверка селективности в запросах на полнотекстовый поиск
SET default_text_search_config = russian;

-- Базовый запрос:
--     SELECT to_tsvector(contributors) as doc_tsv FROM generate_table_with_fixed_selectivities(0.2, 0.1, 20);
-- Его надо превратить в tsvector. Он станет подзапросом
SELECT * FROM (
	SELECT *, to_tsvector(contributors) as doc_tsv FROM generate_table_with_fixed_selectivities(0.2, 0.1, 20)
) as subquery WHERE doc_tsv @@ to_tsquery('Кириллами');
SELECT count(*) FROM (
	SELECT *, to_tsvector(contributors) as doc_tsv FROM generate_table_with_fixed_selectivities(0.2, 0.1, 20)
) as subquery WHERE doc_tsv @@ to_tsquery('Кириллами');





-- Вот так пользоваться функцией, чтобы заполнить таблицу - через рекурсивный запрос
-- Все нужные преобразования над результатом функции производятся в запросе `prepared_selectivity`
WITH selectivity as (
	SELECT *, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) as rn1 FROM generate_table_with_fixed_selectivities(0.2, 0.1, 1000)
),
prepared_selectivity as (
	SELECT 'Repo No.' || rn1::varchar(255) as name, to_tsvector(contributors) as contributors_tsv, * FROM selectivity
)
INSERT INTO repositories (name, contributors, contributors_tsv, files_cnt)
SELECT name, contributors, contributors_tsv, files_cnt FROM prepared_selectivity;








-- основная процедура/функция для создания и заполнения таблицы для единичного эксперимента
CREATE OR REPLACE FUNCTION create_and_fill_table(num_sel real, text_sel real, size_ integer, indexes boolean)
RETURNS integer AS
$$
DECLARE
  -- Имя таблицы имеет вид `repositories_1000_10_20_true`
  table_name text := 'repositories_' || size_ || '_' || (num_sel * 100)::integer::text || '_' || (text_sel * 100)::integer::text || '_' || indexes::text;
  btree_index_name text :='bree_idx_' || size_ || '_' || (num_sel * 100)::integer::text || '_' || (text_sel * 100)::integer::text || '_' || indexes::text;
  gin_index_name text :='gin_idx_' || size_ || '_' || (num_sel * 100)::integer::text || '_' || (text_sel * 100)::integer::text || '_' || indexes::text;
  size_validation integer;
BEGIN
  EXECUTE format('DROP TABLE IF EXISTS %s', table_name);
  EXECUTE format('
	 CREATE TABLE %s (
		id serial PRIMARY KEY NOT NULL,
    name varchar(255) NOT NULL,
    contributors text,
    contributors_tsv tsvector,
    files_cnt integer DEFAULT 0
	)', table_name);
  EXECUTE format('
	  WITH selectivity as (
		  SELECT *, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) as rn1 FROM generate_table_with_fixed_selectivities(%s, %s, %s)
	  ),
	  prepared_selectivity as (
		  SELECT ''Repo No.'' || rn1::varchar(255) as name, to_tsvector(contributors) as contributors_tsv, * FROM selectivity
	  )
	  INSERT INTO %s (name, contributors, contributors_tsv, files_cnt)
	  SELECT name, contributors, contributors_tsv, files_cnt FROM prepared_selectivity;
  ', num_sel, text_sel, size_, table_name);

  IF indexes THEN
  	EXECUTE format('
      DROP INDEX IF EXISTS %s;
      -- B-tree
      CREATE INDEX %s ON %s (files_cnt);
    ', btree_index_name, btree_index_name, table_name);
    EXECUTE format('
      DROP INDEX IF EXISTS %s;
      -- GIN
      CREATE INDEX %s ON %s  USING GIN (contributors_tsv);
    ', gin_index_name, gin_index_name, table_name);
  END IF;
  
  EXECUTE format('
  	SELECT count(*) FROM %s
  ', table_name) INTO size_validation;
  RETURN size_validation;
END
$$ LANGUAGE 'plpgsql';


-- Тест
-- Если бы не захотел возращать число, была бы процедурой
-- CALL create_and_fill_table(0.1, 0.2, 1000, true);
SELECT create_and_fill_table(0.1, 0.2, 1000, true); -- Выведет 1000
SELECT count(*) FROM repositories_1000_10_20_true;
