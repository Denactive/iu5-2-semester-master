DROP TABLE IF EXISTS "USERS" CASCADE
;

DROP TABLE IF EXISTS "CONTRIBUTORS" CASCADE
;

DROP TABLE IF EXISTS "ROLES" CASCADE
;

CREATE TABLE "USERS"
(
	"ID" serial PRIMARY KEY NOT NULL,
	"LOGIN" varchar(255) NOT NULL,
	"PASSWORD" varchar(255) NOT NULL,
	"FIO" varchar(255) NOT NULL,
	"AGE" int NOT NULL CONSTRAINT age_adult CHECK ("AGE" >= 18) 
)
;

CREATE TABLE "ROLES"
(
	"ID" serial PRIMARY KEY NOT NULL,
	"NAME" varchar(255) NOT NULL,
	"ACCESS_RIGHTS" varchar(255) NOT NULL,
	"IS_STAFF" boolean NOT NULL DEFAULT FALSE
)
;

CREATE TABLE "CONTRIBUTORS"
(
	"ID" serial PRIMARY KEY NOT NULL,
	"CREATED" timestamp NOT NULL,
	"ROLE_ID" int NOT NULL,
	"USER_ID" int NOT NULL,
	CONSTRAINT "FK_ROLE" FOREIGN KEY("ROLE_ID") REFERENCES "ROLES"("ID"),
	CONSTRAINT "FK_USER" FOREIGN KEY("USER_ID") REFERENCES "USERS"("ID")
)
;

INSERT INTO "ROLES"("NAME", "ACCESS_RIGHTS", "IS_STAFF") VALUES ('manager', 'view, create_repo, add_users, run_pipeline', TRUE);
INSERT INTO "ROLES"("NAME", "ACCESS_RIGHTS", "IS_STAFF") VALUES ('developer', 'view, push, run_pipeline', TRUE);
INSERT INTO "ROLES"("NAME", "ACCESS_RIGHTS", "IS_STAFF") VALUES ('sre', 'view', FALSE);

INSERT INTO "USERS"("LOGIN", "PASSWORD", "FIO", "AGE") VALUES ('dskirillov', 'denis', 'Кириллов Д. С.', 18);

INSERT INTO "CONTRIBUTORS"("CREATED", "ROLE_ID", "USER_ID") VALUES ((NOW()), (SELECT "ID" FROM "ROLES" WHERE "NAME"='manager'), (SELECT "ID" FROM "USERS" WHERE "LOGIN"='dskirillov'));

-- 3
-- default
-- INSERT INTO "ROLES"("NAME", "ACCESS_RIGHTS") VALUES ('sre', 'view'); -- "IS_STAFF" = false
-- SELECT * FROM "ROLES" WHERE "NAME"='sre';

-- 4
-- UPDATE "USERS" SET "AGE"=19 WHERE "LOGIN"='dskirillov';
-- SELECT * FROM "USERS"
-- INSERT INTO "ROLES"("NAME", "ACCESS_RIGHTS") VALUES ('role_to_delete', 'view');
-- SELECT * FROM "ROLES"
-- DELETE FROM "ROLES" WHERE "NAME"='role_to_delete';
-- SELECT * FROM "ROLES"
-- SELECT * FROM "ROLES" WHERE "IS_STAFF"=TRUE;

-- 5
ALTER TABLE "USERS" ADD COLUMN "VECTOR" lseg;
UPDATE "USERS" SET "VECTOR"=lseg '((0,0),(1,1))' WHERE "VECTOR" IS NULL;
-- SELECT * FROM "USERS";

-- Пересечение (#) работает только с прямоугольниками и прямыми
-- SELECT box '((1,-1),(-1,1))' # box '((1,1),(-2,-2))';
-- SELECT circle'((0,0),2)' # circle '((1,1),2)'

ALTER TABLE "USERS" ADD COLUMN "ACCESS_LOG" json;
UPDATE "USERS" SET "ACCESS_LOG"='{"last_visit": "2024-03-01","is_active": true}' WHERE "ACCESS_LOG" IS NULL;
-- SELECT * FROM "USERS";

ALTER TABLE "USERS" ADD COLUMN "LAST_LOG_SERIES" tsrange;
UPDATE "USERS" SET "LAST_LOG_SERIES"='[2024-02-29 14:30, 2024-03-01 15:30)' WHERE "LAST_LOG_SERIES" IS NULL;
-- SELECT * FROM "USERS";

SELECT * FROM "USERS" WHERE date("ACCESS_LOG"->>'last_visit')>date('2024-02-29');
SELECT length("VECTOR") FROM "USERS";
SELECT lower("LAST_LOG_SERIES") FROM "USERS";

-- 6
ALTER TABLE "ROLES" ALTER COLUMN "ACCESS_RIGHTS" TYPE text[] USING '{"view"}';
UPDATE "ROLES" SET "ACCESS_RIGHTS"='{"view", "manager", "create_repo", "add_users", "run_pipeline"}' WHERE "NAME"='manager';
UPDATE "ROLES" SET "ACCESS_RIGHTS"='{"view", "developer", "push", "run_pipeline"}' WHERE "NAME"='developer';
-- SELECT * FROM "ROLES";
SELECT "ACCESS_RIGHTS"[1] as "view", "ACCESS_RIGHTS"[2] as "rights_name", "ACCESS_RIGHTS"[3] as "main_action" FROM "ROLES";


-- 7

DROP TYPE IF EXISTS ACCESS_RIGHTS_FLAGS_T;
CREATE TYPE ACCESS_RIGHTS_FLAGS_T AS (
  can_maintain boolean,
  can_push boolean,
  can_view boolean,
  can_run_pipeline boolean
);

-- Так внутри USING не работает =( Но внутри INSERT работает
-- ALTER TABLE "ROLES" ALTER COLUMN "ACCESS_RIGHTS" TYPE ACCESS_RIGHTS_FLAGS_T USING '{"can_maintain": false, "can_push": false, "can_run_pipeline": false, "can_view": true}';
ALTER TABLE "ROLES" ALTER COLUMN "ACCESS_RIGHTS" TYPE ACCESS_RIGHTS_FLAGS_T USING '(false, false, false, true)';

UPDATE "ROLES" SET "ACCESS_RIGHTS" = ROW(True, True, True, True) WHERE "NAME"='manager';
UPDATE "ROLES" SET "ACCESS_RIGHTS" = ROW(True, True, True, True) WHERE "NAME"='developer';
UPDATE "ROLES" SET "ACCESS_RIGHTS"."can_maintain" = False WHERE "NAME"='developer';

-- SELECT * FROM "ROLES";
SELECT * FROM "ROLES" WHERE ("ACCESS_RIGHTS")."can_push" = True;

-- 8
DROP TYPE IF EXISTS ROLES_E;
CREATE TYPE ROLES_E AS ENUM ('manager', 'developer', 'sre');
ALTER TABLE "ROLES" ALTER COLUMN "NAME" TYPE ROLES_E USING "NAME"::roles_e;

-- INSERT INTO "ROLES"("NAME", "ACCESS_RIGHTS", "IS_STAFF") VALUES ('admin', ROW(True, True, True, True), TRUE); -- Ошибка

-- 9
CREATE TABLE "USERS_WITH_STATISTICS" (
	commits int,
	commits_last_month int,
	commits_current_month int
) INHERITS ("USERS");

INSERT INTO "USERS_WITH_STATISTICS"("LOGIN", "PASSWORD", "FIO", "AGE", "commits", "commits_last_month", "commits_current_month") VALUES ('sv', 'svetlana', 'Очеретная С.В.', 18, 500, 100, 0);
-- SELECT * FROM "USERS";
-- SELECT * FROM "USERS_WITH_STATISTICS";
SELECT * FROM ONLY "USERS";

-- Наследуются ограничения CHECK
-- INSERT INTO "USERS_WITH_STATISTICS"("LOGIN", "PASSWORD", "FIO", "AGE", "commits", "commits_last_month", "commits_current_month") VALUES ('dskirillov', 'denis', 'Кириллов Д. С.', 16, 0, 0, 0); -- Ошибка
-- SELECT * FROM "USERS";

-- Защита. По 1 заданию на каждый пункт

DO $$ 
    BEGIN
        -- Защита.1 Модифицировать CONSTRAINT через ALTER TABLE
		ALTER TABLE "CONTRIBUTORS"
		DROP CONSTRAINT "FK_ROLE";

		ALTER TABLE "CONTRIBUTORS" 
		ADD CONSTRAINT "FK_ROLE" 
		FOREIGN KEY ("ROLE_ID") 
		REFERENCES "ROLES"("ID")
		ON DELETE CASCADE;

		-- Защита.2
		-- DDL сценарии к созданным таблицам.
		-- Добавить UNIQUE CONSTRAINT в таблицу через UI.

		-- Защита.3
		-- Заполнить таблицу данными через UI.

		-- Защита.4
		-- Вектор и его длина двумя способами
		select "VECTOR", length("VECTOR") from "USERS";
		-- select "VECTOR", sqrt(("VECTOR"[0][0] - "VECTOR"[1][0]) * ("VECTOR"[0][1] - "VECTOR"[1][1])) from "USERS";
		select "VECTOR", "VECTOR"[1] <-> "VECTOR"[0] from "USERS";

		-- Защита.5
		select ("ACCESS_LOG"->>'last_visit') from "USERS";

		-- Защита.6
		-- Пересечь range
		select ('[2024-03-01 14:30, 2024-03-01 15:30)')::tsrange as int1, "LAST_LOG_SERIES", ('[2024-03-01 14:30, 2024-03-01 15:30)')::tsrange * "LAST_LOG_SERIES" as inter from "USERS";

		-- Защита.7
		-- Обращение к элементам массива, длина, последний элемент
		SELECT "ACCESS_RIGHTS", "ACCESS_RIGHTS"[array_length("ACCESS_RIGHTS", 1)] FROM "ROLES" WHERE array_length("ACCESS_RIGHTS", 1) > 1;

		-- Защита.8
		-- Фильтр и UPDATE полей структуры. ВНИМАНИЕ НА СКОБКИ! в SELECT/UPDATE части они не нужны, а внутри WHERE нужны. И с кавычками тоже. ПОЛНЫЙ БРЕД
		SELECT * FROM "ROLES" WHERE NOT ("ACCESS_RIGHTS")."can_maintain" AND ("ACCESS_RIGHTS").can_push; 
		UPDATE "ROLES" SET "ACCESS_RIGHTS".can_view = False WHERE NOT ("ACCESS_RIGHTS").can_maintain AND ("ACCESS_RIGHTS").can_push;

		-- Защита.9
		-- После наследования в базовой таблице USERS присутствуют строки из расширенной таблицы USERS_WITH_STATISTICS. Сделать SELECT на USERS так, чтобы отображались только строки, принадлежащие USERS, и не было строк, принадлежащих USERS_WITH_STATISTICS
		-- SELECT * FROM "USERS";
		-- SELECT * FROM "USERS_WITH_STATISTICS";
		SELECT * FROM ONLY "USERS";
    END;
$$;
