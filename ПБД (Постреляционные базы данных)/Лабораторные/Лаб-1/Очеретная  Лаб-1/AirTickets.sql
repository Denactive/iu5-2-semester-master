-- Удаление таблиц
DROP TABLE IF EXISTS "Users" CASCADE;
DROP TABLE IF EXISTS "LuggageVariants" CASCADE;
DROP TABLE IF EXISTS "Flights" CASCADE;
DROP TABLE IF EXISTS "Tickets" CASCADE;
DROP TABLE IF EXISTS "Passengers" CASCADE;

-- Вывод таблиц
select * from "Users";
select * from "LuggageVariants";
select * from "Tickets";

select * from "Passengers";
select * from "Flights";

-- Создание таблиц
CREATE TABLE "Users"
(
	"userID" serial PRIMARY KEY NOT NULL,
	"login" varchar(255) NOT NULL UNIQUE,
	"password" varchar(255) NOT NULL
);

CREATE TABLE "LuggageVariants"
(
	"luggageVariantID" integer PRIMARY KEY NOT NULL AUTO_INCREMENT,
	"cost" real NOT NULL CHECK ("cost" > 0),
	"maxWeight" integer NOT NULL DEFAULT 0
);

CREATE TABLE "Tickets"
(
	"ticketID" serial PRIMARY KEY NOT NULL,
	"purchased" boolean NOT NULL DEFAULT false,
	"booked" boolean NOT NULL DEFAULT false,
	
	"luggageVariantID" integer,
	"userID" integer,
	
	CONSTRAINT "FK_Tickets_LuggageVariants" FOREIGN KEY("luggageVariantID") REFERENCES "LuggageVariants"("luggageVariantID"),
	CONSTRAINT "FK_Tickets_Users" FOREIGN KEY("userID") REFERENCES "Users"("userID") --ON UPDATE CASCADE ON DELETE CASCADE 
);

-- Заполнение таблиц
INSERT INTO "Users"("userID", "login", "password") VALUES (1, 'svetlanlka', '123');
INSERT INTO "Users"("userID", "login", "password") VALUES (2, 'user2', '456');
INSERT INTO "Users"("userID", "login", "password") VALUES (3, 'user3', '789');
INSERT INTO "Users"("userID", "login", "password") VALUES (4, 'user4', '342');

INSERT INTO "LuggageVariants"("cost", "maxWeight") VALUES (5000, 40);
INSERT INTO "LuggageVariants"("cost", "maxWeight") VALUES (8000, 80);
INSERT INTO "LuggageVariants"("cost", "maxWeight") VALUES (2800, 20);
INSERT INTO "LuggageVariants"("cost", "maxWeight") VALUES (1600, 10);
INSERT INTO "LuggageVariants"("cost") VALUES (1000);
INSERT INTO "LuggageVariants"("cost") VALUES (2000);

INSERT INTO "Tickets"("purchased", "booked", "luggageVariantID", "userID") VALUES (false, false, 5, 1);
INSERT INTO "Tickets"("purchased", "booked", "luggageVariantID", "userID") VALUES (true, true, 6, 1);
INSERT INTO "Tickets"("purchased", "booked", "luggageVariantID", "userID") VALUES (false, true, 7, 1);
INSERT INTO "Tickets"("purchased", "booked", "luggageVariantID", "userID") VALUES (true, true, 7, 2);
INSERT INTO "Tickets"("purchased", "booked", "luggageVariantID", "userID") VALUES (true, true, 8, 3);
INSERT INTO "Tickets"("purchased", "booked", "luggageVariantID", "userID") VALUES (false, true, 17, 3);
INSERT INTO "Tickets"("purchased", "booked", "luggageVariantID", "userID") VALUES (false, true, 5, 4);
INSERT INTO "Tickets"("purchased", "booked", "luggageVariantID", "userID") VALUES (true, true, 6, 4);

-- Редактирование записи в таблице
UPDATE "LuggageVariants" SET "maxWeight"=100 WHERE "maxWeight">35;

-- Удаление записи из таблицы
DELETE FROM "Tickets" WHERE "luggageVariantID"=2;

-- Выборка с группировкой
SELECT COUNT("purchased"),"userID"
FROM "Tickets"
GROUP BY "userID";

-- Добавление данных в таблицу Flights 
-- (геометрический тип flightPath, JSON тип companyData, диапазон дат flightDate, массив busyPlaces)
INSERT INTO "Flights"("flightPath", "companyData", "flightDate")
VALUES (
  path(
    '(55.967938990379864, 37.41668736110115),
     (45.00513385390844, 37.3486931665311),
	 (45.04128405787348, 33.98052486361324)'
  ),
	'{"name": "S7 Airlines","location": "Москва","phoneNumber": "89161234567"}',
	'[2024-02-21, 2024-02-22]'
);
INSERT INTO "Flights"("flightPath", "companyData", "flightDate", "busyPlaces")
VALUES (
  path(
    '(45.342, 37.234),
     (47.023, 40.645),
	 (48.222, 40.656)'
  ),
	'{"name": "S8 Airlines","location": "Санкт-Петербург","phoneNumber": "89161678967"}',
	'[2024-02-23, 2024-02-24]',
	ARRAY[1,2,4]
);

-- вспомогательные действия
select * from "Flights";
select "flightID", "busyPlaces" from "Flights";
select "flightID", "airplaneData" from "Flights";
DELETE FROM "Flights" WHERE "flightID"=2;

-- вывод площади прямоугольника
select area(box '(1,2),(3,4)');

-- вывод поля JSON типа с обращением к полю JSON типа
select "companyData"->>'name' from "Flights" where "companyData"->>'location'='Москва';

-- Массивы
-- добавление данных
INSERT INTO "Flights"("busyPlaces") VALUES (ARRAY[1,3,4,5]);
-- редактирование данных
UPDATE "Flights" SET "busyPlaces"='{4, 5, 6}' WHERE "flightID"=3;
-- вывод данных по индексам
SELECT "flightID", "busyPlaces"[1], "busyPlaces"[2] FROM "Flights";
-- вывод длин массивов
SELECT array_length("busyPlaces", 1) AS busyPlaces_count FROM "Flights";
-- преобразование массива в набор строк
SELECT unnest("busyPlaces") FROM "Flights" WHERE "flightID"=3;
-- добавление элемента в массив
UPDATE "Flights" SET "busyPlaces" =array_append("busyPlaces", 7) WHERE "flightID"=3;
-- удаление элемента из массива
UPDATE "Flights" SET "busyPlaces" =array_remove("busyPlaces", 4) WHERE "flightID"=3;
-- проверка наличия элемента в массиве
SELECT "flightID", "busyPlaces" FROM "Flights" WHERE 1 = ANY ("busyPlaces");


-- Структуры
-- создание
CREATE TYPE Airplane AS (
 name text,
 placesCount integer,
 speed numeric,
 maxFlightHeight numeric,
 weightLimit numeric
);

-- добавление записи в таблицу со столбцом структурного типа
INSERT INTO "Flights"("flightPath", "companyData", "flightDate", "busyPlaces", "airplaneData") VALUES (
  path('(35.342, 27.234),(37.023, 30.645),(38.222, 30.656)'),
	'{"name": "S9 Airlines","location": "Санкт-Петербург","phoneNumber": "89161234567"}',
	'[2024-02-25, 2024-02-26]',
	ARRAY[1,2],
	('Boeing-767', 229, 873, 10668, 158000)
);
-- редактирование данных столбца структурного типа
UPDATE "Flights"  SET "airplaneData" = ROW('Boeing-777', 235, 891, 10668, 242600) WHERE "flightID"=5;
-- редактирование поля столбца-структуры
UPDATE "Flights"  SET "airplaneData".placesCount = 219 WHERE "flightID"=6;
-- вывод столбца-структуры
SELECT "airplaneData" FROM "Flights";
-- вывод поля столбца-структуры с условием на другое поля столбца-структуры
SELECT ("airplaneData").name FROM "Flights"  WHERE ("airplaneData").weightLimit > 200000;


-- перечисления
-- создание
CREATE TYPE Country AS ENUM ('Russia', 'France', 'Italy', 'China', 'Japan', 'Egypt', 'Turkey');
-- редактирование
UPDATE "Flights" SET "destinationCountry"='' WHERE "flightID"=4;
-- неверное редактирование
UPDATE "Flights" SET "destinationCountry"='Transylvania' WHERE "flightID"=4;
-- вывод
SELECT "flightID", "destinationCountry" FROM "Flights";


-- производная (дочерняя) таблица
-- создание
CREATE TABLE "Passengers" (
	name text,
	surname text,
	age integer,
	gender boolean,
	dateBirthday date,
	country text,
	passportSeries text,
	passportNumber text
) INHERITS ("Users");


-- добавление записи в производную таблицу
INSERT INTO "Passengers"("login", "password", 
	name, surname, age, gender, dateBirthday, country, passportSeries, passportNumber) 
VALUES ('user4', '111', 'Петр', 'Петров', 75, true, '1949-01-01', 'Russia', '1111', '121212');
-- добавление записи в базовую таблицу
INSERT INTO "Users"("login", "password") VALUES ('user5', '222');

-- выборка строк только из базовой таблицы (без записей производной)
select * FROM ONLY "Users";
-- выборка из базовой (родительской) таблицы
select * from "Users";
-- выборка из производной (дочерней) таблицы
select * from "Passengers";
-- выборка из дочерней таблицы по условию
select "userID", name, surname from "Passengers" where length("password") > 3;
-- выборка из базовой таблицы по условию
select "userID", "login" from "Users" where length("password") > 3;

-- редактирование производной таблицы
UPDATE "Passengers" SET age=23 WHERE "userID"=4;
-- редактирование базовой таблицы через производную
UPDATE "Passengers" SET "password"='1122' WHERE "userID"=4;
-- удаление записи базовой таблицы
DELETE FROM "Users" WHERE "userID"=4;
-- удаление записи производной таблицы
DELETE FROM "Passengers" WHERE "userID"=4;

-- для примеров лаб2
INSERT INTO "Passengers" ("userID", "login", "password", 
	name, surname, age, gender, dateBirthday, country, passportSeries, passportNumber) 
VALUES (1, 'svetlanka', 'sveta123', 'Светлана', 'Очеретная', 21, true, '2002-08-10', 'Russia', '1331', '133332');
delete from "Users" where "login"='svetlanlka';
INSERT INTO "Passengers" ("login", "password", 
	name, surname, age, gender, dateBirthday, country, passportSeries, passportNumber) 
VALUES ('child', 'baby123', 'Владик', 'Иванов', 5, true, '2019-02-10', 'Russia', '1321', '133232');
--============================================================================--

-- Лабораторная №2

-- скалярные функции (возвращают единичное значение)
-- пример простой функции без обращения к таблице
CREATE OR REPLACE FUNCTION sum_2numbers(x bigint, y bigint) 
	RETURNS bigint AS $$
 		SELECT x + y;
	$$ LANGUAGE SQL;

SELECT sum_2numbers(5, 6);

-- пример функции с обращением к таблице "Tickets"
CREATE OR REPLACE FUNCTION "booked_tickets_count"(user_id integer)
	RETURNS bigint AS 
		'SELECT count("booked") 
		FROM "Tickets"
		WHERE "userID"=user_id AND "booked"=true' 
	LANGUAGE SQL volatile;

SELECT "booked_tickets_count"(1);

-- пример аналогичной booked_tickets_count функции, но с объявлением переменной
CREATE OR REPLACE FUNCTION "booked_tickets_count_2"(user_id integer)
RETURNS bigint AS
$$
DECLARE
	tickets_count bigint;
BEGIN
	SELECT count("booked") INTO tickets_count FROM "Tickets"
	WHERE "userID"=user_id AND "booked"=true;

	RETURN tickets_count;
END
$$
LANGUAGE plpgsql;

SELECT "booked_tickets_count_2"(1);

-- запрос с использованием всех 3х функций, созданных выше
SELECT sum_2numbers("booked_tickets_count"(1), "booked_tickets_count_2"(2));
-----------------------------------------------------------------------------

-- табличные функции (возвращают набор строк)

-- встроенные табличные ф-ии (inline): тело которых - один sql-запрос
-- пример inline-функции: возвращает ТАБЛИЦУ со столбцами: 
-- цены вариантов багажа, умноженные на коэффициент; id вариантов багажа.
CREATE TYPE luggage_prices_type AS (new_cost double precision, luggage_variant_id int);

CREATE OR REPLACE FUNCTION changed_luggage_prices (koef numeric)
	RETURNS SETOF luggage_prices_type AS $$
 		SELECT $1 * "cost", "luggageVariantID" FROM "LuggageVariants";
	$$ LANGUAGE SQL;

SELECT * from changed_luggage_prices(0.95);
DROP FUNCTION changed_luggage_prices(numeric);

-- многоинструкционные табличные ф-ии («multi-statement»): состоят из нескольких sql-запросов,
-- результаты записываются в возвращаемый набор данных определенного типа
-- пример multi-statement-функции: получим варианты багажа пользователей со скидочными ценами, учитывая возраст
CREATE TYPE LuggageVariantsForUsers AS ("userID" integer, "luggageVariantID" integer, "cost" real, "maxWeight" integer);

CREATE OR REPLACE FUNCTION luggage_variants_with_discounted_prices_for_users (user_ids integer[]) 
	RETURNS SETOF LuggageVariantsForUsers AS
$$
DECLARE 
	user_id integer;
	user_age integer;
	ulvr LuggageVariantsForUsers%rowtype;
BEGIN
	FOREACH user_id IN ARRAY user_ids LOOP
		SELECT INTO user_age age FROM "Passengers" WHERE "userID" = user_id;
		
		FOR ulvr IN 
			SELECT ts."userID", lvs."luggageVariantID", lvs."cost", lvs."maxWeight" FROM "Tickets" AS ts
			INNER JOIN "LuggageVariants" AS lvs 
			ON lvs."luggageVariantID" = ts."luggageVariantID"
			GROUP BY ts."userID", lvs."luggageVariantID"
			HAVING ts."userID"=user_id
		LOOP
			IF user_age < 12 THEN
				ulvr."cost" = ulvr."cost" * 0.75;
			ELSEIF user_age > 60 THEN
				ulvr."cost" = ulvr."cost" * 0.5;
			END IF;
			
			RETURN NEXT ulvr;
		END LOOP;
	END LOOP;

	RETURN;
END;
$$ LANGUAGE plpgsql;

-- удаление функции
DROP FUNCTION luggage_variants_with_discounted_prices_for_users(integer[]);

-- вызов функции
select * from  luggage_variants_with_discounted_prices_for_users(ARRAY[1, 5, 6]);

-- вспомогательные вызовы
select * from "LuggageVariants";
select * from "Tickets";
select * from "Passengers";
--------------------------------------------------------------------------------------

-- хранимые процедуры (не возвращают значения)
-- пример: для вариантов багажа с весмами в диапазоне [weight_min, weight_max] 
-- уменьшим веса на kgToreduce
-- в примере также есть: динамический запрос, вызов и перехват исключения, условный оператор IF
CREATE OR REPLACE PROCEDURE reduce_luggage_weights
	(kgToReduce integer, weight_min integer, weight_max integer) AS 
$$ 	
DECLARE
	lv_id integer;
	array_is_empty boolean;
	message text;
BEGIN
	array_is_empty := true;
	
	FOR lv_id IN EXECUTE 'SELECT "luggageVariantID" FROM "LuggageVariants" WHERE "maxWeight" >= $1 AND 
 		"maxWeight" <= $2 AND "maxWeight" >= $3' USING weight_min, weight_max, kgToReduce
		LOOP
			array_is_empty := false;
			UPDATE "LuggageVariants" SET "maxWeight" = "maxWeight" - kgToReduce 
				WHERE "luggageVariantID"=lv_id;
		END LOOP;
	
	IF array_is_empty IS TRUE THEN 
		RAISE EXCEPTION USING errcode='E0001', 
			message='Вариантов багажа с весами, большими уменьшаемой суммы,
			и лежащими в диапазоне [' || $2 || ', ' || $3 || '] и не существует';
	END IF;
	
	EXCEPTION WHEN SQLSTATE 'E0001' THEN
		GET STACKED DIAGNOSTICS message = message_text;
		RAISE NOTICE E'\nПЕРЕХВАЧЕННАЯ ОШИБКА: %\n', message;
RETURN;
END;
$$ LANGUAGE plpgsql;

-- вызов процедуры
CALL reduce_luggage_weights(5, 10, 70);

-- вспомогательные действия для возврата LuggageVariants в исходное состояние
UPDATE "LuggageVariants" SET "maxWeight" = "maxWeight" + 5 WHERE "luggageVariantID"=8;
--------------------------------------------------------------------------------------

-- рекурсивный запрос (вывод данных на основе предыдущих строк в выборке)
WITH RECURSIVE ForeFather AS ( 
	SELECT "userID", name, surname, dateBirthday, fatherID FROM "FamilyMembers"
	WHERE "userID"=13
UNION ALL
	SELECT fm."userID", fm.name, fm.surname, fm.dateBirthday, fm.fatherID
	FROM "FamilyMembers" fm, ForeFather ff
	WHERE ff.fatherID=fm."userID"
)
SELECT * FROM ForeFather;

WITH RECURSIVE Predki AS ( 
	SELECT "userID", name, surname, dateBirthday, motherID, fatherID, 2 as predki_count FROM "FamilyMembers"
	WHERE "userID"=9
UNION ALL
	SELECT fm."userID", fm.name, fm.surname, fm.dateBirthday, fm.motherID, fm.fatherID, ff.predki_count + 2
	FROM "FamilyMembers" fm, Predki ff
	WHERE ff.fatherID=fm."userID" OR ff.motherID=fm."userID"
)
SELECT * FROM Predki; count(*)

-- таблица для примера
CREATE TABLE "FamilyMembers" (
	fatherID integer,
	motherID integer
) INHERITS ("Passengers");

-- заполнение таблицы
INSERT INTO "FamilyMembers"("userID", "login", "password", name, surname, gender, age, dateBirthday, fatherID, motherID)
	VALUES (7, 'user7', '7777', 'Петр', 'Иванов', true, 79, '1945-01-01', 18, 19);
INSERT INTO "FamilyMembers"("userID", "login", "password", name, surname, gender, age, dateBirthday, fatherID, motherID)
	VALUES (8, 'user8', '8888','Анжела', 'Симонова', false, 79, '1945-01-01', 20, 21);
INSERT INTO "FamilyMembers"("userID", "login", "password", name, surname, gender, age, dateBirthday, fatherID, motherID)
	VALUES (9, 'user9', '9999','Георгий', 'Иванов', true, 49, '1975-01-01', 7, 8);
INSERT INTO "FamilyMembers"("userID", "login", "password", name, surname, gender, age, dateBirthday, fatherID, motherID)
	VALUES (10, 'user10', '1010','Елена', 'Полякова', false, 49, '1975-01-01', 16, 17);
INSERT INTO "FamilyMembers"("userID", "login", "password", name, surname, gender, age, dateBirthday, fatherID, motherID)
	VALUES (11, 'user11', '1111','Сергей', 'Иванов', true, 24, '2000-01-01', 9, 10);
INSERT INTO "FamilyMembers"("userID", "login", "password", name, surname, gender, age, dateBirthday, fatherID, motherID)
	VALUES (12, 'user12', '1212','Мария', 'Гагарина', false, 24, '2000-01-01', 14, 15);
INSERT INTO "FamilyMembers"("userID", "login", "password", name, surname, gender, age, dateBirthday, fatherID, motherID)
	VALUES (13, 'user13', '1313','Ваня', 'Иванов', true, 2, '2022-01-01', 11, 12);

-- выборка из таблицы
select "userID", name, surname, gender, age, dateBirthday, fatherID, motherID from "FamilyMembers";

-- удаление из таблицы
delete from "FamilyMembers"; where "userID"=8;
--------------------------------------------------------------------------------------

-- LIMIT - ограничение числа записей
select "userID", name, surname, gender, age, dateBirthday, fatherID, motherID from "FamilyMembers" LIMIT 3;

-- RETURNING - для отображения строк, измененных запросами INSERT/UPDATE 
INSERT INTO "FamilyMembers"("userID", "login", "password", name, surname, gender, age, dateBirthday, fatherID, motherID)
	VALUES (14, 'user14', '1414','Даня', 'Иванов', true, 3, '2021-01-01', 11, 12) 
	RETURNING *;

UPDATE "FamilyMembers" SET "password"='1419' WHERE "userID"=14 RETURNING *;

--------------------------------------------------------------------------------------
-- вспомогательные действия для примера
UPDATE "Passengers" SET country='France' WHERE "userID"=10;
UPDATE "Passengers" SET country='Italy' WHERE "userID"=13;
select * from "Passengers";

-- row_number (функция нумерации, возвращает номер строки) - порядковый номер в группе
SELECT "userID", name, surname, age, country,
	row_number() OVER (partition by country order by age desc) AS rank_by_country,
	rank() OVER (partition by country order by age desc) AS rank_by_country,
	dense_rank() OVER (partition by country order by age desc) AS dense_rank_by_country,
	ntile(3) OVER (order by age desc) AS group_number
FROM "Passengers";

-- rank – ранг в группе 
-- при одинаковых значения одинаковый ранг, ранг следующих значений вычисляется с учетом пропусков
SELECT "userID", name, surname, age, country,
	rank() OVER (partition by country order by age desc) AS rank_by_country
FROM "Passengers";

-- dense_rank - ранг в группе 
-- при одинаковых значения одинаковый ранг, ранг следующих значений вычисляется БЕЗ учета пропусков
SELECT "userID", name, surname, age, country,
	dense_rank() OVER (partition by country order by age desc) AS dense_rank_by_country
FROM "Passengers";

-- ntile - делит набор на несколько групп и выводит для каждой строки номер = номеру группы
SELECT "userID", name, surname, age, country,
	ntile(3) OVER (order by age desc) AS group_number
FROM "Passengers";

--------------------------------------------------------------------------------------
-- функция, работающая с курсором (нужен, чтобы пройтись по каждой строке выборки)
-- пример: посчитаем среднюю цену за килограмм багажа (варианты "без багажа"(maxWeight=0) не рассматриваем)
CREATE OR REPLACE FUNCTION average_price_per_kg_of_luggage()
	RETURNS real AS
$$
DECLARE
	cr CURSOR FOR select * from "LuggageVariants";
	
	lg_id integer;
	lg_cost real;
	lg_max_weight integer;
	_result real;
	_count integer;
BEGIN
	_result := 0;
	_count := 0;
	
	OPEN cr; --открываем курсор
	LOOP --начинаем цикл по курсору
		FETCH cr INTO lg_id, lg_cost, lg_max_weight; --извлекаем данные из строки курсора
		IF NOT FOUND THEN EXIT; END IF; -- выходим из цикла, если ничего не найдено
		
		select into lg_cost, lg_max_weight "cost", "maxWeight" from "LuggageVariants" where "maxWeight" > 0;
		_result = _result + (lg_cost / lg_max_weight);
		_count = _count + 1;
	END LOOP; --заканчиваем цикл по курсору
	CLOSE cr; --закрываем курсор
	
	RETURN _result / _count;
END
$$
LANGUAGE plpgsql;

select average_price_per_kg_of_luggage();

--------------------------------------------------------------------------------------

-- встроенные и системные функции
-- строковые 
-- format('строка', [аргументы])
SELECT format('INSERT INTO %I(%I, %I) VALUES (%s, %s)', 'LuggageVariants', 'cost', 'maxWeight', '3000', '25');

-- продедура с динамическим вопросом с format
CREATE OR REPLACE PROCEDURE insert_values_into_table (_table_name text, col1 text, col2 text, value1 text, value2 text) AS
$$
BEGIN
	EXECUTE format('INSERT INTO %I(%I, %I) VALUES (%s, %s)',$1, $2, $3, $4, $5);
	RETURN;
END
$$
LANGUAGE plpgsql;

-- вызов процедуры
CALL insert_values_into_table('LuggageVariants', 'cost', 'maxWeight', '700', '55');

-- выборка из процедуры
select * from "LuggageVariants";

-- чтобы удалить строки, добавленные процедурой
delete from "LuggageVariants" where "luggageVariantID"=20;

-- еще строковые функции
select 'Света' || 'Очеретная'; -- СветаОчеретная, контакенация строк
select char_length('sveta'); -- 5, число символов в строке
select bit_length('sveta'); -- 40, число бит в строке
select octet_length('sveta'); -- 5, число байт в строке
select lower('Света'); -- света, перевод в нижний регистр
select upper('Света'); -- СВЕТА, перевод в верхний регистр
select (overlay('Свеееее' placing 'та' from 4 for 6)); -- Света, заменить часть строки
select position('ет' in 'Света'); -- 3, позиция подстроки в строке
select substring('ееееесветавыфв' from 6 for 5); -- света, извлечение подстроки из строки
select trim(both 'ghj' from 'hgjSvetagghg'); -- Sveta, удаляет подстроки с заданными символами


-- системные функции получения информации о сеансе
select current_catalog; -- AirTickets (текущий каталог)
select current_database(); -- AirTickets (текущая бд)
select current_role; -- postgres (текущая роль пользователя)
select current_schema; -- public (текущая схема бд)
select current_query(); -- текущий запрос, этот же
select inet_client_addr(); -- адрес удаленного сервера
select inet_client_port(); -- порт удаленного сервера, 58918
select inet_server_addr(); -- адрес локального сервера
select inet_server_port(); -- порт локального сервера, 5431
select pg_backend_pid(); -- код серверного процесса, обслуживающего текущий сеанс, 15168

--------------------------------------------------------------------------------------

-- агрегатные функции (вызываются для каждой строки таблицы)
-- состоят из SFUNC (ф-ии состояния), FINALFUNC (финальной ф-ии),
-- STYPE (тип состояния), INITCOND (начальное состояние)

-- пример: найдем число мужчин и женщин среди пассажиров и выведем результат в виде строки

-- STYPE (тип состояния)
CREATE TYPE genderCountType AS (
	menCount int,
	womenCount int
);

-- SFUNC (функция состояния, вызывается для всех строк)
CREATE OR REPLACE FUNCTION sum_gender_counts (genderCountType, boolean)
RETURNS genderCountType AS
$$
DECLARE
	genderCount genderCountType;
BEGIN
	IF $2 = true THEN
		genderCount.menCount := $1.menCount + 1;
		genderCount.womenCount := $1.womenCount;
	ELSE 
		genderCount.womenCount := $1.womenCount + 1;
		genderCount.menCount := $1.menCount;
	END IF;

	RETURN genderCount;
END
$$
LANGUAGE plpgsql;

-- проверка функции
select sum_gender_counts((1, 2), true); -- (2, 2)
select sum_gender_counts((1, 2), false); -- (1, 3)

--  FINALFUNC (финальная функция)
CREATE OR REPLACE FUNCTION gender_count_to_text (genderCountType)
	RETURNS text LANGUAGE SQL
AS $$
 SELECT format('Количество мужчин: %s, количество женщин: %s', $1.menCount, $1.womenCount);
$$;

-- проверка функции
select gender_count_to_text((40, 30)); -- Количество мужчин: 40, количество женщин: 30

-- сама агрегатная функция
CREATE OR REPLACE AGGREGATE gender_counts (boolean) (
	SFUNC = sum_gender_counts,
	FINALFUNC = gender_count_to_text,
	STYPE = genderCountType,
	INITCOND = '(0, 0)'
);

-- вызов функции
select gender_counts(gender) from "Passengers";

-- просмотреть таблицу для проверки
select gender from "Passengers";

--------------------------------------------------------------------------------------

-- представления (виртуальные таблицы, представляющие результаты выборки)
CREATE VIEW userTicketsView  AS 
	SELECT us."userID", us."login", ts."purchased", ts."booked"
	FROM "Users" us, "Tickets" ts
 	WHERE us."userID" = ts."userID";

-- вывод представления
select * from userTicketsView;

-- попытка изменения представления (выдаст ошибку)
UPDATE userTicketsView SET "login"='user2_new' WHERE "userID"=2;

-- DML-триггеры (активизируются до/после/вместо вставки, обновления или удаления строк таблиц)
-- создадим триггер на изменение логина userTicketsView

-- функция для триггера
CREATE OR REPLACE FUNCTION update_login_in_user_tickets_view()
 RETURNS TRIGGER AS
$$
BEGIN
	UPDATE userTicketsView SET "login"=NEW."login" WHERE "userID"=OLD."userID";
	IF NOT FOUND THEN RETURN NULL; END IF;

	RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

-- сам триггер
CREATE TRIGGER update_login_in_user_tickets_view_trigger
 INSTEAD OF UPDATE
 ON userTicketsView
 FOR EACH ROW
 EXECUTE PROCEDURE update_login_in_user_tickets_view();

DROP TRIGGER update_login_in_user_tickets_view_trigger ON userTicketsView;


-- пример триггера AFTER
CREATE TABLE user_logs(
	"userID" integer,
	"date" date
);

CREATE OR REPLACE FUNCTION insert_user_log()
 RETURNS trigger AS
$$
BEGIN
 INSERT INTO user_logs("userID", "date")
 VALUES(NEW."userID", current_date);
 RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER insert_user_trigger
 AFTER INSERT
 ON "Users"
 FOR EACH ROW
 EXECUTE PROCEDURE insert_user_log();

-- проверка триггера
INSERT INTO "Users"("userID", "login", "password") VALUES (6, 'user6', '116');
select * from user_logs;
select * from "Users";

-- DDL триггер на удаление таблиц
-- функция для триггера
CREATE OR REPLACE FUNCTION abort_drop()
  RETURNS event_trigger AS $$
BEGIN
  RAISE EXCEPTION 'Удаление таблиц отключено';
END;
$$ LANGUAGE plpgsql;

-- сам триггер
CREATE EVENT TRIGGER drop_table_trigger ON sql_drop
WHEN TAG IN ('DROP TABLE')
EXECUTE PROCEDURE abort_drop();

-- проверка триггера
DROP TABLE "FamilyMembers";


\COPY (select * from "FamilyMembers") To 'C:\Users\Pocht\OneDrive\Study\query.csv' DELIMITER ‘,’ CSV HEADER; with CSV;

------------------------------------------------------
CREATE TABLE emp (
    empname           text PRIMARY KEY,
    salary            integer
);

CREATE TABLE emp_audit(
    operation         char(1)   NOT NULL,
    userid            text      NOT NULL,
    empname           text      NOT NULL,
    salary            integer,
    stamp             timestamp NOT NULL
);

insert into emp(empname, salary) values('emp1', 15000);
insert into emp(empname, salary) values('emp2', 18000);

insert into emp_audit(operation, userid, empname, salary, stamp) values ('u', 1, 'emp1', 1000, current_date);

CREATE VIEW emp_view AS
    SELECT e.empname,
           e.salary,
           max(ea.stamp) AS last_updated
      FROM emp e
      LEFT JOIN emp_audit ea ON ea.empname = e.empname
     GROUP BY 1, 2;

select * from emp_view;

update emp_view set salary=10000 where empname='emp1';

CREATE OR REPLACE FUNCTION update_emp_view() RETURNS TRIGGER AS $$
    BEGIN
        IF (TG_OP = 'DELETE') THEN
            DELETE FROM emp WHERE empname = OLD.empname;
            IF NOT FOUND THEN RETURN NULL; END IF;

            OLD.last_updated = now();
            INSERT INTO emp_audit VALUES('D', user, OLD.*);
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            UPDATE emp SET salary = NEW.salary WHERE empname = OLD.empname;
            IF NOT FOUND THEN RETURN NULL; END IF;

            NEW.last_updated = now();
            INSERT INTO emp_audit VALUES('U', user, NEW.*);
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO emp VALUES(NEW.empname, NEW.salary);

            NEW.last_updated = now();
            INSERT INTO emp_audit VALUES('I', user, NEW.*);
            RETURN NEW;
        END IF;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER emp_audit
	INSTEAD OF INSERT OR UPDATE OR DELETE 
	ON emp_view
    FOR EACH ROW 
	EXECUTE PROCEDURE update_emp_view();


------------------------------------------------------
-- CREATE TABLE userTicketsViewLogs(
--     operation         char(1)   NOT NULL,
--     userid            text      NOT NULL,
-- 	"userID" integer,
-- 	"login" text,
-- 	"purchased" boolean,
-- 	"booked" boolean,
--     stamp             timestamp NOT NULL
-- );

-- функция для триггера
CREATE OR REPLACE FUNCTION update_login_in_user_tickets_view()
 RETURNS TRIGGER AS
$$
BEGIN
-- 	IF (TG_OP = 'UPDATE') THEN
		UPDATE userTicketsView SET "login"=NEW."login" WHERE "userID"=OLD."userID";
		IF NOT FOUND THEN RETURN NULL; END IF;

-- 		NEW.last_updated = now();
-- 		INSERT INTO userTicketsViewLogs VALUES('U', user, NEW.*);
		RETURN NEW;
-- 	END IF;
END;
$$
LANGUAGE 'plpgsql';



-- CREATE TRIGGER update_login_in_user_tickets_view_trigger2
-- INSTEAD OF UPDATE
-- ON userTicketsView
-- AS
-- BEGIN
--     UPDATE us
--     SET us."login" = i."login"
--     FROM INSERTED i
--     JOIN "Users" us ON us."userID" = i."userID"
-- END;