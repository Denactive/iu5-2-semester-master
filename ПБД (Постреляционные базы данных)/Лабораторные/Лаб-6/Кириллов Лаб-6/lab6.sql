DROP TABLE IF EXISTS "USERS" CASCADE
;

DROP TABLE IF EXISTS "CONTRIBUTORS" CASCADE
;

DROP TABLE IF EXISTS "ROLES" CASCADE
;

DROP TABLE IF EXISTS "REPOSITORIES" CASCADE
;

-- 1 ч.1

CREATE TABLE "USERS"
(
	"ID" serial PRIMARY KEY NOT NULL,
	"LOGIN" varchar(255) NOT NULL,
	"PASSWORD" varchar(255) NOT NULL,
	"FIO" varchar(255) NOT NULL,
	"AGE" int CONSTRAINT age_adult CHECK ("AGE" >= 18) 
)
;

CREATE TABLE "ROLES"
(
	"ID" serial PRIMARY KEY NOT NULL,
	"NAME" varchar(255) NOT NULL,
	"ACCESS_RIGHTS" varchar(255) NOT NULL,
	"IS_STAFF" boolean NOT NULL DEFAULT FALSE
)
;

CREATE TABLE "REPOSITORIES"
(
	"ID" serial PRIMARY KEY NOT NULL,
	"NAME" varchar(255) NOT NULL
)
;

-- Многие-ко-многим с указанием мета-информации
CREATE TABLE "CONTRIBUTORS"
(
	"CREATED" timestamp NOT NULL,
	"ROLE_ID" int NOT NULL,
	"USER_ID" int NOT NULL,
	"REPOSITORY_ID" int NOT NULL,
	CONSTRAINT "FK_ROLE" FOREIGN KEY("ROLE_ID") REFERENCES "ROLES"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_USER" FOREIGN KEY("USER_ID") REFERENCES "USERS"("ID") ON DELETE CASCADE,
	CONSTRAINT "FK_REPOSITORY" FOREIGN KEY("REPOSITORY_ID") REFERENCES "REPOSITORIES"("ID") ON DELETE CASCADE,
  PRIMARY KEY ("REPOSITORY_ID", "USER_ID")
)
;

-- 1 ч.2

INSERT INTO "ROLES"("NAME", "ACCESS_RIGHTS", "IS_STAFF") VALUES ('manager', 'view, create_repo, add_users, run_pipeline', TRUE);
INSERT INTO "ROLES"("NAME", "ACCESS_RIGHTS", "IS_STAFF") VALUES ('developer', 'view, push, run_pipeline', TRUE);
INSERT INTO "ROLES"("NAME", "ACCESS_RIGHTS", "IS_STAFF") VALUES ('sre', 'view', FALSE);

INSERT INTO "USERS"("LOGIN", "PASSWORD", "FIO", "AGE") VALUES ('dkirillov', '1111', 'Кириллов Д. С.', 18);
INSERT INTO "USERS"("LOGIN", "PASSWORD", "FIO") VALUES ('avolkov', '1111', 'Волков А. С.');
INSERT INTO "USERS"("LOGIN", "PASSWORD", "FIO") VALUES ('mvinogradova', '1111', 'Виноградова М. В.');
INSERT INTO "USERS"("LOGIN", "PASSWORD", "FIO") VALUES ('vterekhov', '1111', 'Терехов В. И.');
INSERT INTO "USERS"("LOGIN", "PASSWORD", "FIO") VALUES ('aantonov', '1111', 'Антонов А. И.');

INSERT INTO "REPOSITORIES"("NAME") VALUES ('Любимый проект кафедры');
INSERT INTO "REPOSITORIES"("NAME") VALUES ('Чуть менее любимый проект кафедры');
INSERT INTO "REPOSITORIES"("NAME") VALUES ('Средне любимый проект кафедры');
INSERT INTO "REPOSITORIES"("NAME") VALUES ('Не любимый проект кафедры');
INSERT INTO "REPOSITORIES"("NAME") VALUES ('Вообще не любимый проект кафедры');

INSERT INTO "CONTRIBUTORS"("CREATED", "ROLE_ID", "USER_ID", "REPOSITORY_ID") VALUES ((NOW()), (SELECT "ID" FROM "ROLES" WHERE "NAME"='manager'), (SELECT "ID" FROM "USERS" WHERE "LOGIN"='dkirillov'), (SELECT "ID" FROM "REPOSITORIES" WHERE "NAME"='Любимый проект кафедры'));
INSERT INTO "CONTRIBUTORS"("CREATED", "ROLE_ID", "USER_ID", "REPOSITORY_ID") VALUES ((NOW()), (SELECT "ID" FROM "ROLES" WHERE "NAME"='manager'), (SELECT "ID" FROM "USERS" WHERE "LOGIN"='avolkov'), (SELECT "ID" FROM "REPOSITORIES" WHERE "NAME"='Чуть менее любимый проект кафедры'));
INSERT INTO "CONTRIBUTORS"("CREATED", "ROLE_ID", "USER_ID", "REPOSITORY_ID") VALUES ((NOW()), (SELECT "ID" FROM "ROLES" WHERE "NAME"='manager'), (SELECT "ID" FROM "USERS" WHERE "LOGIN"='mvinogradova'), (SELECT "ID" FROM "REPOSITORIES" WHERE "NAME"='Средне любимый проект кафедры'));
INSERT INTO "CONTRIBUTORS"("CREATED", "ROLE_ID", "USER_ID", "REPOSITORY_ID") VALUES ((NOW()), (SELECT "ID" FROM "ROLES" WHERE "NAME"='developer'), (SELECT "ID" FROM "USERS" WHERE "LOGIN"='vterekhov'), (SELECT "ID" FROM "REPOSITORIES" WHERE "NAME"='Любимый проект кафедры'));
INSERT INTO "CONTRIBUTORS"("CREATED", "ROLE_ID", "USER_ID", "REPOSITORY_ID") VALUES ((NOW()), (SELECT "ID" FROM "ROLES" WHERE "NAME"='developer'), (SELECT "ID" FROM "USERS" WHERE "LOGIN"='aantonov'), (SELECT "ID" FROM "REPOSITORIES" WHERE "NAME"='Любимый проект кафедры'));


SELECT * FROM "ROLES";
SELECT * FROM "USERS";
SELECT * FROM "REPOSITORIES";
SELECT * FROM "CONTRIBUTORS";

-- 2.1 все поля - элементы

SELECT xmlelement(name user_record,
				  xmlforest("LOGIN", "FIO", "AGE")
		    )
		FROM "USERS"

-- 2.2 все поля - атрибуты

SELECT entity2char(
	xmlelement(name user_record,
				  xmlattributes("LOGIN" as login, "FIO" as fio, "AGE")
		)::text || chr(10)
	) 
	FROM "USERS";

-- 2.3 добавление корневого элемента, 
-- 2.3.1. Заголовок каждому
SELECT xmlroot(
  xmlelement(
	  name user_record,
    xmlforest("LOGIN", "FIO", "AGE")
  ), version '1.1', standalone yes)
  FROM "USERS";

-- 2.3.2. Перевести всю таблицу + добавить 1 общий заголовок
SELECT xmlroot(
  xmlelement(
	  name all_users,
	  xmlagg(
		  xmlelement(
			  name user_record,
				xmlforest("LOGIN", "FIO", "AGE")
		  )
	  )
  ), version '1.1', standalone yes)
  FROM "USERS";

-- 2.4 переименование строк

-- 2.5. БД в XML
SELECT xmlelement(name user_record,
				  xmlforest("LOGIN" as username, "FIO" as "Фамилия И.О.", "AGE" as "Число лет")
		    )
		FROM "USERS"

-- Вывод схемы (public) с описанием полей и их типов:
SELECT schema_to_xmlschema('public', true, true, '');

-- Два предыдущих вывода вместе:
SELECT schema_to_xml_and_xmlschema('public', true, true, '');

-- ЕЩЕ
SELECT database_to_xml(true, true, '');
SELECT database_to_xmlschema(true, true, '');
SELECT database_to_xml_and_xmlschema(true, true, '');

-- Аналогичные действия можно совершать только с таблицами или с запросами (т.е. частью таблиц):
SELECT table_to_xml('"USERS"', true, true, '');
SELECT table_to_xmlschema('"USERS"', true, true, '');
SELECT table_to_xml_and_xmlschema('"USERS"', true, true, '');

SELECT query_to_xml('SELECT * FROM "USERS" WHERE "AGE" < 30', true, true, '');
SELECT query_to_xmlschema('SELECT * FROM "USERS" WHERE "AGE" < 30', true, true, '');
SELECT query_to_xml_and_xmlschema('SELECT * FROM "USERS" WHERE "AGE" < 30', true, true, '');

-- Также результаты выводов можно сразу копировать в файл (главное, чтобы файл находился в публичной папке, в нашем случае это C:\Users\Public):
-- Можно сохранить только в  C:\Пользователи\Общая
COPY (SELECT schema_to_xml('public', true, true, '')) to 'C:\Users\Public\db_lab6.xml';

-- 2.6. Отображение значений NULL
SELECT table_to_xml('"USERS"', true, true, '');
SELECT table_to_xml('"USERS"', false, true, '');



-- 3.1
SELECT query_to_xml('SELECT * FROM "USERS" WHERE "AGE" < 30', true, true, '');

-- 3.2
SELECT query_to_xml(
	'SELECT xmlelement(name user_record,
				  xmlforest("LOGIN", "FIO", "AGE")
		    )
		FROM "USERS"', true, true, '');
    
-- 3.3
SELECT query_to_xml('
  SELECT xmlelement(
	  name all_users,
	  xmlagg(
		  xmlelement(
			  name user_record,
				xmlforest("LOGIN", "FIO", "AGE")
		  )
	  )
  ) as root_all_users FROM "USERS"', true, true, '');

-- 3.4 step 1
SELECT * FROM "CONTRIBUTORS" as c JOIN "USERS" as u ON c."USER_ID" = u."ID";

-- 3.4 step 2
SELECT  entity2char(
	xmlagg(
		xmlelement(name user_record,
				   xmlattributes(u."LOGIN", u."FIO", u."AGE")
				  )
		)::text
	)
	FROM "CONTRIBUTORS" as c JOIN "USERS" as u ON c."USER_ID" = u."ID";

-- 3.4 step 3
SELECT entity2char(
		xmlelement(
			name contributor_record,
			xmlconcat(
				xmlforest(c."ROLE_ID", c."REPOSITORY_ID", c."CREATED"),
				xmlelement(name user_record,
						   xmlattributes(u."LOGIN", u."FIO", u."AGE")
						  )
				)
			)::text
		)
		FROM "CONTRIBUTORS" as c JOIN "USERS" as u ON c."USER_ID" = u."ID";

-- 3.4 step 4 end
SELECT entity2char(
	xmlelement(
		name all_contributor_records,
		xmlagg(
			xmlelement(
				name contributor_record,
				xmlconcat(
					xmlforest(c."ROLE_ID", c."REPOSITORY_ID", c."CREATED"),
					xmlelement(name user_record,
							   xmlattributes(u."LOGIN", u."FIO", u."AGE")
							  )
					)
				)
			)
		)::text
	)
	FROM "CONTRIBUTORS" as c JOIN "USERS" as u ON c."USER_ID" = u."ID";

-- 4
-- Только C:\Пользователи\Общая
COPY (SELECT schema_to_xml('public', true, true, '')) to 'C:\Users\Public\db_lab6.xml';
SELECT xmlparse(DOCUMENT pg_read_file('C:\Users\Public\db_lab6.xml'));

CREATE TABLE SCHEME_TABLE_XML AS 
SELECT xmlparse(DOCUMENT pg_read_file('C:\Users\Public\db_lab6.xml'));

SELECT * FROM SCHEME_TABLE_XML;

SELECT xpath('//public/CONTRIBUTORS', xmlparse(DOCUMENT pg_read_file('C:\Users\Public\db_lab6.xml')));

-- не может распарсить из-за какой-то фигурной скобки
SELECT xmltable.*
	FROM xmlparse(DOCUMENT pg_read_file('C:\Users\Public\db_lab6.xml')),
		XMLTABLE ('//public/CONTRIBUTORS' PASSING 'CONTRIBUTORS'
      		COLUMNS
				userID integer PATH '@USER_ID' NOT NULL
	);


-- 5.1

SELECT
-- Атрибуты
-- В сохранненой таблице нет атрибутов. Воспользуемся синтаксисом xml-строки
	-- true
	xpath_exists('//h[@attr]', xml'<h attr="1"><e/></h>'),
	-- false
	xpath_exists('//h[@CHEBURASHKA]', xml'<h attr="1"><e/></h>'),
-- Значение атрибута
	-- true
	xpath_exists('//h[@attr=1]', xml'<h attr="1"><e/></h>'),
	-- false
	xpath_exists('//h[@attr=''medved'']', xml'<h attr="1"><e/></h>'),
-- Элементы
	-- true
	xpath_exists('//public/CONTRIBUTORS', xmlparse(DOCUMENT pg_read_file('C:\Users\Public\db_lab6.xml'))),
	-- false
	xpath_exists('//public/CHEBURASHKA', xmlparse(DOCUMENT pg_read_file('C:\Users\Public\db_lab6.xml'))),
-- Значение элемента
	-- true
	xpath_exists('//public/CONTRIBUTORS/ROLE_ID[.=1]', xmlparse(DOCUMENT pg_read_file('C:\Users\Public\db_lab6.xml'))),
	-- false
	xpath_exists('//public/CONTRIBUTORS/ROLE_ID[.=100]', xmlparse(DOCUMENT pg_read_file('C:\Users\Public\db_lab6.xml')));

-- 5.2

SELECT
-- Извлечение атрибутов
	unnest(xpath('//h/@attr', xml'<h attr="1"><e/></h>')),
-- Извлечение элементов
	unnest(xpath('//h[e]', xml'<h attr="1"><e l="lo"/></h>')),
-- Извлечение содержимого
	unnest(xpath('//h/text()', xml'<h attr="1">ello</h>'));

-- 5.3
SELECT unnest(xpath('//public/CONTRIBUTORS[1]', xmlparse(DOCUMENT pg_read_file('C:\Users\Public\db_lab6.xml'))));

-- 6.1

-- Запрос с агрегацией и группировкой
-- 6.1.1
SELECT
	unnest(xpath('//public/USERS/ID/text()', xml_doc))::text::int as identifier,
	unnest(xpath('//public/USERS/AGE/text()', xml_doc))::text::int as age,
	unnest(xpath('//public/USERS/PASSWORD/text()', xml_doc))::text as passwrd
	FROM
		xmlparse(DOCUMENT pg_read_file('C:\Users\Public\db_lab6.xml')) as xml_doc;

-- 6.1.2
-- Чтобы иметь возможность агрегировать, надо обернуть запрос, извлекающий значения из XML, в другой запрос
SELECT passwrd, count(identifier), avg(age)
  FROM (
    SELECT
      unnest(xpath('//public/USERS/ID/text()', xml_doc))::text::int as identifier,
      unnest(xpath('//public/USERS/AGE/text()', xml_doc))::text::int as age,
      unnest(xpath('//public/USERS/PASSWORD/text()', xml_doc))::text as passwrd
      FROM
        xmlparse(DOCUMENT pg_read_file('C:\Users\Public\db_lab6.xml')) as xml_doc
	) as xml_parse_subquery
	GROUP BY passwrd, age;

-- Защита
-- Схема по умолчанию: table_to_xml_and_xmlschema и кратко рассказать, что там содержится

-- Распарсить сайт https://bmstu.ru/news?category[]=arktika через XPATH: извлечь все заголовки «p» новостей. Продемонстрировать результат либо на ресурсе http://xpather.com , либо в devtools.

-- Путевое выражение
-- //html//div[contains(@class, "News__list")]
-- выберет контейнер с карточками новостей. Он имеет атрибут класса News__list, но в этот атрибут также записана автогенерируемая строка, поэтому надо использовать функцию contains.

-- Остается выбрать все параграфы и извлечь текст
-- //html//div[contains(@class, "News__list")]//p/text()
