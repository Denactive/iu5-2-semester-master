-- Удаление таблиц
DROP TABLE IF EXISTS Users CASCADE;
DROP TABLE IF EXISTS LuggageVariants CASCADE;
DROP TABLE IF EXISTS Flights CASCADE;
DROP TABLE IF EXISTS Tickets CASCADE;
DROP TABLE IF EXISTS Passengers CASCADE;

-- Вывод таблиц
select * from only Users;
select * from LuggageVariants;
select * from Flights;
select * from Tickets;
select * from Passengers;

------------------------------ 1 ------------------------------
-- Создание таблиц
CREATE TABLE Users
(
	userID serial PRIMARY KEY NOT NULL,
	login varchar(255) NOT NULL UNIQUE,
	password varchar(255) NOT NULL
);

CREATE TABLE Passengers (
	name text,
	surname text,
	age integer,
	gender boolean,
	country text,
	passportSeries text,
	passportNumber text
) INHERITS (Users);

CREATE TABLE LuggageVariants
(
	luggageVariantID serial PRIMARY KEY NOT NULL,
	cost real NOT NULL CHECK ("cost" > 0),
	maxWeight integer NOT NULL DEFAULT 0
);

CREATE TABLE Flights
(
	flightID serial PRIMARY KEY NOT NULL,
	departurePoint varchar(255) NOT NULL,
	departureTimeAndDate timestamp NOT NULL,
	destinationPoint varchar(255) NOT NULL,
	destinationTimeAndDate timestamp NOT NULL,
	company varchar(255) NOT NULL
);

CREATE TABLE Tickets
(
	ticketID serial PRIMARY KEY NOT NULL,
	purchased boolean NOT NULL DEFAULT false,
	place integer,
	
	luggageVariant_ID integer,
	user_ID integer,
	flight_ID integer,
	
	CONSTRAINT FK_Tickets_LuggageVariants FOREIGN KEY(luggageVariant_ID) REFERENCES LuggageVariants(luggageVariantID),
	CONSTRAINT FK_Tickets_Users FOREIGN KEY(user_ID) REFERENCES Users(userID),
	CONSTRAINT FK_Tickets_Flights FOREIGN KEY(flight_ID) REFERENCES Flights(flightID)
);


-- Заполнение таблиц
INSERT INTO Users(userID, login, password) VALUES (1, 'svetlanlka', '111');
INSERT INTO Users(userID, login, password) VALUES (2, 'tatyanka', '222');
INSERT INTO Users(userID, login, password) VALUES (3, 'sofiyka', '333');
INSERT INTO Users(userID, login, password) VALUES (4, 'petrovich', '444');
INSERT INTO Users(userID, login, password) VALUES (5, 'ivanov', '555');

INSERT INTO LuggageVariants(luggageVariantID, cost, maxWeight) VALUES (1, 4000, 40);
INSERT INTO LuggageVariants(luggageVariantID, cost, maxWeight) VALUES (2, 6000, 80);
INSERT INTO LuggageVariants(luggageVariantID, cost, maxWeight) VALUES (3, 2800, 20);
INSERT INTO LuggageVariants(luggageVariantID, cost, maxWeight) VALUES (4, 1600, 10);
INSERT INTO LuggageVariants(luggageVariantID, cost, maxWeight) VALUES (5, 7000, 100);

INSERT INTO Flights(flightID, departurePoint, departureTimeAndDate, destinationPoint, destinationTimeAndDate, company) 
	VALUES (1, 'Москва', '2024-12-10 11:15', 'Санкт-Петербург', '2024-12-10 13:00', 'Аэрофлот');
INSERT INTO Flights(flightID, departurePoint, departureTimeAndDate, destinationPoint, destinationTimeAndDate, company) 
	VALUES (2, 'Москва', '2024-12-10 18:00', 'Санкт-Петербург', '2024-12-10 19:40', 'S7 Airlines');
INSERT INTO Flights(flightID, departurePoint, departureTimeAndDate, destinationPoint, destinationTimeAndDate, company) 
	VALUES (3, 'Анапа', '2024-12-11 16:30', 'Москва', '2024-12-11 18:00', 'Аэрофлот');
INSERT INTO Flights(flightID, departurePoint, departureTimeAndDate, destinationPoint, destinationTimeAndDate, company) 
	VALUES (4, 'Москва', '2024-11-10 17:00', 'Анапа', '2024-11-10 19:20', 'Победа');
INSERT INTO Flights(flightID, departurePoint, departureTimeAndDate, destinationPoint, destinationTimeAndDate, company) 
	VALUES (5, 'Москва', '2024-11-12 11:30', 'Анапа', '2024-11-12 14:00', 'Аэрофлот');

INSERT INTO Tickets(ticketID, purchased, place, luggageVariant_ID, user_ID, flight_ID) VALUES (1, false, 1, 3, 1, 4);
INSERT INTO Tickets(ticketID, purchased, place, luggageVariant_ID, user_ID, flight_ID) VALUES (2, false, 2, 4, 2, 4);
INSERT INTO Tickets(ticketID, purchased, place, luggageVariant_ID, user_ID, flight_ID) VALUES (3, true, 1, 2, 3, 1);
INSERT INTO Tickets(ticketID, purchased, place, luggageVariant_ID, user_ID, flight_ID) VALUES (4, true, 2, 1, 5, 1);
INSERT INTO Tickets(ticketID, purchased, place, luggageVariant_ID, user_ID, flight_ID) VALUES (5, false, 1, 3, 3, 5);

INSERT INTO Tickets(ticketID, purchased, place, luggageVariant_ID, user_ID, flight_ID) VALUES (6, false, 1, null, 4, 2);
INSERT INTO Tickets(ticketID, purchased, place, luggageVariant_ID, user_ID, flight_ID) VALUES (7, false, 3, null, 3, 4);
INSERT INTO Tickets(ticketID, purchased, place, luggageVariant_ID, user_ID, flight_ID) VALUES (8, true, 4, null, 5, 4);

INSERT INTO Passengers(userID, login, password, name, surname, age, gender, country, passportSeries, passportNumber) 
VALUES (1, 'svetlanlka', '111', 'Светлана', 'Очеретная', 21, false, 'Russia', '1111', '121212');
INSERT INTO Passengers(userID, login, password, name, surname, age, gender, country, passportSeries, passportNumber) 
VALUES (2, 'tatyanka', '222', 'Татьяна', 'Очеретная', 15, false, 'Russia', '2222', '212121');
INSERT INTO Passengers(userID, login, password, name, surname, age, gender, country, passportSeries, passportNumber) 
VALUES (3, 'sofiyka', '333', 'София', 'Беляева', 35, false, 'Italy', '3333', '212121');
INSERT INTO Passengers(userID, login, password, name, surname, age, gender, country, passportSeries, passportNumber) 
VALUES (4, 'petrovich', '444', 'Петр', 'Петров', 40, true, 'Italy', '4444', '434343');
INSERT INTO Passengers(userID, login, password, name, surname, age, gender, country, passportSeries, passportNumber) 
VALUES (5, 'ivanov', '555', 'Иван', 'Иванов', 25, true, 'Russia', '5555', '515151');


------------------------------ 2 [export to xml] ------------------------------ 

-- 2.1. one field-element
select xmlelement(name Passengers, name) from Passengers;

-- 2.1. all fields-elements
select xmlelement(
	name Passengers, 
	xmlforest(name, surname, age, gender, country, passportSeries, passportNumber)
) from Passengers;

-- 2.2. fields-attributes
select xmlelement(
	name LuggageVariants, 
	xmlattributes(luggageVariantID, cost, maxWeight)
) from LuggageVariants;

-- with russian symbols: method 1 <-- not neccesary for this lab
SELECT 
  xmlparse(CONTENT entity2char(xmlelement(NAME Passengers, xmlagg(Passengers))::text))
FROM (SELECT 
  xmlelement(name Passengers, xmlattributes(name, surname, age, gender, country, passportSeries, passportNumber)) 
	AS Passengers from Passengers
)j;

-- with russian symbols: method 2 (and with \n) <-- not neccesary for this lab
select entity2char(
	xmlelement(
		name Passengers, 
		xmlattributes(name, surname, age, gender, country, passportSeries, passportNumber)
	)::text) || chr(10) 
from Passengers;

-- 2.3. add root element
select xmlroot(xmlelement(
	name LuggageVariants, xmlforest(luggageVariantID, cost, maxWeight)
), version '1.1', standalone yes) from LuggageVariants;


-- 2.4. rename lines
select xmlelement(
	name LuggageVariants, 
	xmlforest(luggageVariantID as id, cost as price, maxWeight)
) from LuggageVariants;

-- 2.5. default schema
select schema_to_xml('public', true, true, '');
select schema_to_xmlschema('public', true, true, '');
select schema_to_xml_and_xmlschema('public', true, true, '');

-- export database
select database_to_xml(true, true, '');
select database_to_xmlschema(true, true, '');
select database_to_xml_and_xmlschema(true, true, '');

-- export one table
select table_to_xml('Users', true, true, '');
select table_to_xmlschema('Users', true, true, '');
select table_to_xml_and_xmlschema('Users', true, true, '');

-- export query
select query_to_xml('select * from Passengers where age < 30', true, true, '');
select query_to_xmlschema('select * from Passengers where age < 30', true, true, '');
select query_to_xml_and_xmlschema('select * from Passengers where age < 30', true, true, '');

-- with copy to file (only to Public folder)
copy (select database_to_xml(true, true, '')) to 'C:\Users\Public\Documents\PBD_lab6\airtickets_copy.xml';


-- 2.6. NULL values = true
select table_to_xml('Tickets', true, true, '');
-- 2.6. NULL values = false
select table_to_xml('Tickets', false, true, '');

------------------------------ 3 ------------------------------
-- 3.1. query with condition
select query_to_xml('select * from Flights where destinationPoint=''Анапа''', true, true, '');

-- 3.2. export tables to xml with arbitrary structure
---- 3.2.1. with attributes
select xmlelement(
	name AirTickets, 
	xmlagg(
		xmlelement(
			name user, 
			xmlattributes(userID as id, login, password)
		)
	)
) from Users;

---- 3.2.1. add new lines
select xmlelement(
	name AirTickets, 
	xmlconcat('

', xmlagg(
		xmlconcat(xmlelement(
			name user, 
			xmlattributes(userID as id, login, password)
		), '
'))
	)
) from Users;

---- 3.2.2. add child xml-elements and attributes for them
select xmlelement(
	name AirTickets, 
	xmlconcat('

', xmlagg(
		xmlconcat(xmlelement(
			name user, 
			xmlattributes(userID as id, login, password),
			xmlconcat( 
				(select xmlagg(xmlelement(
					name ticket, 
					xmlattributes(ticketID as id, purchased), 
					place)
				) from Tickets where userID=userID)
			) 
		), '
'))
	)
) from Users;

---- 3.2.2. add new lines
select xmlelement(
	name AirTickets, 
	xmlconcat('

', xmlagg(
		xmlconcat(xmlelement(
			name user, 
			xmlattributes(userID as id, login, password),
			xmlconcat(
				(select xmlconcat('
', xmlagg(xmlconcat('
	', xmlelement(
					name ticket, 
					xmlattributes(ticketID as id, purchased), 
					place)
				)), '
') from Tickets where userID=userID)
			) 
		), '
'))
	)
) from Users;

-- 3.2. for all tables
copy (
	
select xmlparse(CONTENT entity2char(xmlelement(
	name AirTickets,
	xmlconcat('

',
(select xmlelement(
	name passengers, 
	xmlconcat('

', xmlagg(
		xmlconcat(xmlelement(
			name passenger, 
			xmlattributes(userID as uid, login, password, name, surname, age, gender, country, passportSeries, passportNumber),
			xmlconcat(
				(select xmlconcat('
', xmlagg(xmlconcat('
	', xmlelement(
					name ticket, 
					xmlattributes(ticketID as tid, purchased, luggageVariant_ID as lvid, user_ID as uid, flight_ID as fid), 
					place)
				)), '
') from Tickets where user_ID=userID)
			) 
		), '
'))
	)
) from Passengers
), '
',
(select xmlelement(
	name LuggageVariants, 
	xmlconcat('

', xmlagg(
		xmlconcat(xmlelement(
			name LuggageVariant, 
			xmlattributes(luggageVariantID as lvid, cost, maxWeight),
			xmlconcat(
				(select xmlconcat('
', xmlagg(xmlconcat('
	', xmlelement(
					name ticket, 
					xmlattributes(ticketID as tid, purchased, luggageVariant_ID as lvid, user_ID as uid, flight_ID as fid), 
					place)
				)), '
') from Tickets where luggageVariant_ID=luggageVariantID)
			) 
		), '
'))
	)
) from LuggageVariants)), '
',
(select xmlelement(
	name Flights, 
	xmlconcat('

', xmlagg(
		xmlconcat(xmlelement(
			name Flight, 
			xmlattributes(flightID as fid, departurePoint, departureTimeAndDate, destinationPoint, destinationTimeAndDate, company),
			xmlconcat(
				(select xmlconcat('
', xmlagg(xmlconcat('
	', xmlelement(
					name ticket, 
					xmlattributes(ticketID as tid, purchased, luggageVariant_ID as lvid, user_ID as uid, flight_ID as fid), 
					place)
				)), '
') from Tickets where flight_ID=flightID)
			) 
		), '
'))
	)
) from Flights), '
'	
)::text))
	
) to 'C:\Users\Public\Documents\PBD_lab6\airtickets.xml'; --(FORMAT CSV);--(DELIMITER E'\n', FORMAT TEXT, NULL '', ENCODING 'utf8');


------------------------------ 4 ------------------------------
-- read xml from file
select xmlparse(DOCUMENT pg_read_file('C:\Users\Public\Documents\PBD_lab6\airtickets.xml'));

-- select with condition for get data as tables from xml
CREATE TABLE passengerlist AS 
select xmlparse(DOCUMENT pg_read_file('C:\Users\Public\Documents\PBD_lab6\airtickets.xml'))
AS Passengers;

SELECT xmltable.* FROM passengerlist,
   XMLTABLE ('/airtickets/passengers/passenger' PASSING Passengers
      COLUMNS
		userID integer PATH '@uid' NOT NULL,
		login text PATH '@login' NOT NULL,
		password varchar(255) PATH '@password' NOT NULL,
		name text PATH '@name',
		surname text PATH '@surname',
		age integer PATH '@age',
		gender boolean PATH '@gender',
		country text PATH '@country',
		passportSeries text PATH '@passportseries',
		passportNumber text PATH '@passportnumber',
		tickerID integer PATH 'ticket[1]/@tid'
	)
WHERE age >= 25;

------------------------------ 5 ------------------------------
-- 5.1. check data for existing
---- 5.1.1.1. attributes
select xpath_exists(
	'//passenger[@country]', 
	xmlparse(DOCUMENT pg_read_file('C:\Users\Public\Documents\PBD_lab6\airtickets.xml'))
);

---- 5.1.1.2. xml-elements with concrete values of attributes
select xpath_exists(
	'/airtickets/passengers/passenger[@gender=''false'' and @country=''Italy'']', 
	xmlparse(DOCUMENT pg_read_file('C:\Users\Public\Documents\PBD_lab6\airtickets.xml'))
);

---- 5.1.2. xml-elements
select xpath_exists(
	'//passenger//ticket', 
	xmlparse(DOCUMENT pg_read_file('C:\Users\Public\Documents\PBD_lab6\airtickets.xml'))
);

---- 5.1.3. values of xml-elements
select xpath_exists(
	'//passenger[@uid=3]//ticket[.=3]', 
	xmlparse(DOCUMENT pg_read_file('C:\Users\Public\Documents\PBD_lab6\airtickets.xml'))
);


-- 5.2. get data
---- 5.2.1. attributes
select unnest(xpath(
	'//passenger/@name', 
	xmlparse(DOCUMENT pg_read_file('C:\Users\Public\Documents\PBD_lab6\airtickets.xml'))
));

---- 5.2.2. xml-elements
select unnest(xpath(
	'//passenger[@uid=3]/ticket', 
	xmlparse(DOCUMENT pg_read_file('C:\Users\Public\Documents\PBD_lab6\airtickets.xml'))
));

---- 5.2.3. values of xml-elements
select unnest(xpath(
	'//passenger[@uid=3]/ticket/text()', 
	xmlparse(DOCUMENT pg_read_file('C:\Users\Public\Documents\PBD_lab6\airtickets.xml'))
));

-- 5.3. get xml fragment
select unnest(xpath(
	'//luggagevariant[1]', 
	xmlparse(DOCUMENT pg_read_file('C:\Users\Public\Documents\PBD_lab6\airtickets.xml'))
));


------------------------------ 6 ------------------------------
-- aggregation. tickets count for passengers
select xpath('passenger/@name', ps) as name, xpath('count(passenger/ticket)', ps) as cnt 
from unnest(xpath(
	'//passenger', 
	xmlparse(DOCUMENT pg_read_file('C:\Users\Public\Documents\PBD_lab6\airtickets.xml'))
)) as ps;

-- aggregarion with grouping. average age of passengers for each gender
select gender, avg(age) as cnt
from (
select unnest(xpath('passenger/@age', ps))::text::int as age, 
	   unnest(xpath('passenger/@gender', ps))::text as gender
from unnest(xpath(
	'//passenger', 
	xmlparse(DOCUMENT pg_read_file('C:\Users\Public\Documents\PBD_lab6\airtickets.xml'))
)) as ps
)j
GROUP BY gender;

-- condition
select xpath('passenger/@name', ps) as name, 
	   xpath('passenger/@age', ps) as age, 
	   xpath('count(passenger/ticket)', ps) as tickets_count
from unnest(xpath(
	'/airtickets/passengers/passenger[@age > 20 and count(ticket) > 1]', 
	xmlparse(DOCUMENT pg_read_file('C:\Users\Public\Documents\PBD_lab6\airtickets.xml'))
)) as ps;

----------------------- full text search -------------------------
-- func to read file
CREATE OR REPLACE FUNCTION my_doc()
RETURNS text AS
$$
DECLARE
	doc text;
BEGIN
	SELECT pg_read_file('C:\Users\Public\Documents\PBD_lab6\document_for_analyze.txt') INTO doc;
	RETURN doc;
END
$$ LANGUAGE plpgsql;

------------------------------- 7 [ts-vector, ts-query] --------------------------------
-- ts-vector [types casting]
select to_tsvector ('english', my_doc());
select my_doc()::tsvector;

-- ts-query [types casting] --> impress & comfort
select to_tsquery('english', 'the & impress & comfort');
select 'the & impress & comfort'::tsquery;
select plainto_tsquery('english', 'the & impress & comfort');

-- search with & (and)
select to_tsvector ('english', my_doc()) @@ to_tsquery('english', 'impress & comfort');

-- search with | (or)
select to_tsvector ('english', my_doc()) @@ to_tsquery('english', 'bad | terror');

-- search with <->
select to_tsvector ('english', my_doc()) @@ to_tsquery('english', 'seat <-> arrange');
select to_tsvector ('english', my_doc()) @@ to_tsquery('english', 'arrange <-> seat');

-- phraseto_tsquery
select phraseto_tsquery('Flight begin from Moscow, ends in Anapa');

-- search with !
select to_tsvector ('english', my_doc()) @@ 
	to_tsquery('english', '!bad & !terror & !horrible & !sad & !inattentive & !long');


------------------------------ 8 ------------------------------
-- create dictionary
CREATE TEXT SEARCH DICTIONARY public.simple_dict (
	TEMPLATE = pg_catalog.simple,
	STOPWORDS = my_own_stop_word_dict
);

-- check dictionary
select ts_lexize('public.simple_dict', 'say');
select ts_lexize('public.simple_dict', 'good');

------------------------------ 9 ------------------------------
-- create synonyms dictionary
CREATE TEXT SEARCH DICTIONARY my_synonyms_dictionary (
	TEMPLATE = synonym,
	SYNONYMS = my_synonyms
);

-- DROP TEXT SEARCH CONFIGURATION tst_config;
-- DROP TEXT SEARCH DICTIONARY my_synonyms_dictionary;

-- check synonyms dictionary
select ts_lexize('my_synonyms_dictionary', 'travel');
select ts_lexize('my_synonyms_dictionary', 'fast');

-- create configuration for synonyms dictionary
CREATE TEXT SEARCH CONFIGURATION tst_config (copy=simple);
ALTER TEXT SEARCH CONFIGURATION tst_config ALTER MAPPING FOR asciiword
	WITH my_synonyms_dictionary;

-- check synonyms dictionary
select to_tsvector('tst_config', 'travel fast boarding place');

-- search with synonyms dictionary
select to_tsvector('english', my_doc()) @@ to_tsquery('tst_config', 'registration');

select to_tsvector('english', my_doc()) @@ to_tsquery('tst_config', 'fast');


-- create thesaurus dictionary
CREATE TEXT SEARCH DICTIONARY my_thesaurus_dictionary (
	TEMPLATE = thesaurus,
	DictFile = my_thesaurus,
	Dictionary = english_stem
);

-- create configuration for thesaurus dictionary
ALTER TEXT SEARCH CONFIGURATION english 
	ALTER MAPPING FOR asciiword, asciihword, hword_asciipart
	WITH my_thesaurus_dictionary, english_stem;

-- DROP TEXT SEARCH DICTIONARY my_thesaurus_dictionary;
	
-- check thesaurus dictionary
select plainto_tsquery('english', 'The General Electric 90 is powered by General Electric 90 engines');
select to_tsvector('english', 'The aircraft advanced avionics systems, including Traffic Collision Avoidance System and Automatic Dependent Surveillance-Broadcast');

-- search with thesaurus dictionary
select to_tsvector('english', my_doc2()) @@ plainto_tsquery('english', 'GE');


------------------------------ 10 ------------------------------
CREATE TABLE Reviews
(
	reviewID serial PRIMARY KEY NOT NULL,
	information json,
	content text
);

INSERT INTO Reviews(reviewID, information, content) VALUES (1, '{"authorName": "Svetlana", "points": 5}', 'The Moscow-Anapa flight was a breeze! The check-in process was smooth and efficient, the flight attendants were friendly and accommodating, and the in-flight service was top-notch. I arrived in Anapa feeling refreshed and ready to enjoy my vacation.');
INSERT INTO Reviews(reviewID, information, content) VALUES (2, '{"authorName": "Tatyana", "points": 2}', 'My expreience on the Moscow-Anapa flight was marred by long delays and poor communication from the airline. The Moscow-Anapa flight was delayed by several hours without any explanation or updates provided to passengers. The lack of transparency was frustrating and put a damper on what should have been a pleasant journey.');
INSERT INTO Reviews(reviewID, information, content) VALUES (3, '{"authorName": "Sofiya", "points": 3}', 'The Moscow-Anapa flight was marred by issues with luggage handling. Several passengers, including myself, experienced delays in receiving our checked bags upon arrival in Anapa. The disorganization and lack of communication regarding the baggage situation was disappointing and put a sour note on an otherwise uneventful flight.');
INSERT INTO Reviews(reviewID, information, content) VALUES (4, '{"authorName": "Petr", "points": 5}', 'I was pleasantly surprised by the comfort and cleanliness of the aircraft on the Moscow-Anapa flight. The seats were spacious, the cabin was well-maintained, and the overall ambiance was relaxing. I appreciated the attention to detail and felt at ease throughout the flight');
INSERT INTO Reviews(reviewID, information, content) VALUES (5, '{"authorName": "Ivan", "points": 5}', 'I give high marks to the Moscow-Anapa flight for its punctuality and efficiency. The Moscow-Anapa flight departed and arrived on time, allowing me to plan my travel itinerary with confidence. I appreciated the airlines attention to timeliness and reliability, making for a stress-free journey on Moscow-Anapa flight.');

select * from Reviews;

-- add tsvector-column
ALTER TABLE Reviews ADD COLUMN TextSearchableIndexCol tsvector
GENERATED ALWAYS AS (to_tsvector('english', coalesce(content, ''))) STORED;

-- add index
CREATE INDEX TextSearchIndex ON Reviews USING GIN (TextSearchableIndexCol);

-- search with index
select information->'authorName' as author, (information->'points')::text::int as points 
from Reviews where TextSearchableIndexCol @@ to_tsquery('english', 'delay')
order by points;

------------------------------ 11 ------------------------------
-- ranking
select information->'authorName' as author, (information->'points')::text::int as points,
ts_rank_cd(TextSearchableIndexCol, query) as rank
from Reviews, to_tsquery('english', 'Moscow<->Anapa<->flight') query
where query @@ to_tsvector('english', content)
order by rank desc;

-- headline
select ts_headline('english', content, to_tsquery('delay'), 
	'StartSel = <, StopSel = >, MinWords=10') 
from Reviews;


---------------------------------------------- additional funcs ----------------------------------------------

-- for utf8 symbols
-- https://stackoverflow.com/questions/14961992/postgresql-replace-html-entities-function/14985946#14985946
create table character_entity(
    name text primary key,
    ch char(1) unique
);
insert into character_entity (ch, name) values
    (E'\u00C6','AElig'),(E'\u00C1','Aacute'),(E'\u00C2','Acirc'),(E'\u00C0','Agrave'),(E'\u0391','Alpha'),(E'\u00C5','Aring'),(E'\u00C3','Atilde'),(E'\u00C4','Auml'),(E'\u0392','Beta'),(E'\u00C7','Ccedil'),
    (E'\u03A7','Chi'),(E'\u2021','Dagger'),(E'\u0394','Delta'),(E'\u00D0','ETH'),(E'\u00C9','Eacute'),(E'\u00CA','Ecirc'),(E'\u00C8','Egrave'),(E'\u0395','Epsilon'),(E'\u0397','Eta'),(E'\u00CB','Euml'),
    (E'\u0393','Gamma'),(E'\u00CD','Iacute'),(E'\u00CE','Icirc'),(E'\u00CC','Igrave'),(E'\u0399','Iota'),(E'\u00CF','Iuml'),(E'\u039A','Kappa'),(E'\u039B','Lambda'),(E'\u039C','Mu'),(E'\u00D1','Ntilde'),
    (E'\u039D','Nu'),(E'\u0152','OElig'),(E'\u00D3','Oacute'),(E'\u00D4','Ocirc'),(E'\u00D2','Ograve'),(E'\u03A9','Omega'),(E'\u039F','Omicron'),(E'\u00D8','Oslash'),(E'\u00D5','Otilde'),(E'\u00D6','Ouml'),
    (E'\u03A6','Phi'),(E'\u03A0','Pi'),(E'\u2033','Prime'),(E'\u03A8','Psi'),(E'\u03A1','Rho'),(E'\u0160','Scaron'),(E'\u03A3','Sigma'),(E'\u00DE','THORN'),(E'\u03A4','Tau'),(E'\u0398','Theta'),
    (E'\u00DA','Uacute'),(E'\u00DB','Ucirc'),(E'\u00D9','Ugrave'),(E'\u03A5','Upsilon'),(E'\u00DC','Uuml'),(E'\u039E','Xi'),(E'\u00DD','Yacute'),(E'\u0178','Yuml'),(E'\u0396','Zeta'),(E'\u00E1','aacute'),
    (E'\u00E2','acirc'),(E'\u00B4','acute'),(E'\u00E6','aelig'),(E'\u00E0','agrave'),(E'\u2135','alefsym'),(E'\u03B1','alpha'),(E'\u0026','amp'),(E'\u2227','and'),(E'\u2220','ang'),(E'\u00E5','aring'),
    (E'\u2248','asymp'),(E'\u00E3','atilde'),(E'\u00E4','auml'),(E'\u201E','bdquo'),(E'\u03B2','beta'),(E'\u00A6','brvbar'),(E'\u2022','bull'),(E'\u2229','cap'),(E'\u00E7','ccedil'),(E'\u00B8','cedil'),
    (E'\u00A2','cent'),(E'\u03C7','chi'),(E'\u02C6','circ'),(E'\u2663','clubs'),(E'\u2245','cong'),(E'\u00A9','copy'),(E'\u21B5','crarr'),(E'\u222A','cup'),(E'\u00A4','curren'),(E'\u21D3','dArr'),
    (E'\u2020','dagger'),(E'\u2193','darr'),(E'\u00B0','deg'),(E'\u03B4','delta'),(E'\u2666','diams'),(E'\u00F7','divide'),(E'\u00E9','eacute'),(E'\u00EA','ecirc'),(E'\u00E8','egrave'),(E'\u2205','empty'),
    (E'\u2003','emsp'),(E'\u2002','ensp'),(E'\u03B5','epsilon'),(E'\u2261','equiv'),(E'\u03B7','eta'),(E'\u00F0','eth'),(E'\u00EB','euml'),(E'\u20AC','euro'),(E'\u2203','exist'),(E'\u0192','fnof'),
    (E'\u2200','forall'),(E'\u00BD','frac12'),(E'\u00BC','frac14'),(E'\u00BE','frac34'),(E'\u2044','frasl'),(E'\u03B3','gamma'),(E'\u2265','ge'),(E'\u003E','gt'),(E'\u21D4','hArr'),(E'\u2194','harr'),
    (E'\u2665','hearts'),(E'\u2026','hellip'),(E'\u00ED','iacute'),(E'\u00EE','icirc'),(E'\u00A1','iexcl'),(E'\u00EC','igrave'),(E'\u2111','image'),(E'\u221E','infin'),(E'\u222B','int'),(E'\u03B9','iota'),
    (E'\u00BF','iquest'),(E'\u2208','isin'),(E'\u00EF','iuml'),(E'\u03BA','kappa'),(E'\u21D0','lArr'),(E'\u03BB','lambda'),(E'\u2329','lang'),(E'\u00AB','laquo'),(E'\u2190','larr'),(E'\u2308','lceil'),
    (E'\u201C','ldquo'),(E'\u2264','le'),(E'\u230A','lfloor'),(E'\u2217','lowast'),(E'\u25CA','loz'),(E'\u200E','lrm'),(E'\u2039','lsaquo'),(E'\u2018','lsquo'),(E'\u003C','lt'),(E'\u00AF','macr'),
    (E'\u2014','mdash'),(E'\u00B5','micro'),(E'\u00B7','middot'),(E'\u2212','minus'),(E'\u03BC','mu'),(E'\u2207','nabla'),(E'\u00A0','nbsp'),(E'\u2013','ndash'),(E'\u2260','ne'),(E'\u220B','ni'),
    (E'\u00AC','not'),(E'\u2209','notin'),(E'\u2284','nsub'),(E'\u00F1','ntilde'),(E'\u03BD','nu'),(E'\u00F3','oacute'),(E'\u00F4','ocirc'),(E'\u0153','oelig'),(E'\u00F2','ograve'),(E'\u203E','oline'),
    (E'\u03C9','omega'),(E'\u03BF','omicron'),(E'\u2295','oplus'),(E'\u2228','or'),(E'\u00AA','ordf'),(E'\u00BA','ordm'),(E'\u00F8','oslash'),(E'\u00F5','otilde'),(E'\u2297','otimes'),(E'\u00F6','ouml'),
    (E'\u00B6','para'),(E'\u2202','part'),(E'\u2030','permil'),(E'\u22A5','perp'),(E'\u03C6','phi'),(E'\u03C0','pi'),(E'\u03D6','piv'),(E'\u00B1','plusmn'),(E'\u00A3','pound'),(E'\u2032','prime'),
    (E'\u220F','prod'),(E'\u221D','prop'),(E'\u03C8','psi'),(E'\u0022','quot'),(E'\u21D2','rArr'),(E'\u221A','radic'),(E'\u232A','rang'),(E'\u00BB','raquo'),(E'\u2192','rarr'),(E'\u2309','rceil'),
    (E'\u201D','rdquo'),(E'\u211C','real'),(E'\u00AE','reg'),(E'\u230B','rfloor'),(E'\u03C1','rho'),(E'\u200F','rlm'),(E'\u203A','rsaquo'),(E'\u2019','rsquo'),(E'\u201A','sbquo'),(E'\u0161','scaron'),
    (E'\u22C5','sdot'),(E'\u00A7','sect'),(E'\u00AD','shy'),(E'\u03C3','sigma'),(E'\u03C2','sigmaf'),(E'\u223C','sim'),(E'\u2660','spades'),(E'\u2282','sub'),(E'\u2286','sube'),(E'\u2211','sum'),
    (E'\u2283','sup'),(E'\u00B9','sup1'),(E'\u00B2','sup2'),(E'\u00B3','sup3'),(E'\u2287','supe'),(E'\u00DF','szlig'),(E'\u03C4','tau'),(E'\u2234','there4'),(E'\u03B8','theta'),(E'\u03D1','thetasym'),
    (E'\u2009','thinsp'),(E'\u00FE','thorn'),(E'\u02DC','tilde'),(E'\u00D7','times'),(E'\u2122','trade'),(E'\u21D1','uArr'),(E'\u00FA','uacute'),(E'\u2191','uarr'),(E'\u00FB','ucirc'),(E'\u00F9','ugrave'),
    (E'\u00A8','uml'),(E'\u03D2','upsih'),(E'\u03C5','upsilon'),(E'\u00FC','uuml'),(E'\u2118','weierp'),(E'\u03BE','xi'),(E'\u00FD','yacute'),(E'\u00A5','yen'),(E'\u00FF','yuml'),(E'\u03B6','zeta'),
    (E'\u200D','zwj'),(E'\u200C','zwnj')
;

create or replace function entity2char(t text)
returns text as $body$
declare
    r record;
begin
    for r in
        select distinct ce.ch, ce.name
        from
            character_entity ce
            inner join (
                select name[1] "name"
                from regexp_matches(t, '&([A-Za-z]+?);', 'g') r(name)
            ) s on ce.name = s.name
    loop
        t := replace(t, '&' || r.name || ';', r.ch);
    end loop;

    for r in
        select distinct
            hex[1] hex,
            ('x' || repeat('0', 8 - length(hex[1])) || hex[1])::bit(32)::int codepoint
        from regexp_matches(t, '&#x([0-9a-f]{1,8}?);', 'gi') s(hex)
    loop
        t := regexp_replace(t, '&#x' || r.hex || ';', chr(r.codepoint), 'gi');
    end loop;

    for r in
        select distinct
            chr(codepoint[1]::int) ch,
            codepoint[1] codepoint
        from regexp_matches(t, '&#([0-9]{1,10}?);', 'g') s(codepoint)
    loop
        t := replace(t, '&#' || r.codepoint || ';', r.ch);
    end loop;

    return t;
end;
$body$
language plpgsql immutable;

DROP TABLE character_entity;


-- xml[] to int[]
CREATE OR REPLACE FUNCTION xml_list_to_int(xml[])
RETURNS int[] AS $$
SELECT ARRAY(SELECT to_number(($1[i])::text,'999999.99')::int
                FROM generate_series(1, array_upper($1,1)) g(i))
$$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (xml[] AS int[]) WITH FUNCTION xml_list_to_int(xml[]);


-- other doc for full text search with thesaurus
CREATE OR REPLACE FUNCTION my_doc2()
RETURNS text AS
$$
DECLARE
	doc text;
BEGIN
	SELECT pg_read_file('C:\Users\Public\Documents\PBD_lab6\document_for_analyze2.txt') INTO doc;
	RETURN doc;
END
$$ LANGUAGE plpgsql;
