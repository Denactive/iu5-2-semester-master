-- Продолжение sql-файлика lab1


--  2. Скалярная в синтаксисе SQL
CREATE OR REPLACE FUNCTION mul2(x bigint, y bigint) 
	RETURNS bigint AS $$
 		SELECT x * y;
	$$ LANGUAGE SQL;

SELECT mul2(100, 500);

--  2. Скалярная в синтаксисе plpgsql
CREATE OR REPLACE FUNCTION get_reversed_user_id(login text)
	RETURNS integer AS
	$$
	DECLARE
		user_id integer;
	BEGIN
		SELECT "ID" FROM "USERS" WHERE "LOGIN" = login into user_id;
		RETURN user_id;
	END;
	$$ LANGUAGE plpgsql;

SELECT * FROM get_reversed_user_id('dskirillov');





-- База для функций
-- CREATE OR REPLACE FUNCTION get_user_contributions(user_id integer)
-- 	RETURNS TABLE(login varchar(255), is_password_strong boolean) AS
-- 	$$
-- -- 	DECLARE 
-- 	BEGIN
-- 		SELECT * FROM CONTRIBUTORS WHERE CONTRIBUTORS.ID = user_id
-- 		RETURN;
-- 	END;
-- 	$$ LANGUAGE plpgsql;
-- 	


SELECT * FROM "USERS";

-- 3. inline. Проверка длины пароля
CREATE OR REPLACE FUNCTION is_password_strong()
	RETURNS TABLE(login varchar(255), is_password_strong boolean) AS
	$$
		SELECT "LOGIN", length("PASSWORD") > 6 FROM "USERS";
	$$ LANGUAGE SQL;

SELECT * FROM is_password_strong();









-- 4, multi-statement
CREATE OR REPLACE FUNCTION get_user_contributions(login text)
	RETURNS TABLE(
		"CREATED" timestamp,
		"NAME" varchar(255),
		"ACCESS_RIGHTS" ACCESS_RIGHTS_FLAGS_T,
		"IS_STAFF" boolean
	) AS
	$$
	DECLARE
		user_id integer;
	BEGIN
		SELECT "ID" FROM "USERS" WHERE "LOGIN" = login into user_id;
		RETURN QUERY
			SELECT "CONTRIBUTORS"."CREATED", "ROLES"."NAME", "ROLES"."ACCESS_RIGHTS", "ROLES"."IS_STAFF"
			FROM "CONTRIBUTORS" INNER JOIN "ROLES" ON "CONTRIBUTORS"."ROLE_ID" = "ROLES"."ID" WHERE "CONTRIBUTORS"."ID" = user_id;
	END;
	$$ LANGUAGE plpgsql;

SELECT * FROM get_user_contributions('dskirillov');






-- База для процедур
-- CREATE OR REPLACE PROCEDURE insertContributorDesc() AS
-- 	$$
-- 	DECLARE 
-- 	BEGIN
		
-- 	END;
-- 	$$

-- ALTER TABLE "CONTRIBUTORS" DROP COLUMN "DESCRIPTION";
ALTER TABLE "CONTRIBUTORS" ADD COLUMN "DESCRIPTION" text;

SELECT * FROM "CONTRIBUTORS";

-- 5,6. Поцедура, циклы, IF, EXCEPTION
CREATE OR REPLACE PROCEDURE insertContributorDescription(contributor_str varchar(20), admin_str varchar(20), timestamp_limit timestamp) AS
	$$
	DECLARE
		contributor_id integer;
		role_name text;
		is_staff boolean;
		created timestamp;
		
		captured_message text;
	BEGIN
		FOR contributor_id, role_name, is_staff, created IN
			EXECUTE '
				SELECT "CONTRIBUTORS"."ID", "ROLES"."NAME", "ROLES"."IS_STAFF", "CONTRIBUTORS"."CREATED"
				FROM "CONTRIBUTORS"
				INNER JOIN "ROLES" ON "CONTRIBUTORS"."ROLE_ID" = "ROLES"."ID"
				-- WHERE "CONTRIBUTORS"."DESCRIPTION" IS NULL;
			'
		LOOP
			IF created < timestamp_limit THEN
				RAISE EXCEPTION USING
					errcode='E0001',
					message='Нельзя модифицировать DESCRIPTION старым пользователям';
			END IF;
			
			IF is_staff THEN
				UPDATE "CONTRIBUTORS" SET "DESCRIPTION"=format('%s-%s-%s', admin_str, role_name, contributor_id) WHERE "ID" = contributor_id;
			ELSE
				UPDATE "CONTRIBUTORS" SET "DESCRIPTION"=format('%s-%s-%s', contributor_str, role_name, contributor_id) WHERE "ID" = contributor_id;
			END IF;
		END LOOP;
		
		EXCEPTION WHEN SQLSTATE 'E0001' THEN
			GET STACKED DIAGNOSTICS captured_message = MESSAGE_TEXT;
			RAISE NOTICE E'\nПЕРЕХВАЧЕННАЯ ОШИБКА: %\n', captured_message;
			RETURN;
	END;
	$$ LANGUAGE plpgsql;

CALL insertContributorDescription('Контрибутор', 'Админ', '2010-01-01 00:00:00'::timestamp);
-- exception
-- CALL insertContributorDescription('Контрибутор', 'Админ', now()::timestamp);

SELECT * FROM "CONTRIBUTORS";




-- Далее Расширенная часть стоимостью 1 балл

-- 7. Рекурсия. Сделано непраивльно. Нужна работа с нашими таблицами, а не искусстенный пример.
-- Например, сделать таблицу Сотрудников. Пусть есть 2 столбца. Первый - ID Сотрудника, кому подчиняется
-- данный Сотрудник. Второй - ID подчиенного Сотрудника данного Сотрудника.
CREATE OR REPLACE FUNCTION factorial(base integer)
	RETURNS SETOF integer AS
    $$
	BEGIN
	RETURN QUERY
		WITH RECURSIVE r AS (
			-- стартовая часть рекурсии
			SELECT 
				1 AS i, 
				1 AS acc

			UNION 
			-- рекурсивная часть 
			SELECT 
				i+1 AS i, 
				acc * (i+1) as acc 
			FROM r
			WHERE i < base
		) SELECT acc FROM r;
	END;
	$$ LANGUAGE plpgsql;

SELECT * FROM factorial(5);


-- 8.
SELECT * FROM factorial(5) LIMIT 3;

INSERT INTO "CONTRIBUTORS"
	VALUES (101, '2009-01-01 00:00:00'::timestamp, 1, 1)
	RETURNING "ID";

-- 9 row_number(), Rank(), dense_rank(), ntile(4).
SELECT *, ROW_NUMBER() OVER () FROM "CONTRIBUTORS";
SELECT *, ROW_NUMBER() OVER (ORDER BY "ID" DESC) FROM "CONTRIBUTORS";

SELECT *, RANK() OVER (ORDER BY "CREATED") FROM "CONTRIBUTORS";

SELECT *, DENSE_RANK() OVER (ORDER BY "CREATED") FROM "CONTRIBUTORS";

SELECT *, NTILE(2) OVER (ORDER BY "CREATED") FROM "CONTRIBUTORS";


-- 10
CREATE OR REPLACE FUNCTION read_join(
	OUT contributor_id integer,
	OUT	role_name text
)
RETURNS SETOF RECORD AS
$$
DECLARE
    c_ CURSOR FOR
        SELECT "CONTRIBUTORS"."ID" as contributor_id, "ROLES"."NAME" as role_name
			FROM "CONTRIBUTORS"
			INNER JOIN "ROLES" ON "CONTRIBUTORS"."ROLE_ID" = "ROLES"."ID";
    record RECORD;
BEGIN
    OPEN c_;
	
    LOOP
        FETCH NEXT FROM c_ INTO record;
        EXIT WHEN NOT FOUND;
        contributor_id := record.contributor_id;
        role_name := record.role_name;
        RETURN NEXT;
    END LOOP;
	
    CLOSE c_;
END;
$$
LANGUAGE PLPGSQL;
	
SELECT * FROM read_join();

-- 11
SELECT pg_size_pretty(pg_total_relation_size('public."CONTRIBUTORS"'));







-- Защита
$$DO

  -- 1. Скрипт для удаления старых CONTRIBUTORS

  INSERT INTO "CONTRIBUTORS" VALUES (100, '2009-01-01 00:00:00'::timestamp, 1, 1)
  SELECT * FROM "CONTRIBUTORS";

  CREATE OR REPLACE PROCEDURE delete_old_users(timestamp_limit timestamp) AS
    $$
    DECLARE
      contributor_id integer;
      role_name text;
      is_staff boolean;
      created timestamp;
      
      captured_message text;
    BEGIN
      FOR contributor_id, role_name, is_staff, created IN
        EXECUTE '
          SELECT "CONTRIBUTORS"."ID", "ROLES"."NAME", "ROLES"."IS_STAFF", "CONTRIBUTORS"."CREATED"
          FROM "CONTRIBUTORS"
          INNER JOIN "ROLES" ON "CONTRIBUTORS"."ROLE_ID" = "ROLES"."ID"
        '
      LOOP
        IF created < timestamp_limit THEN
          DELETE FROM "CONTRIBUTORS" WHERE "ID" = contributor_id;
        END IF;
      END LOOP;
    END;
    $$ LANGUAGE plpgsql;

  CALL delete_old_users('2010-01-01 00:00:00'::timestamp);
  SELECT * FROM "CONTRIBUTORS";

  -- 2. Скрипт для отлова ошибки синтаксиса. Сначала выполняем без EXCEPTION WHEN SQLSTATE,
  -- скрипт упадет, и будет представлен код ошибки = 42601.
  -- Потом добавляем EXCEPTION WHEN SQLSTATE '42601' THEN
  CREATE OR REPLACE PROCEDURE secure_syntax(timestamp_limit timestamp) AS
    $$
    DECLARE
      contributor_id integer;
      role_name text;
      is_staff boolean;
      created timestamp;
      
      captured_message text;
    BEGIN
      FOR contributor_id, role_name, is_staff, created IN
        EXECUTE '
          SELECT "CONTRIBUTORS"."ID", "ROLES"."NAME", "ROLES"."IS_STAFF", "CONTRIBUTORS"."CREATED"
          FROMdajkdkjakd "CONTRIBUTORS"
          INNER JOIN "ROLES" ON "CONTRIBUTORS"."ROLE_ID" = "ROLES"."ID"
        '
      LOOP
        
      END LOOP;
      
      EXCEPTION WHEN SQLSTATE '42601' THEN
        RAISE NOTICE'Была ошибка синтаксиса, но мы сдюжили';
        RETURN;
    END;
    $$ LANGUAGE plpgsql;

  CALL secure_syntax('2010-01-01 00:00:00'::timestamp);



  -- Доп 3. Дано имя таблицы и имя поля. Получить значения, отловить вохможные ошибки
  CREATE OR REPLACE FUNCTION select_field_from_table(tablename varchar(20), fieldname varchar(20))
    RETURNS SETOF varchar(255) AS
    $$
    BEGIN
      RETURN QUERY EXECUTE format('
        SELECT %I FROM %I
      ', fieldname, tablename);
      
    EXCEPTION WHEN OTHERS THEN
      RAISE NOTICE'Нет такой таблицы или такого поля, но мы сдюжили';
    END;
    $$ LANGUAGE plpgsql;

  SELECT * FROM select_field_from_table('USERS', 'LOGIN');
  -- exception
  -- SELECT * FROM select_field_from_table('USERS', 'LOGIdshd');
$$
