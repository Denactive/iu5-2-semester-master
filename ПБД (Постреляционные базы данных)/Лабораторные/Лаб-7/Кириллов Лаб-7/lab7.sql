-- Таблица с английским текстом
DROP TABLE IF EXISTS "SERVERS";

CREATE TABLE "SERVERS"
(
	"ID" serial PRIMARY KEY NOT NULL,
	"NAME" varchar(127) NOT NULL UNIQUE,
	"DESCRIPTION" varchar(511) NOT NULL
);

INSERT INTO "SERVERS"("NAME", "DESCRIPTION") VALUES (
	'UnitMCU Medium',
	'The UnitMCU Medium is a versatile server unit designed for medium-sized enterprises that require efficient and reliable performance. With advanced processing capabilities and scalable storage options, this unit is ideal for handling a variety of applications and workloads.'
);

INSERT INTO "SERVERS"("NAME", "DESCRIPTION") VALUES (
	'TiSserver Smart',
	'The TiSserver Smart is a smart server unit that combines performance with intelligent features for enhanced productivity and management. Equipped with cutting-edge technology, this unit offers high-speed connectivity and secure data handling, making it suitable for businesses seeking a modern and efficient server solution.'
);

INSERT INTO "SERVERS"("NAME", "DESCRIPTION") VALUES (
	'SL Unit',
	'The SL Unit is a compact and efficient server unit tailored for small to mid-sized businesses. Featuring a streamlined design and optimized performance, this unit delivers reliable functionality for basic computing needs and essential business applications, making it a cost-effective solution for organizations with limited space and resources.'
);

INSERT INTO "SERVERS"("NAME", "DESCRIPTION") VALUES (
	'ASUS RS Series',
	'The ASUS RS Series is a robust and reliable server unit designed for enterprise-level performance and scalability. With extensive storage capacity, advanced security features, and high-speed processing power, this series is ideal for demanding workloads and critical business operations.'
);

SELECT * FROM "SERVERS";

-- 1.
-- Поиск вхождений лексем на русском
CREATE OR REPLACE FUNCTION my_doc()
RETURNS text AS
$$
DECLARE
	doc text;
BEGIN
	SELECT pg_read_file('C:\Users\Public\text.txt') INTO doc;
	RETURN doc;
END
$$ LANGUAGE plpgsql;

SELECT my_doc();

-- Элементарные проверки
SELECT
	-- true
	to_tsvector('russian', my_doc()) @@ to_tsquery('russian', 'пурпурный'),
	to_tsvector('russian', my_doc()) @@ to_tsquery('russian', 'книжица'),
	-- false
	to_tsvector('russian', my_doc()) @@ to_tsquery('russian', 'книга'),
	to_tsvector('russian', my_doc()) @@ to_tsquery('russian', 'крокозябра');
	
-- Соответствия И, ИЛИ, НЕ, предшествие, альтернативный запрос phraseto_tsquery
SELECT
-- &
	-- true
	to_tsvector('russian', my_doc()) @@ to_tsquery('russian', 'пурпурный & книжица'),
	-- false
	to_tsvector('russian', my_doc()) @@ to_tsquery('russian', 'пурпурный & книга'),
-- |
	-- true
	to_tsvector('russian', my_doc()) @@ to_tsquery('russian', 'пурпурный | книжица'),
	-- false
	to_tsvector('russian', my_doc()) @@ to_tsquery('russian', 'крокозябра | книга'),
-- !
	-- true
	to_tsvector('russian', my_doc()) @@ to_tsquery('russian', '!книга'),
	-- false
	to_tsvector('russian', my_doc()) @@ to_tsquery('russian', '!книжица'),
-- <->
	phraseto_tsquery('Сдают паспорта, и я сдаю мою пурпурную книжицу.'),
	-- true
	to_tsvector('russian', my_doc()) @@ to_tsquery('russian', 'пурпурный <-> книжица'),
	-- false
	to_tsvector('russian', my_doc()) @@ to_tsquery('russian', 'книжица <-> пурпурный');




-- Далее все для английского
-- 2 стоп-словарь
-- SELECT ts_lexize('simple_dict', 'this server unit  is bad');
SELECT
	ts_lexize('simple_dict', 'server'),
	ts_lexize('simple_dict', 'unit'),
	ts_lexize('simple_dict', 'a'),
	ts_lexize('simple_dict', 'the'),
	ts_lexize('simple_dict', 'enterprise')
;

-- 3 словарь синонимов

SELECT
	ts_lexize('my_synonyms_dictionary', 'enterprise'), -- project
	ts_lexize('my_synonyms_dictionary', 'server'), -- unit
	ts_lexize('my_synonyms_dictionary', 'unit'), -- null
	ts_lexize('my_synonyms_dictionary', 'server unit'), -- null
	ts_lexize('my_synonyms_dictionary', 'robust'), -- strong
	ts_lexize('my_synonyms_dictionary', 'enterprise-level'), -- high
	-- обратная замена не работает
	ts_lexize('my_synonyms_dictionary', 'high') -- null
;

DROP TEXT SEARCH CONFIGURATION tst_config;
CREATE TEXT SEARCH CONFIGURATION tst_config (copy=simple);
ALTER TEXT SEARCH CONFIGURATION tst_config ALTER MAPPING FOR asciiword WITH my_synonyms_dictionary;

SELECT to_tsvector('tst_config', 'this robust server is good solution for enterprise');
-- enterprise-level не хочет работать в функции to_tsvector, хотя работает в ts_lexize
SELECT to_tsvector('tst_config', 'this robust server is enterprise-level solution');
