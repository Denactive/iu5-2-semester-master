// Вызов скрипта через консоль забагает кириллицу

// print all 
// MATCH (e) RETURN e;

// drop db
MATCH (e) DETACH DELETE e;

// MATCH (e:User) REMOVE e:User RETURN e;
// MATCH (e:Role) REMOVE e:Role RETURN e;
// MATCH (e:Repository) REMOVE e:Repository RETURN e;
// MATCH (e:Contributor) REMOVE e:Contributor RETURN e;

CREATE
  (dk: User { 
    login: 'd.kirillov',
    password: '111',
    firstname: 'Denis', 
    lastname: 'Kirillov',
    age: 21}),
  (ip: User { 
    login: 'i.petrov',
    password: '111',
    firstname: 'Ivan', 
    lastname: 'Petrov',
    age: 18
  }),
  (pi: User { 
    login: 'p.ivanov',
    password: '111',
    firstname: 'Petr', 
    lastname: 'Ivanov',
    age: 15
  }),

  (me: Role {
    name: 'manager'
  }),
  (de: Role {
    name: 'developer'
  }),
  (sr: Role {
    name: 'sre'
  }),

  (re: Repository {
    name: 'XVII пятилетка'
  }),

  (re)-[:USER_IS {since: '2019-03-07'}]->(dk),
  (re)-[:ROLE_IS {since: '2019-03-07'}]->(me),
  
  (re)-[:USER_IS {since: '2020-04-07'}]->(ip),
  (re)-[:ROLE_IS {since: '2020-04-07'}]->(de)
;


// 2

MATCH (e) RETURN e;
MATCH (e:User) RETURN e;
MATCH (e:User) RETURN e.login, e.lastname, e.firstname;
MATCH (e:User{login: 'd.kirillov'}) RETURN e;
MATCH (e:User) WHERE e.login = 'd.kirillov' RETURN e;
MATCH (e:User) WHERE e.age <= 20 RETURN e;
MATCH (u) WHERE u:USER RETURN u

// 3

MATCH
  (pi:User{login: 'p.ivanov'}),
  (re:Repository)
MERGE
  (re) -[имя_связи:USER_IS {since: timestamp()}]->(pi) 
RETURN имя_связи

// 4

MATCH (p:User) WHERE p.login='p.ivanov' DETACH DELETE p;

MATCH (re:Repository)-[имя_связи]->(u:User) 
WHERE u.login='p.ivanov'
DELETE имя_связи;

MATCH (u) WHERE u.login='p.ivanov' 
SET u.age=11
RETURN u.login, u.firstname, u.lastname, u.age;

MATCH (u) WHERE u.login='p.ivanov' 
SET u.eats='Жульен'
RETURN u.login, u.firstname, u.lastname, u.age, u.eats;

MATCH (u) WHERE u.login='p.ivanov'
REMOVE u.eats
RETURN u.login, u.firstname, u.lastname, u.age, u.eats;

// Замена метки
MATCH (u:User)
WHERE u.login='p.ivanov'
REMOVE u:User
SET u:Admin
RETURN u;


// Удаление метки
MATCH (u:Admin)
WHERE u.login='p.ivanov'
REMOVE u:Admin
RETURN u;

// Создание метки
MATCH (u)
WHERE u.login='p.ivanov'
SET u:Admin
RETURN u;


// 5

// node filtering
MATCH (r)-->(u) 
WHERE u.login='d.kirillov'
RETURN r, u;

// relation filtering
MATCH (r:Repository)-[connection]->(u:User) WHERE connection.since is not null RETURN r.name, u.login;
MATCH (r:Repository)-[connection]->(u:User) WHERE connection.since = '2019-03-07' RETURN r.name, u.login;

// label filtering
MATCH (r)-->(u) WHERE u: User RETURN r, u;
MATCH (u)-->(r) WHERE u: User RETURN r, u;

// 6

// IS NOT NULL
MATCH (p) WHERE p.name IS NOT NULL RETURN p;

// AND, OR
MATCH (r:Repository)-->(u:User) 
WHERE (u.login='i.petrov' OR u.login='d.kirillov') AND (r.name IS NOT NULL)
RETURN r, u;

// sort
MATCH (u:User) 
RETURN u.login, u.firstname, u.lastname, u.age
ORDER BY u.firstname, u.age;

// direct relation -> ok
// direct relation -> empty
// см. п.5 label filtering

// relation params
// См. п.5 relation filtering

// 7

// LIMIT
MATCH (n) RETURN n LIMIT 5;
// SKIP
MATCH (n) RETURN n SKIP 5;

// 8

// так не работает из-за имени
MATCH (u:User) RETURN u 
UNION 
MATCH (r:Repository) RETURN r;

MATCH (u_or_r:User) RETURN u_or_r
UNION 
MATCH (u_or_r:Repository) RETURN u_or_r;

// 9

// agregation
MATCH (u:User) RETURN avg(u.age) as avg_age;

// inline functions
// substring
MATCH (r:Repository)-[connection]->(u:User)
WHERE connection.since is not null
RETURN 'день: ' + SUBSTRING(connection.since, 8, 2) +
'| месяц: ' + SUBSTRING(connection.since, 5, 2) + 
'| год: '+ SUBSTRING(connection.since, 0, 4)
AS connectionSince;

// with start node
MATCH (r:Repository)-[connection*1..2]->(u:User)
RETURN r, u;

MATCH (r:Repository)-[connection*0..1]->(u:User)
RETURN r, u;

// relation templates
// нужны длинные цепочки связей
CREATE
  (dk: YamYam {
    name: 'жульен',
    ingredients: ['chicken', 'cheese', 'mushrooms']
  }),
  (ck: Ingredient{
    name: 'chicken',
    eats: 'seeds'
  });

MATCH
  (y:YamYam),
  (u:User {login:'d.kirillov'})
MERGE
  (u)-[conn1:LIKES]->(y)
RETURN conn1;

MATCH
  (y:YamYam),
  (i:Ingredient)
MERGE
  (y)-[conn2:REQUIRES]->(i)
RETURN conn2;


// MATCH (e:Employee)-[*]->(w:Workday) RETURN e,w

// Шаблоны отношений переменной длины
// Это описывает график из трех узлов и двух отношений
MATCH (a)-[*2]->(b)
RETURN a, b;


// 10

CREATE INDEX userLoginIndex FOR (u:User) ON (u.login);

// Использование индекса:
MATCH (u1:USER) USING INDEX u2:User(login)
WHERE u2.login='d.kirillov'
RETURN u1, u2

-- show all indexes
show indexes;

-- delete index
DROP INDEX userLoginIndex;


// 11

// unique property constraint
// Нужно удалить индекс с логина
// CREATE CONSTRAINT uniqueUserLoginConstraint
// FOR (u:User)
// REQUIRE u.login IS UNIQUE;

// Property existence constraint requires Neo4j Enterprise Editio
// CREATE CONSTRAINT existingUserLoginConstraint
// FOR (u:User)
// REQUIRE u.login IS NOT NULL;

CREATE CONSTRAINT uniqueRepositoryNameConstraint
FOR (r:Repository)
REQUIRE r.name IS UNIQUE;

// демо
CREATE
  (r1:Repository{name: 'XVIII пятилетка', tag:'хайп'}),
  (r2:Repository{name: 'XVIII пятилетка', tag:'всесоюзный съезд'});

// show all constraints
show constraints;

// delete constraint
DROP CONSTRAINT uniqueRepositoryNameConstraint;


// 12

CREATE
  (dk: Cerial {
    name: 'Каша',
    ingredients: ['wheet', 'milk', 'water']
});

// размер массива, обращение по индексу
MATCH (i:Cerial{name: 'Каша'}) 
RETURN i.name, i.ingredients[1], size(i.ingredients);

// Проверка на наличие элемента в списке 
MATCH (i)
WHERE 'milk' in i.ingredients or 'cheese' in i.ingredients
RETURN i;



// Доп. 1

// 1. Показать работу с массивом
// См.12

// 2. Вывести репозиторий, у которого есть связь с жульеном
MATCH (r:Repository)-[*]->(y:YamYam{name: 'жульен'})
RETURN r,y;

// 3. Вывести репозиторий, у которого больше 1 роли и больше 1 юзера

// По идее, в более ранних версиях СУБД должно было работать так:
// Тут проблема в том, что агрегация не происходит, cnt всегда равен 1
MATCH (r:Repository)-->(u:User)
WITH r, u, count(conn) as cnt
WHERE cnt >= 2
RETURN *;

// Или так, с использованием встроенной функции size, аналогичной агрегационной функции count
// Это тоже в более ранних версиях СУБД
MATCH (r:Repository)-->(u:User)
WHERE size((r)-[]->(u:User)) >= 2
RETURN *;

// В 5.12.0 работает такой синтаксис подзапроса COUNT {...}. Это именно подзапрос
MATCH (r:Repository)-->(u:User)
WHERE COUNT { (r)-[]->(:User) } >= 2
RETURN *;

// 4. Вывести всех юзеров, у которых есть репозиторий и они старше 18

MATCH (re:Repository)-->(u:User)
WHERE u.age >= 18
RETURN *;
