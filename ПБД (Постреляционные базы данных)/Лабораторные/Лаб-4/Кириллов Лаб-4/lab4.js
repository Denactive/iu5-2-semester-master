// 1

// use gitDb
db = connect("mongodb://localhost/gitDb");

db.roles.drop();
db.createCollection("roles", { capped: true, size: 30000, max: 3 });
db.users.drop();
db.createCollection("users");
db.contributors.drop();
db.createCollection("contributors");
db.repositories.drop();
db.createCollection("repositories");

db.roles.insertOne({
  name: "manager",
  description: "Менеджер - самый главный в проекте",
  accessRights: [
    "addContributors",
    "runPipeline",
    "viewPipelineLogs",
    "release",
  ],
});

// 2.1

db.roles.insertMany([
  {
    name: "sre",
    description: "SRE - смотрит логи",
    accessRights: ["viewPipelineLogs"],
  },
  {
    name: "developer",
    description: "Разработчик - пишет код",
    accessRights: ["commit", "runPipeline", "viewPipelineLogs"],
  },
]);

db.roles.find().pretty();

db.users.insertMany([
  {
    login: "i.petrov",
    password: "123",
    pasport: {
      name: "Ivan",
      surname: "Petrov",
      age: 15,
    },
  },
  {
    login: "p.ivanov",
    password: "123",
    pasport: {
      name: "Petr",
      surname: "Ivanov",
      age: 19,
      eats: "пельмени",
    },
  },
  {
    login: "o.olegov",
    password: "123",
    pasport: {
      name: "Oleg",
      surname: "Olegov",
      age: 28,
    },
  },
]);

db.users.find().pretty();

db.repositories.insertOne({
  name: "Всесоюзный проект",
});

db.repositories.find().pretty();

// 2.2

db.users.updateOne({ login: "p.ivanov" }, { $set: { password: "321" } });
db.repositories.find({ login: "p.ivanov" }).pretty();

// 2.3

db.users.updateOne({ login: "p.ivanov" }, { $unset: { "pasport.eats": "" } });
db.users.find({ login: "p.ivanov" }).pretty();

// 2.4 замена всего объекта

db.users.replaceOne(
  { login: "p.ivanov" },
  {
    login: "p.ivanov",
    password: "789",
    pasport: {
      name: "Petr",
      surname: "Ivanovich",
      age: 22,
      eats: "беляши",
    },
  }
);

db.users.find({ login: "p.ivanov" }).pretty();

// 2.5. Удаление

db.users.deleteOne({ login: "p.ivanov" });
db.users.find({}).pretty();

// 3.1 вывод всей коллекции

db.users.find().pretty();

// 3.2 вывод с фильтрацией

db.users.find({
  $and: [{ "pasport.age": { $gte: 15 } }, { "pasport.age": { $lt: 20 } }],
});
db.users.find({
  $or: [{ login: { $eq: "o.olegov" } }, { "pasport.age": { $lt: 16 } }],
});

// 3.3 проекция вывода

db.users.find({}, { login: 1, password: 1 });
db.users.find({}, { _id: 0, login: 1, password: 1 });

// 3.4 сортировка

db.users
  .find({}, { _id: 0, login: 1, "pasport.age": 1 })
  .sort({ "pasport.age": 1 });

// 3.5 удаление дубликатов.
// Имелся ввиду вывод уникальных

db.users.insertOne({
  login: "o.olegov",
  description: "я вообще другой !",
});

db.users.distinct("login");

// 3.6 условия на поля вложенных структур.
// Похоже на 3.2

db.users.find({
  $and: [{ "pasport.age": { $gte: 15 } }, { "pasport.age": { $lt: 20 } }],
});

// 3.7 поиск по вложенным коллекциям объекта
// по словам аспиранта это массивы

db.repositories.updateOne(
  {
    name: "Всесоюзный проект",
  },
  {
    $set: {
      payingHistory: [
        {
          date: new Date("2024-02-01T08:00:01"),
          amount: 1.7,
          currency: "usd",
        },
        {
          date: new Date("2024-03-01T12:44:31"),
          amount: 210.1,
          currency: "rub",
        },
        {
          date: new Date("2024-04-01T09:33:01"),
          amount: 2.5,
          currency: "usd",
        },
      ],
    },
  }
);

db.roles.find({ accessRights: { $all: ["runPipeline", "viewPipelineLogs"] } });

db.roles.find({ accessRights: { $size: 3 } });
// Так нельзя =(
// db.roles.find({ accessRights: { $gt: { $size: 2 } } });

db.roles.find({ accessRights: { $nin: ["release"] } });

db.roles.find({ accessRights: { $in: ["runPipeline"] } });

db.repositories
  .find({
    payingHistory: {
      $elemMatch: {
        date: {
          $gt: new Date("2024-03-01"),
        },
      },
    },
  })
  .pretty();

// 4.1 проверка на наличие и отсутствие полей в документе

db.users.find({ "description ": { $exists: 1 } });
db.users.find({ "description ": { $exists: 0 } });

// 4.2 ограничение на количество документов в результате

db.roles.countDocuments();
db.roles.find({}, { accessRights: 1 }).limit(2);
db.roles.find({}, { accessRights: 1 }).skip(2);
db.users.distinct("pasport.name");

// 4.3 с операторами сравнения
// См. п3.2, п.3.6

// 4.4 работа с массивами
// Поиск выполняли в п.3.7 базовой части.

// До изменений
db.roles.find({ name: "sre" });
// ["viewPipelineLogs"]
db.roles.updateOne({ name: "sre" }, { $push: { accessRights: "runPipeline" } });
db.roles.find({ name: "sre" });
// ["viewPipelineLogs", "runPipeline"]

db.roles.updateOne(
  { name: "sre" },
  {
    $push: {
      accessRights: {
        $each: [
          "downloadLogs",
          "viewContributors",
          "viewContributors",
          "viewContributors",
          "viewContributors",
        ],
      },
    },
  }
);
db.roles.find({ name: "sre" });
// ["viewPipelineLogs", "runPipeline", 'downloadLogs', 'viewContributors', 'viewContributors', 'viewContributors', 'viewContributors']

db.roles.updateOne(
  { name: "sre" },
  { $addToSet: { accessRights: "downloadLogs" } }
);
db.roles.find({ name: "sre" });
// ["viewPipelineLogs", "runPipeline", 'downloadLogs', 'viewContributors', 'viewContributors', 'viewContributors', 'viewContributors']

db.roles.updateOne({ name: "sre" }, { $pop: { accessRights: 1 } });
db.roles.find({ name: "sre" });
// ["viewPipelineLogs", "runPipeline", 'downloadLogs', 'viewContributors', 'viewContributors', 'viewContributors']

db.roles.updateOne(
  { name: "sre" },
  { $pull: { accessRights: "viewContributors" } }
);
db.roles.find({ name: "sre" });
// ["viewPipelineLogs", "runPipeline", 'downloadLogs', 'viewContributors', 'viewContributors']

db.roles.updateOne(
  { name: "sre" },
  { $pullAll: { accessRights: ["downloadLogs", "runPipeline"] } }
);
db.roles.find({ name: "sre" });
// ["viewPipelineLogs", "runPipeline", 'downloadLogs']

db.roles.updateOne({ name: "sre" }, { $push: { accessRights: "runPipeline" } });
db.roles.updateOne({ name: "sre" }, { $set: { "accessRights.1": "saveLogs" } });
db.roles.find({ name: "sre" });
// ["viewPipelineLogs", "runPipeline", 'saveLogs']

// 4.5 группировка (group) и агрегирование (aggregate)

// сначала добавлю записей

db.users.deleteOne({ description: { $exists: true } });

db.users.insertMany([
  {
    login: "d.kirillov",
    password: "123",
    pasport: {
      name: "Denis",
      surname: "Kirillov",
      age: 30,
      eats: "жульен",
    },
  },
  {
    login: "a.sergeev",
    password: "123",
    pasport: {
      name: "Alexey",
      surname: "Sergeev",
      age: 34,
      eats: "жульен",
    },
  },
  {
    login: "o.andreev",
    password: "123",
    pasport: {
      name: "Oles",
      surname: "Andreev",
      age: 28,
      eats: "жульен",
    },
  },
]);

db.users.aggregate([
  { $project: { _id: 0, login: 1, "pasport.age": 1, "pasport.eats": 1 } },
  { $group: { _id: "$pasport.eats", avg_age: { $avg: "$pasport.age" } } },
  { $sort: { avg_age: -1 } },
]);

// 5

db.repositories.find({}, { _id: 1 }).limit(1); // 66263ba50a588d1842117ba5
db.users.find({}, { _id: 1 }).limit(1); // 66263ba50a588d1842117ba2
db.roles.find({}, { _id: 1 }).limit(1); // 66263ba50a588d1842117b9f

db.contributors.drop();
db.createCollection("contributors");
db.contributors.insertOne({
  repositoryId: {
    $ref: "repositories",
    $id: ObjectId("66263ba50a588d1842117ba5"),
    $db: "gitDb",
  },
  userId: {
    $ref: "users",
    $id: ObjectId("66268d120a588d1842117baa"),
    $db: "gitDb",
  },
  roleId: {
    $ref: "roles",
    $id: ObjectId("66263ba50a588d1842117b9f"),
    $db: "gitDb",
  },
});

db.contributors.find();
db.contributors.findOne().userId; // Вывод DBRef
// теоретически, можно было бы "сджоинить" как-то вот так, но проблема в том, что DBRef.$id не возвращается
db.contributors.findOne().userId.$id; // хрен там плавал
// db.users.find({ _id: db.contributors.findOne().userld.$id });
// db.contributors.find({ "userId.$id": db.users.findOne({}, { _id: 1 })._id });

// переход по ссылке DBRef см. ниже в доп. 1.


// 6

// по логину по убыванию
db.users.createIndex({ login: -1 });
db.users.getIndexes();

// Защита №1. Join

// https://www.mongodb.com/docs/manual/core/views/join-collections-with-view/
// Join'ить можно в пайплайнах с помощью оператора $lookup
// Чтобы посмотреть на результат join'а, можно создать материальное представление (View) на его основе
// полезными могут быть операторы пайплайна: $project, $unwind

db.contributorsUsersView.drop();
db.createView("contributorsUsersView", "contributors", [
  {
    $lookup: {
      from: "users",
      localField: "userId.$id",
      foreignField: "_id",
      as: "us",
    },
  },
]);
db.contributorsUsersView.find();


// Join'ить можно и просто по одинаковым полям. Создам такую же таблицу без DBRef и такой же View для нее:

db.contributors2.drop();
db.createCollection("contributors2");
db.contributors2.insertOne({
  repositoryId: ObjectId("66263ba50a588d1842117ba5"),
  userId: ObjectId("66268d120a588d1842117baa"),
  roleId: ObjectId("66263ba50a588d1842117b9f"),
});
db.contributors2.updateOne(
  { repositoryId: ObjectId("66263ba50a588d1842117ba5") },
  {
    $set: {
      created: "2024-04-23",
    },
  }
);

db.contributorsUsersView2.drop();
db.createView("contributorsUsersView2", "contributors2", [
  {
    $lookup: {
      from: "users",
      localField: "userId",
      foreignField: "_id",
      as: "us",
    },
  },
]);
db.contributorsUsersView2.find();



// Защита №2. Количество людей с возрастом больше N

db.users.find({'pasport.age': {$gt: 29}}).count()
